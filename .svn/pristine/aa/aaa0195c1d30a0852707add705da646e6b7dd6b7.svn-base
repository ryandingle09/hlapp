<?php

date_default_timezone_set('Asia/Kolkata');

class Caresurvey extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('caresurvey_model', 'caresurvey');
		$this->load->model('count_model', 'dbcount');
    }
	
	public function checksegment()
	{
		if(!$this->uri->segment(3)) redirect('dashboard');
	}
	
	public function index()
	{
		$data['host'] = $this->caresurvey->get_hospitals();
		$this->layouts->view('home_view', $data);
	}
	
	public function byhospital()
	{
		$this->layouts->view('byhospital_view');
	}
	
	public function iplist()
	{
		$data = [
			'upload_date' 	=> $this->caresurvey->get_upload_date(),
			'unit'			=> $this->caresurvey->get_unit_by_hospital(),
		];
		$this->layouts->view('list_view',$data);
	}

	public function aslist()
	{
		$data = [
			'upload_date' 	=> $this->caresurvey->get_upload_date(),
			'unit'			=> $this->caresurvey->get_unit_by_hospital(),
		];
		$this->layouts->view('list_view',$data);
	}

	public function oplist()
	{
		$data = [
			'upload_date' 	=> $this->caresurvey->get_upload_date(),
			'unit'			=> $this->caresurvey->get_unit_by_hospital(),
		];
		$this->layouts->view('list_view',$data);
	}

	public function erlist()
	{
		$data = [
			'upload_date' 	=> $this->caresurvey->get_upload_date(),
			'unit'			=> $this->caresurvey->get_unit_by_hospital(),
		];
		$this->layouts->view('list_view',$data);
	}

	public function save_per_item()
    {
    	$params = [
    		'type'		=> $this->input->post('type'),
    		'hosp'		=> $this->input->post('hosp'),
    		'code'		=> $this->input->post('code'),
    		'random'	=> $this->input->post('random'),
    		'field'		=> $this->input->post('field'),
    		'value'		=> $this->input->post('value'),
    	];
    	
		$this->caresurvey->save_per_item($params);
    }

    public function get_modal_content()
    {
    	$params = [
    		'title'		=> $this->input->post('title'),
    	];
    	
		$this->load->view('modal/patient_list',$params);
    }

    public function complete()
    {
    	$type = $this->uri->segment(3);
    	$view = '';

    	switch ($type){
			case 'AS':
				$view = 'as';
				break;
			case 'IP':
				$view = 'ip';
				break;
			case 'OP':
				$view = 'op';
				break;
			case 'ER':
				$view = 'er';
				break;
			default:
				show_404();
				break;
		}

		if($this->session->userdata('username'))
		{
			if($type == 'ER' || $type == 'OP')
				$this->layoutsms->view(''.$view.'/'.$view.'_complete');
			else
				$this->layouts->view(''.$view.'/'.$view.'_complete');
		}	
		else
		{
			$this->layoutsms->view(''.$view.'/'.$view.'_complete');
		}
    	
    }

    public function complete2()
    {
    	$type = $this->uri->segment(3);
    	$view = '';

    	switch ($type){
			case 'AS':
				$view = 'as';
				break;
			case 'IP':
				$view = 'ip';
				break;
			default:
				show_404();
				break;
		}

		if($this->session->userdata('username'))
		{
			$this->layoutsms->view(''.$view.'/'.$view.'_complete2');
		}	
		else
		{
			$this->layoutsms->view(''.$view.'/'.$view.'_complete2');
		}
    	
    }

    public function check_randomizer_existense()
    {
    	$code 	= $this->input->post('code');
    	$random = $this->input->post('random');
    	$type 	= $this->input->post('type');
    	$hosp 	= $this->input->post('hosp');
    	$this->caresurvey->check_randomizer_existense($code, $random, $hosp, $type);
    }

    public function cc()
    {
		$type 		= $this->uri->segment(3);
		$hosp 		= $this->uri->segment(4);
		$code 		= $this->uri->segment(5);
		$random 	= $this->uri->segment(6);

		//check if call canter
		if($type != 'IP' && $type != 'AS') show_404();
		
		//check if both exists on randomizer and survey table
		$checker 	= $this->caresurvey->check_both_randomizer_and_survey($type, $hosp, $code, $random);

		if($checker == '0') show_404();

		//check if complete
		$is_complete = $this->caresurvey->check_if_complete($type, $hosp, $code);
		if($is_complete == '1') redirect('caresurvey/complete/'.$type.'');

		$view 		= ($type == 'IP') ? 'ip' : 'as';
		
		$trans_hosp = $this->caresurvey->get_hospital_detail($hosp);

		$this->caresurvey->remove_in_randomizer($type, $hosp, $code, $random);

		$doctor_details = $this->caresurvey->get_survey_detail();
		$dr_code 		= (isset($doctor_details->drcode)) ? $doctor_details->drcode : '';

		$data = [
			'survey' 			=> $doctor_details,
			'doctor' 			=> $this->caresurvey->get_doctor_detail($dr_code),
			'hospital_name'		=> $trans_hosp->description,
			'hospital_trans'	=> '<p dir="rtl">'.$trans_hosp->description_ar.'<p>',
		];

		$this->layouts->view(''.$view.'/'.$view.'_survey', $data);
    }

    public function sms()
    {
    	$type 		= $this->uri->segment(3);
		$hosp 		= $this->uri->segment(4);
		$code 		= $this->uri->segment(5);
		$random 	= $this->uri->segment(6);

		//check if sms
		if($type != 'OP' && $type != 'ER') show_404();
		
		//check if both exists on randomizer and survey table
		$checker 	= $this->caresurvey->check_both_randomizer_and_survey($type, $hosp, $code, $random);

		if($checker == '0') show_404();

		//check if complete
		$is_complete = $this->caresurvey->check_if_complete($type, $hosp, $code);
		if($is_complete == '1') redirect('caresurvey/complete/'.$type.'');

		$view 		= ($type == 'OP') ? 'op' : 'er';
		
		$trans_hosp = $this->caresurvey->get_hospital_detail($hosp);

		$this->caresurvey->remove_in_randomizer($type, $hosp, $code, $random);

		$doctor_details = $this->caresurvey->get_survey_detail();
		$dr_code 		= (isset($doctor_details->drcode)) ? $doctor_details->drcode : '';

		$data = [
			'survey' 			=> $doctor_details,
			'doctor' 			=> $this->caresurvey->get_doctor_detail($dr_code),
			'hospital_name'		=> $trans_hosp->description,
			'hospital_trans'	=> '<p dir="rtl">'.$trans_hosp->description_ar.'<p>',
		];

		if($this->session->userdata('username'))
			$this->layoutsms->view(''.$view.'/'.$view.'_survey', $data);
		else
			$this->layoutsms->view(''.$view.'/'.$view.'_survey', $data);
    }

    public function view_survey()
    {
		$type 		= $this->uri->segment(3);
		$hosp 		= $this->uri->segment(4);
		$code 		= $this->uri->segment(5);
		$random 	= $this->uri->segment(6);

		//check if call canter
		if($type != 'IP' && $type != 'AS') show_404();
		
		//check if both exists on randomizer and survey table
		$checker 	= $this->caresurvey->check_both_randomizer_and_survey($type, $hosp, $code, $random);

		if($checker == '0') show_404();

		$view 		= ($type == 'IP') ? 'ip' : 'as';
		
		$trans_hosp = $this->caresurvey->get_hospital_detail($hosp);

		$this->caresurvey->remove_in_randomizer($type, $hosp, $code, $random);

		$doctor_details = $this->caresurvey->get_survey_detail();
		$dr_code 		= (isset($doctor_details->drcode)) ? $doctor_details->drcode : '';

		$data = [
			'survey' 			=> $doctor_details,
			'doctor' 			=> $this->caresurvey->get_doctor_detail($dr_code),
			'hospital_name'		=> $trans_hosp->description,
			'hospital_trans'	=> '<p dir="rtl">'.$trans_hosp->description_ar.'<p>',
		];

		$this->layouts->view(''.$view.'/'.$view.'_search_survey', $data);
    }

    public function email_survey()
    {
		$type 		= $this->uri->segment(3);
		$hosp 		= $this->uri->segment(4);
		$code 		= $this->uri->segment(5);
		$random 	= $this->uri->segment(6);

		//check if call canter
		if($type != 'IP' && $type != 'AS') show_404();
		
		//check if both exists on randomizer and survey table
		$checker 	= $this->caresurvey->check_both_randomizer_and_survey($type, $hosp, $code, $random);

		if($checker == '0') show_404();

		//check if complete
		$is_complete = $this->caresurvey->check_if_complete($type, $hosp, $code);
		if($is_complete == '1') redirect('caresurvey/complete2/'.$type.'');

		$view 		= ($type == 'IP') ? 'ip' : 'as';
		
		$trans_hosp = $this->caresurvey->get_hospital_detail($hosp);

		$this->caresurvey->remove_in_randomizer($type, $hosp, $code, $random);

		$doctor_details = $this->caresurvey->get_survey_detail();
		$dr_code 		= (isset($doctor_details->drcode)) ? $doctor_details->drcode : '';

		$data = [
			'survey' 			=> $doctor_details,
			'doctor' 			=> $this->caresurvey->get_doctor_detail($dr_code),
			'hospital_name'		=> $trans_hosp->description,
			'hospital_trans'	=> '<p dir="rtl">'.$trans_hosp->description_ar.'<p>',
		];

		if($this->session->userdata('username'))
			$this->layoutsms->view(''.$view.'/'.$view.'_email_survey', $data);
		else
			$this->layoutsms->view(''.$view.'/'.$view.'_email_survey', $data);
    }

    public function send_survey_by_mail()
    {
    	$trans_hosp = $this->caresurvey->get_hospital_detail($this->input->post('hosp'));
    	$data 		= [
    		'name'  			=> $this->input->post('respondent'),
    		'link' 				=> base_url().'caresurvey/email_survey/'.$this->input->post('type').'/'.$this->input->post('hosp').'/'.$this->input->post('code').'/',
    		'hospital_name'		=> $trans_hosp->description,
			'hospital_trans'	=> $trans_hosp->description_ar,
    	];

    	$this->email->set_mailtype("html");
    	$this->email->set_newline("\r\n");
        $this->email->from('health-links@healthlinks-dev.com', 'Health-Links');
        $this->email->to($this->input->post('email'));
        $this->email->subject('Emailed Survey');
    	$mail 	= $this->load->view('mail/survey', $data, TRUE);
	  	$this->email->message($mail);

        if($this->email->send())
        {
        	echo '1';
        }
        else 
        {
        	echo '0';
        }
    }

    public function search_mrn_as()
    {
    	$value = $this->input->post('value');
    	if(empty($value)) echo '<tr><td colspan="11">No Result Found.</td></tr>';
    	else
    	{
    		$result = $this->caresurvey->get_search_mrn($value, 'as');
	    	if(empty($result)) echo '<tr><td colspan="11">No Result Found.</td></tr>';
	    	else
	    	{
	    		$response 	= '';
	    		$status 	= '';
	    		foreach($result as $row)
		    	{
		    		$response1 	= ($row->initial_response == '1') ? 'Yes' : '';
		    		$response2 	= ($row->initial_response == '2') ? 'Yes' : '';
		    		$response3 	= ($row->initial_response == '3') ? 'Yes' : '';
		    		$status 	= ($row->status == 'submitted') ? 'Complete' : $row->status;
		    		echo '
		    			<tr>
		                    <td>'.$row->mrno.'</td>
		                    <td>'.$row->patname.'</td>
		                    <td>'.$row->guardian.'</td>
		                    <td>'.$row->mobile.'</td>
		                    <td>'.$row->unit.'</td>
		                    <td>'.date('m-d-Y', $row->visitdate).'</td>
		                    <td>'.$response1.'</td>
		                    <td>'.$response2.'</td>
		                    <td>'.$response3.'</td>
		                    <td>'.$status.'</td>
		                    <td>
		                    	<a target="_blank" class="btn btn-info" data-hosp="'.$row->hosp.'" data-code="'.$row->code.'" href="'.base_url().'caresurvey/view_survey/AS/'.$row->hosp.'/'.$row->code.'/"> View <i class="fa fa-eye"></i> </a>
		                    </td>
		                </tr>
		    		';
		    	}
	    	}
    	}
    }

    public function search_mrn_ip()
    {
    	$value = $this->input->post('value');
    	if(empty($value)) echo '<tr><td colspan="11">No Result Found.</td></tr>';
    	else
    	{
	    	$result = $this->caresurvey->get_search_mrn($value, 'ip');
	    	if(empty($result)) echo '<tr><td colspan="11">No Result Found.</td></tr>';
	    	else
	    	{
	    		$response 	= '';
	    		$status 	= '';
	    		foreach($result as $row)
		    	{
		    		$response1 	= ($row->initial_response == '1') ? 'Yes' : '';
		    		$response2 	= ($row->initial_response == '2') ? 'Yes' : '';
		    		$response3 	= ($row->initial_response == '3') ? 'Yes' : '';
		    		$status 	= ($row->status == 'submitted') ? 'Complete' : $row->status;
		    		echo '
		    			<tr>
		                    <td>'.$row->mrno.'</td>
		                    <td>'.$row->patname.'</td>
		                    <td>'.$row->guardian.'</td>
		                    <td>'.$row->mobile.'</td>
		                    <td>'.$row->unit.'</td>
		                    <td>'.date('m-d-Y', $row->visitdate).'</td>
		                    <td>'.$response1.'</td>
		                    <td>'.$response2.'</td>
		                    <td>'.$response3.'</td>
		                    <td>'.$status.'</td>
		                    <td>
		                    	<a target="_blank" class="btn btn-info" data-hosp="'.$row->hosp.'" data-code="'.$row->code.'" href="'.base_url().'caresurvey/view_survey/IP/'.$row->hosp.'/'.$row->code.'/"> View <i class="fa fa-eye"></i> </a>
		                    </td>
		                </tr>
		    		';
		    	}
	    	}
	    }
    }

    public function get_unit_by_hospital()
    {
    	$unit 	= $this->caresurvey->get_unit_by_hospital();

    	if(count($unit) == 0)
    	{
    		echo '<h3>No Data.</h3>';
    	}
    	else
    	{
    		foreach($unit as $row)
    		{
	    		$targetsIP 			= $this->dbcount->getTotalTargetsIP($row->code);

	    		if(is_null($targetsIP)) $tarIP = 0;
	    		else $tarIP = $targetsIP->target;

	    		$remainingTotalIP 	= $this->dbcount->getRemainingTargetIP($this->input->post('upload_date'), $row->code);
				$totalCompleteIP 	= $this->dbcount->getTotalCompleteIP($this->input->post('upload_date'), $row->code);
				
				$targetsAS 			= $this->dbcount->getTotalTargetsAS($row->code);
	    		$remainingTotalAS 	= $this->dbcount->getRemainingTargetAS($this->input->post('upload_date'), $row->code);
				$totalCompleteAS 	= $this->dbcount->getTotalCompleteAS($this->input->post('upload_date'), $row->code);
				
				echo '
					<a href="javascript:;" id="get-patient" data-description="'.$row->description.'" data-unit="'.$row->code.'" data-type="'.$this->input->post('type').'" data-hosp="'.$this->input->post('hosp').'" data-date="'.$this->input->post('upload_date').'" data-toggle="modal" data-target=".patient_modal">
						<div class="col-md-12 border-double white no-padding">
							<table width="100%">
								<tr>
									<td width="50%" rowspan="3" class="padding-10"><h3>'.$row->description.'</h3></td>
									<td class="text-center border-right border-left border-bottom">SURVEYS TO TARGET</td>
									<td class="text-center border-right border-bottom">REMAINING RECORD</td>
									<td class="text-center border-bottom">TODAYS COMPLETES</td>
								</tr>
								<tr>
									<td class="text-center border-right border-left"><h1>'.$tarIP.'</h1></td>
									<td class="text-center border-right"><h1>'.$remainingTotalIP->total_remaining.'</h1></td>
									<td class="text-center"><h1>'.$totalCompleteIP->total_comp.'</h1></td>
								</tr>
							</table>
						</div>
					</a>
					<div class="col-md-12"></div>
				';
			}
    	}
   	}
	
	public function get_data_table()
    {
    	$type = ($this->uri->segment(3) == 'iplist') ? 'IP' : 'AS';

		$aColumns = [
			"code", 
			"random_no", 
			"patname", 
			"mobile", 
			"nationality",
			"hosp"
		];

        $sEcho = $this->input->get_post('sEcho', true);

        $rResult = $this->caresurvey->get_patient_list($aColumns);

        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
        $iTotal = $this->caresurvey->total_length();

        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );

        foreach($rResult->result_array() as $aRow)
        {
    		$action = '';
            $row = array();
            foreach($aColumns as $col)
            {
               	$row[] = $aRow['patname'];
               	$row[] = $aRow['mobile'];  
               	$row[] = $aRow['nationality'];  
               	$action  = '<div class="text-center"><a target="_blank" class="btn btn-info" id="start-survey" data-hosp="'.$aRow['hosp'].'" data-code="'.$aRow['code'].'" data-type="'.$type.'" data-random="'.$aRow['random_no'].'" href="'.base_url().'caresurvey/cc/'.$type.'/'.$this->uri->segment(4).'/'.$aRow['code'].'/'.$aRow['random_no'].'"> Start Survey </a></div>';
            	$row[] = $action;	 
            }

            $output['aaData'][] = $row;
        }

        echo json_encode($output);

    } 
}