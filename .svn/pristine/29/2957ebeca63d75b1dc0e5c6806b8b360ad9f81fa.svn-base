<?php

class Caresurvey_model extends DBCARE_Model
{
    public function get_hospitals()
    {
        return $this->db->get(DBCARE_Model::tbl_hospitals)->result();
    }

    public function get_hospital_detail($hosp)
    {
        $this->db->where('code', $hosp);
        $data = $this->db->get(DBCARE_Model::tbl_hospitals);
        return $data->row();
    }

    public function get_doctor_detail($drcode)
    {
        $this->db->where('drcode', $drcode);
        $data = $this->db->get(DBCARE_Model::tbl_doctors);
        return $data->row();
    }

    public function check_randomizer_existense($code, $random, $hosp, $type)
    {
        $this->db->where('code', $code);
        $this->db->where('random_no', $random);
        $this->db->where('hosp', $hosp);
        $data = $this->db->get(($type == 'IP') ? DBCARE_Model::tbl_ip_randomized : DBCARE_Model::tbl_as_randomized)->result();
        if(count($data) == 0) echo 0;
        else echo 1;
    }

    public function get_survey_detail()
    {
        $db = '';
        switch ($this->uri->segment(3)){
            case 'IP':
                $db = DBCARE_Model::tbl_ip_survey;
                break;
            case 'AS':
                $db = DBCARE_Model::tbl_as_survey;
                break;
            case 'OP':
                $db = DBCARE_Model::tbl_op_survey;
                break;
            case 'ER':
                $db = DBCARE_Model::tbl_er_survey;
                break;
        }

        $this->db->where([
            'hosp'   => $this->uri->segment(4),
            'code'   => $this->uri->segment(5)
        ]);
        $data = $this->db->get($db);

        return $data->row();
    }

    public function get_unit_by_hospital()
    {
        $hosp           = $this->input->post('hosp');
        $type           = $this->input->post('type');
        $survey_type    = '';

        switch ($type){
            case 'iplist':
                $survey_type = 'IP';
                break;
            case 'aslist':
                $survey_type = 'AS';
                break;
            case 'oplist':
                $survey_type = 'OP';
                break;
            case 'erlist':
                $survey_type = 'ER';
                break;
        }

        $this->db->where('hosp', $hosp);
        $this->db->where('survey_type', $survey_type);
        return $this->db->get(DBCARE_Model::tbl_wards)->result();
    }

    public function get_search_mrn($value, $type)
    {
        $this->db->like('mrno', $value);
        $this->db->or_like('mobile', $value);
        $this->db->limit('20');
        return $this->db->get(($type == 'as') ? DBCARE_Model::tbl_as_survey : DBCARE_Model::tbl_ip_survey)->result();
    }
    
    public function get_upload_date()
    {
        $sTable = '';
        switch ($this->uri->segment(2)){
            case 'iplist':
                $sTable = DBCARE_Model::tbl_ip_randomized;
                break;
            case 'aslist':
                $sTable = DBCARE_Model::tbl_as_randomized;
                break;
            case 'oplist':
                $sTable = DBCARE_Model::tbl_op_randomized;
                break;
            case 'erlist':
                $sTable = DBCARE_Model::tbl_er_randomized;
                break;
        }

        $this->db->where('hosp', $this->uri->segment(3));
        $this->db->select('DISTINCT upload_date', FALSE);
        $this->db->from($sTable);
        
        return $this->db->get()->result();
    }

    public function save_per_item($params)
    {
        $sTable = '';
        switch ($params['type']){
            case 'IP':
                $sTable = DBCARE_Model::tbl_ip_survey;
                break;
            case 'AS':
                $sTable = DBCARE_Model::tbl_as_survey;
                break;
            case 'OP':
                $sTable = DBCARE_Model::tbl_op_survey;
                break;
            case 'ER':
                $sTable = DBCARE_Model::tbl_er_survey;
                break;
        }

        $this->db->where([
            'code'  => $params['code'], 
            'hosp'  => $params['hosp'],
        ]);

        if($params['field'] == 'hl_contact')
        {
            if($params['value'] == '2')
                $this->db->update($sTable, ['hl_contact_name' => '', 'hl_contact_no' => '']);
        }
        
        if($params['field'] == 'hl_qtm')
        {
            if($params['value'] == '2')
                $this->db->update($sTable, ['hl_atm' => '', 'hl_atmd' => '']);
        }

        if($params['field'] == 'initial_response')
        {
            if($params['value'] == '1')
                $this->db->update($sTable, ['rs_day' => '', 'rs_time' => '',  'refuse_reason' => '', 'na_reason' => '']);
            if($params['value'] == '2')
                $this->db->update($sTable, ['refuse_reason' => '', 'na_reason' => '']);
            if($params['value'] == '3')
                $this->db->update($sTable, ['rs_day' => '', 'rs_time' => '', 'na_reason' => '']);
            if($params['value'] == '4')
                $this->db->update($sTable, ['rs_day' => '', 'rs_time' => '', 'refuse_reason' => '']);
            if($params['value'] == '5')
                $this->db->update($sTable, [
                    'rs_day'            => '', 
                    'rs_time'           => '',  
                    'na_reason'         => '',
                    'refuse_reason'     => '', 
                    'na_reason'         => ''
                ]);
        }

        if($params['field'] == 'status')
        {
            $this->db->update($sTable,[
                'status'        => $params['value'], 
                'completedby'   => $this->input->post('completed_by'), 
                'completeddate' => date('Y-m-j m:i:s') 
            ]);
        }

        $this->db->update($sTable, [$params['field'] => $params['value']]);
        
    }

    public function check_if_complete($type, $hosp, $code)
    {
        $db             = '';
        $db2            = '';

        switch ($type){
             case 'ER':
                $db2    = DBCARE_Model::tbl_er_survey;
                break;
             case 'OP':
                $db2    = DBCARE_Model::tbl_op_survey;
                break;
            case 'AS':
                $db2    = DBCARE_Model::tbl_as_survey;
                break;
             case 'IP':
                $db2    = DBCARE_Model::tbl_ip_survey;
                break;
        }

        $this->db->where(['hosp' => $hosp, 'code' => $code, 'status'=> 'submitted']);
        $survey = $this->db->get($db2)->num_rows();

        if($survey == 0) return 0;
        else return 1;
    }

    public function check_both_randomizer_and_survey($type, $hosp, $code, $random)
    {
        $db             = '';
        $db2            = '';

        switch ($type){
             case 'ER':
                $db     = DBCARE_Model::tbl_er_randomized;
                $db2    = DBCARE_Model::tbl_er_survey;
                break;
             case 'OP':
                $db     = DBCARE_Model::tbl_op_randomized;
                $db2    = DBCARE_Model::tbl_op_survey;
                break;
            case 'AS':
                $db     = DBCARE_Model::tbl_as_randomized;
                $db2    = DBCARE_Model::tbl_as_survey;
                break;
             case 'IP':
                $db     = DBCARE_Model::tbl_ip_randomized;
                $db2    = DBCARE_Model::tbl_ip_survey;
                break;
        }

        $this->db->where(['hosp' => $hosp, 'code' => $code,'random_no'=> $random ]);
        $randomizer = $this->db->get($db)->num_rows();

        $this->db->where(['hosp' => $hosp, 'code' => $code]);
        $survey = $this->db->get($db2)->num_rows();

        if($randomizer == 0 && $survey == 0) return 0;
        else return 1;
    }

    public function remove_in_randomizer($type, $hosp, $code, $random)
    {
        $db             = '';
        $db2            = '';
        $survey_type    = '';

        switch ($type){
             case 'ER':
                $db     = DBCARE_Model::tbl_er_randomized;
                $db2    = DBCARE_Model::tbl_er_survey;
                $survey_type    = 'SMS';
                break;
             case 'OP':
                $db     = DBCARE_Model::tbl_op_randomized;
                $db2    = DBCARE_Model::tbl_op_survey;
                $survey_type    = 'SMS';
                break;
            case 'IP':
                $db     = DBCARE_Model::tbl_ip_randomized;
                $db2    = DBCARE_Model::tbl_ip_survey;
                $survey_type    = 'PHONE';
                break;
             case 'AS':
                $db     = DBCARE_Model::tbl_as_randomized;
                $db2    = DBCARE_Model::tbl_as_survey;
                $survey_type    = 'PHONE';
                break;
        }

        $this->db->where(['hosp' => $hosp, 'code' => $code,'random_no'=> $random ]);
        $randomizer = $this->db->get($db);

        if($randomizer->num_rows() == 0) return false;
        else
        {
            foreach ($randomizer->result() as $row)
            {
                $this->db->where(['code' => $code]);
                if($this->db->get($db2)->num_rows() == 1) return false;
                else
                {
                    $this->db->insert($db2,[
                        'code'              => $row->code,
                        'status'            => 'incomplete',
                        'addedby'           => ($this->session->userdata('username') != '') ? $this->session->userdata('username') : 'no username',
                        'addeddate'         => date('Y-m-j m:i:s'),
                        'amendby'           => $row->amendby,
                        'amenddate'         => $row->amenddate,
                        'hosp'              => $row->hosp,
                        'survey_type'       => $survey_type,
                        'upload_date'       => $row->upload_date,
                        'upload_no'         => $row->upload_no,
                        'senddate'          => $row->senddate,
                        'patname'           => $row->patname,
                        'mrno'              => $row->mrno,
                        'drcode'            => $row->drcode,
                        'unit'              => $row->unit,
                        'dept'              => $row->dept,
                        'mobile'            => $row->mobile,
                        'dd_day'            => date('j', strtotime($row->visitdate)),
                        'dd_mon'            => date('m', strtotime($row->visitdate)),
                        'dd_year'           => date('Y', strtotime($row->visitdate)),
                        'dd_time'           => date('m:i:s', strtotime($row->visitdate)),
                        'visitdate'         => $row->visitdate,
                        'age'               => $row->age,
                        'gender'            => $row->gender,
                        'nationality'       => $row->nationality,
                        'fin_cat'           => $row->fin_cat
                    ]);

                    $this->db->where(['code' => $row->code, 'random_no' => $row->random_no]);
                    $this->db->delete($db);
                }
            }

            return true;
        }  
    }

    public function get_patient_list($aColumns)
    {
        $sTable = '';

        switch ($this->uri->segment(3)){
            case 'iplist':
                $sTable = DBCARE_Model::tbl_ip_randomized;
                break;
            case 'aslist':
                $sTable = DBCARE_Model::tbl_as_randomized;
                break;
            case 'oplist':
                $sTable = DBCARE_Model::tbl_op_randomized;
                break;
            case 'erlist':
                $sTable = DBCARE_Model::tbl_er_randomized;
                break;
        }

        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);

        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);

        /** FILTERING **/
        if($sSearch && $sSearch != '')
        {
            for($i=0; $i<count($aColumns); $i++)
            {
                $this->db->or_like($aColumns[$i], $sSearch);
            }
        }

        /** PAGING **/
        if(isset($iDisplayStart) && $iDisplayLength != '-1')
        {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        /** ORDERING **/
        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->db->where([
            'upload_date'   => $this->uri->segment(5),
            'unit'          => $this->uri->segment(6),
            'hosp'          => $this->uri->segment(4)
        ]);
        return $this->db->get($sTable); 
    }   
    
    public function total_length()
    {
        $sTable = '';
        switch ($this->uri->segment(3)){
            case 'iplist':
                $sTable = DBCARE_Model::tbl_ip_randomized;
                break;
            case 'aslist':
                $sTable = DBCARE_Model::tbl_as_randomized;
                break;
            case 'oplist':
                $sTable = DBCARE_Model::tbl_op_randomized;
                break;
            case 'erlist':
                $sTable = DBCARE_Model::tbl_er_randomized;
                break;
        }

         $this->db->where([
            'upload_date'   => $this->uri->segment(5),
            'dept'          => $this->uri->segment(6),
            'hosp'          => $this->uri->segment(4)
        ]);
        return $this->db->count_all($sTable);
    }
}
