<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function get_params($xss = TRUE)
	{
		$CI =& get_instance();

		$get = $CI->input->get(NULL, $xss) ? $CI->input->get(NULL, $xss) : array();
		$post = $CI->input->post(NULL, $xss) ? $CI->input->post(NULL, $xss) : array();
		$params = array_merge(array_map('_secure_param', array_merge($get, $post)), $_FILES);

		return $params;
	}

	function _secure_param($value)
	{
		if (is_array($value)) {
			return array_map('_secure_param', $value);
		} else {
			return urldecode(trim($value));
		}
	}

?>