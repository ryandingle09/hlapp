<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Login_model extends DBCARE_Model
{
   
    public function general_select($table = NULL, $fields = NULL, $where = NULL, $order_by = NULL)
    {
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($order_by);
        $query  = $this->db->get();
        $result = $query->result_array();   

        return $result;
    }

    public function general_update($table, $fields, $where)
    {

        $this->db->set($fields);
        $this->db->where($where);
        $this->db->update($table);
    }

    public function modules($id)
    {
        return $this->db->query('
            SELECT uc.employee_id, uc.module_id as u_module_id, c.* 
            FROM '.DBCARE_Model::tbl_user_modules.' uc 
            JOIN '.DBCARE_Model::tbl_modules.' c ON c.module_id = c.module_id 
            WHERE uc.employee_id = '.$id.' group by c.module_id order by c.module_sort ASC;
        ')->result();
    }

    public function clients($id)
    {
        return $this->db->query('
            SELECT uc.*, c.client_description, c.client_name, c.client_id FROM '.DBCARE_Model::tbl_user_clients.' uc 
            JOIN  '.DBCARE_Model::tbl_clients.' c ON c.client_id = uc.client_id
            WHERE uc.employee_id = '.$id.'
        ')->result();
    }

}