<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DBCARE_Model extends CI_Model
{
	//survey module tables
	const tbl_as_randomized						= 'as_randomized';
	const tbl_as_survey							= 'as_survey';
	const tbl_er_randomized						= 'er_randomized';
	const tbl_er_survey							= 'er_survey';
	const tbl_ip_randomized						= 'ip_randomized';
	const tbl_ip_survey							= 'ip_survey';
	const tbl_op_randomized						= 'op_randomized';
	const tbl_op_survey							= 'op_survey';
	const tbl_hospitals							= 'hosp';
	const tbl_wards								= 'wards';
	const tbl_clinics							= 'clinics';
	const tbl_dept								= 'dept';
	const tbl_doctors							= 'doctors';
	const tbl_fincat							= 'fincat';
	const tbl_fingroup							= 'fingroup';
	const tbl_hosp								= 'hosp';
	const tbl_languages							= 'languages';
	const tbl_nationalities						= 'nationalities';
	const tbl_questions							= 'questions';
	const tbl_responses							= 'responses';
	const tbl_resp_trans						= 'resp_trans';
	const tbl_section							= 'section';
	const tbl_section_trans						= 'section_trans';
	const tbl_userinfo							= 'userinfo';
	const tbl_user_module						= 'user_module';
	const tbl_survey_sections					= 'survey_sections';
	const tbl_survey_questions					= 'survey_questions';
	const tbl_survey_question_choices			= 'survey_question_choices';
	const tbl_clients							= 'clients';
	const tbl_survey_type						= 'survey_type';
	const tbl_survey_type_urls					= 'survey_type_urls';
	const tbl_survey_activated_types			= 'survey_activated_types';
	const tbl_modules							= 'modules';
	const tbl_user_clients						= 'user_clients';
	const tbl_user_modules						= 'user_modules';
}

/* End of file bas_model.php */
/* Location: ./application/core/bas_model.php */