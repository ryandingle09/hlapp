<?php if(!EMPTY($this->session->userdata('username'))) : redirect('auth');  endif; ?>
<?php $id = $this->uri->segment(3); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?=base_url();?>" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link href="<?php echo PATH_CSS ?>login.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>bootstrap-theme.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>font-awesome.min.css" rel="stylesheet">
        <script src="<?php echo PATH_JS ?>jquery.js" type="text/javascript"></script>
        <title>Health-Links</title>
    </head>
    <body class="login-img3-body">
        <div class="container">
            <form class="login-form" method="post" action="<?php echo base_url('auth/reset_password/'.$id) ?>">
                <div class="login-wrap">
                    <p class="login-img"><img src="assets/img/logo_transparent.png" class="img-responsive" width="304" height="236"></p>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" name="password" class="form-control" placeholder="Password" autofocus="" required id="password">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" required id="confirm_password">
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Reset Password</button>
                </div>
            </form>
        </div>

        <script>var baseURL = '<?=base_url()?>';</script>
        <script src="<?php echo base_url().PATH_JS ?>bootstrap.min.js"></script>
        <script type="text/javascript">
        $(function () {
            var password = document.getElementById("password")
              , confirm_password = document.getElementById("confirm_password");

            function validatePassword(){
              if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Do not Match");
              } else {
                confirm_password.setCustomValidity('');
              }
            }

            password.onchange = validatePassword;
            confirm_password.onkeyup = validatePassword;
        });
            
        </script>
    </body>
</html>