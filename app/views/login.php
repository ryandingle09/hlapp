<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?=base_url();?>" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link href="<?php echo PATH_CSS ?>login.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>bootstrap-theme.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>font-awesome.min.css" rel="stylesheet">
        <script src="<?php echo PATH_JS ?>jquery.js" type="text/javascript"></script>
        <title>Health-Links</title>
    </head>
    <body class="login-img3-body">
    
        <div class="container">
            <form class="login-form" method="post" action="<?php echo base_url('auth/login') ?>">
                <div class="login-wrap">
                    <p class="login-img"><img src="assets/img/logo_transparent.png" class="img-responsive" width="304" height="236"></p>
                    <?php if(!empty($this->session->flashdata('message'))): ?>
                        <div class="alert <?php echo (!EMPTY($alert_type)) ? $alert_type : '';?>">
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                        <input type="text" name="username" class="form-control" placeholder="Username" autofocus="" required>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
                    <label class="checkbox"><span class="pull-right"><button type="button" class="btn btn-link" data-toggle="modal" data-target="#forgot_password">Forgot Password?</button></span>
                    </label>
                </div>
            </form>

            <!-- Modal -->
            <div class="modal fade" id="forgot_password" role="dialog">
                <div class="modal-dialog">
                
                    <form  method="post" action="<?php echo base_url('auth/email') ?>">
                       <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Forgot Password</h4>
                            </div>
                            <div class="modal-body">
                                <p>Enter the email registered with your account and we'll send you the link where you can change your password.</p>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email Address" autofocus="" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info">Send Email</button>
                            </div>
                        </div>
                    </form>
                  
                </div>
            </div>

        </div>

        <script>var baseURL = '<?=base_url()?>';</script>
        <script src="<?php echo base_url().PATH_JS ?>bootstrap.min.js"></script>
    </body>
</html>