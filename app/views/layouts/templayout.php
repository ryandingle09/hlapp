<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?=base_url();?>" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link href="https://fonts.googleapis.com/css?family=Miriam+Libre:400,700|Source+Sans+Pro:200,400,700,600,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="<?php echo PATH_CSS ?>style.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>override.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>bootstrap.min.css" rel="stylesheet">
        
        <link href="<?php echo PATH_CSS ?>font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>AdminLTE.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>_all-skins.min.css" rel="stylesheet">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link href="assets/css/override.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>jquery.datetimepicker.css" rel="stylesheet">
        <script src="<?php echo PATH_JS ?>jquery.js" type="text/javascript"></script>
    <title>Health-Links : CARE Randomizer</title>
</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" id="navbar-or">
      <div class="container">
        <a href="<?=base_url()?>">
            <img src="assets/img/hl_logo.png" style="width: 140px"/>
        </a>
      </div>

        <div class="container">
            <div class="btn-group nav" style="float: right">
                <a class="btn btn-danger btn-sm" href="index.php/login/logout" role="button">
              <span class="glyphicon glyphicon-off"></span>
              Logout
            </a>
            </div>
        </div>
    </nav>

    <div class="container" style="margin-top: 150px;">
      <?=$content;?>
  </div>

    <script>var baseURL = '<?=base_url()?>';</script>
    <script src="<?php echo PATH_JS ?>bootstrap.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo PATH_JS ?>jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo PATH_JS ?>fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo PATH_JS ?>app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo PATH_JS ?>demo.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.datetimepicker.js"></script>
    <script>
        $(function(){
            $('#form-submit-er').submit(function(){
                $("#msg-wait-er").html('Please wait... <img src="<?=base_url()?>assets/img/712.gif" width="25" />');
                $('.upload-file-er').attr('disabled', 'disabled');
            });
            
            $('#form-submit-op').submit(function(){
                $("#msg-wait-op").html('Please wait... <img src="<?=base_url()?>assets/img/712.gif" width="25" />');
                $('.upload-file-op').attr('disabled', 'disabled');
            });
            
            $('#form-submit-as').submit(function(){
                $("#msg-wait-as").html('Please wait... <img src="<?=base_url()?>assets/img/712.gif" width="25" />');
                $('.upload-file-as').attr('disabled', 'disabled');
            });
            
            $('#form-submit-ip').submit(function(){
                $("#msg-wait-ip").html('Please wait... <img src="<?=base_url()?>assets/img/712.gif" width="25" />');
                $('.upload-file-ip').attr('disabled', 'disabled');
            });
            
            $('#dtpicker-from-er').datetimepicker({timepicker:false, format:"m/d/Y"}); 
            $('#dtpicker-to-er').datetimepicker({timepicker:false, format:"m/d/Y"}); 
            
            $('#dtpicker-from-op').datetimepicker({timepicker:false, format:"m/d/Y"}); 
            $('#dtpicker-to-op').datetimepicker({timepicker:false, format:"m/d/Y"}); 
            
            $('#dtpicker-from-ip').datetimepicker({timepicker:false, format:"m/d/Y"});
            $('#dtpicker-to-ip').datetimepicker({timepicker:false, format:"m/d/Y"});
            
            $('#dtpicker-from-as').datetimepicker({timepicker:false, format:"m/d/Y"});
            $('#dtpicker-to-as').datetimepicker({timepicker:false, format:"m/d/Y"});
        })
    </script>
</body>
</html>
