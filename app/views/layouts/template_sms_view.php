<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?=base_url();?>" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link href="<?php echo PATH_CSS ?>bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>jquery.datetimepicker.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PATH_CSS ?>jquery-labelauty.css" type="text/css" media="screen" charset="utf-8" />
        <link href="assets/care/css/custom/custom.css" rel="stylesheet">
    	<title>Health-Links - <?php echo ($this->uri->segment(2) == 'startInView' || $this->uri->segment(2) == 'complete2') ? 'Mailed Survey' : 'Sms Survey';?></title>

      <script src="<?php echo PATH_JS ?>jquery.js" type="text/javascript"></script>
</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" style="height: 95px;background: #fff;border: none">
        <div class="col-md-12 toppage_border"></div>
        <div class="container">
          <table class="table" width="100%">
            <tr>
              <td class="text-left">
                <a href="<?=base_url()?>">
                  <img class="img-responsive" src="assets/img/hl_logo.png" width="130px" />
                </a>
              </td>
              <td class="text-right" style="padding-top: 5px">
                <?php if($this->uri->segment(4) == 1):?>
                <img src="<?php echo PATH_IMG ?>careimg.jpg" width="200"/>
                <?php else: ?>
                <img src="<?php echo PATH_IMG ?>rnh.jpg" width="170"/>
                <?php endif; ?>
              </td>
            </tr>
          </table>
      </div>
    </nav>

    <div class="container" style="margin-top: 100px;">
	    <?=$content;?>
	  </div>

	   <footer class="<?php echo ($this->uri->segment(2) == 'complete') ? 'footer-complete' : 'footer-sep' ;?>">
      	<div class="container">
          <table width="100%">
            <tbody><tr>
              <td align="left">
                <img src="assets/img/pressganey_logo.png">
              </td>
              <td align="center"></td>
              <td style="font-size:10px;color:gray;" width="50%" align="right">
                &copy; PRESS GANEY ASSOCIATES,INC. All Rights Reserved<br>
                &copy; 2015 Health.Links. 
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </footer>

    <script>var baseURL = '<?=base_url()?>';</script>
    <script src="<?php echo PATH_JS ?>bootstrap.js"></script>
    <script src="<?php echo PATH_JS ?>jquery-labelauty.js"></script>
    <script src="node_modules/socket.io-client/socket.io.js"></script>
    <script type="text/javascript" src="assets/care/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="assets/care/js/jquery.bootstrap.wizard.min.js"></script>
    <script type="text/javascript" src="assets/care/js/common.js"></script>
    <script type="text/javascript" src="assets/care/js/care_1.js"></script>
</body>
</html>
