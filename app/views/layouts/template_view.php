<?php if(EMPTY($this->session->userdata('username'))) : redirect('auth');  endif;?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?=base_url();?>" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link href="https://fonts.googleapis.com/css?family=Miriam+Libre:400,700|Source+Sans+Pro:200,400,700,600,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="<?php echo PATH_CSS ?>style.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>bootstrap.min.css" rel="stylesheet">
        
        <link href="<?php echo PATH_CSS ?>font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>AdminLTE.min.css" rel="stylesheet">
        <link href="<?php echo PATH_CSS ?>_all-skins.min.css" rel="stylesheet">

        <link href="<?php echo PATH_CSS ?>dataTables.bootstrap.min.css" rel="stylesheet">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <script src="<?php echo PATH_JS ?>jquery.js" type="text/javascript"></script>
        
        <?php 
        $url = $this->uri->segment(1);
        switch($url)
        {
            case 'caresurvey':
            case 'report':
                echo '<link href="assets/care/css/prettify.css" rel="stylesheet">';
                echo '<link href="assets/care/css/jquery.dataTables.css" rel="stylesheet">';
                echo '<link href="assets/care/css/custom/custom.css" rel="stylesheet">';
                echo '<link href="'.PATH_CSS.'jquery.datetimepicker.css" rel="stylesheet">';
                break;
            case 'client':
                echo '<link href="assets/client/css/client.css" rel="stylesheet">';
                echo '<link href="assets/care/css/jquery.dataTables.css" rel="stylesheet">';
                break;
            case 'user_access':
                echo '<link href="assets/care/css/jquery.dataTables.css" rel="stylesheet">';
                break;
            case 'module':
                echo '<link href="assets/care/css/jquery.dataTables.css" rel="stylesheet">';
                break;

            case 'dashboard':
                echo '<link href="'.PATH_CSS.'jquery.datetimepicker.css" rel="stylesheet">';
                break;
                
            case 'carerand':
                echo '<link href="'.PATH_CSS.'jquery.datetimepicker.css" rel="stylesheet">';
                break;

            case 'careeval':
                echo '<link href="'.PATH_CSS.'global.css" rel="stylesheet">';
                echo '<script src="assets/care/js/evaluation.js" type="text/javascript"></script>';
                break;
                
            default:
                // nothing
                break;
        }
        ?>
        
        <script src="<?php echo PATH_JS ?>angular.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo PATH_JS ?>jquery-labelauty.js"></script>
        <link rel="stylesheet" href="<?php echo PATH_CSS ?>jquery-labelauty.css" type="text/css" media="screen" charset="utf-8" />
        
        <title>Health-Links</title>
    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
        
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <div class="logo">
                    <?php if(empty($this->session->userdata('photo'))):?>
                        <!-- mini logo for sidebar mini 50x50 pixels -->
                        <span class="logo-mini p-md"><img src="<?php echo PATH_IMG?>user.jpg" class="img-circle img-responsive" width="50" height="50"/></span>
                        <!-- logo for regular state and mobile devices -->
                        <img src="<?php echo PATH_IMG?>user.jpg" class="logo-lg p-t-xs img-circle img-responsive" width="40" height="40" style="float: left; margin: 0px 0px 15px 0px;"/>
                    <?php else: ?>
                        <!-- mini logo for sidebar mini 50x50 pixels -->
                        <span class="logo-mini p-md"><img src="assets/uploads/profiles/<?php echo $this->session->userdata('photo');?>" class="img-circle img-responsive" width="50" height="40"/></span>
                        <!-- logo for regular state and mobile devices -->
                        <img src="assets/uploads/profiles/<?php echo $this->session->userdata('photo');?>" class="logo-lg p-t-xs img-circle" width="40" height="45" style="float: left; margin: 0px 0px 15px 0px;"/>
                    <?php endif;?>

                    <h5 class="p-t-xs">
                        <?php echo $this->session->userdata('fullname'); ?>
                    </h5>
                </div>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="<?php echo base_url('auth/logout')?>" >
                                    <i class="fa fa-sign-out hidden-xs" aria-hidden="true"> Logout</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            ?>

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <center><span class="logo-lg  p-md"><img src="<?php echo PATH_IMG?>hl_logo_white.png" class="img-responsive" style="width: 80%"/></span></center>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">

                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <?php
                                $range = '';
                                if(isset($_GET['from']) && isset($_GET['to'])):
                                    $range = "?from=".$_GET['from']."&to=".$_GET['to']."";
                                else:
                                    $range = '';
                                endif;
                                ?>
                                <ul class="treeview-menu">
                                    <li><a href="dashboard"><i class="fa fa-circle-o"></i>Overall</a></li>
                                    <?php foreach ($this->session->userdata('client') as $value) : ?>
                                    <?php if(!EMPTY($value->client_id)): ?>
                                        <li><a href="<?php echo 'dashboard/dashboard_client/'.$value->client_id.''.$range;?>" data-toggle="tooltip" data-placement="top" title="<?php echo $value->client_description; ?>"><i class="fa fa-circle-o"></i><?php echo $value->client_name; ?></a></li>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                         
                        <!-- <?php //if(in_array("15613", $client_access)){?>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-stethoscope"></i> <span>Care</span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <?php foreach ($modules as $value) : ?>
                                        <?php echo ($value['module_code'] == 'DASH_CLNT') ? '<li><a href="dashboard/dashboard_client/15613"><i class="fa fa-circle-o"></i> Dashboard</a></li>' : ''?>
                                    <?php endforeach; ?>
                                    <?php foreach ($modules as $value) : ?>
                                        <?php echo ($value['module_code'] == 'SRVY') ? '<li><a href="caresurvey"><i class="fa fa-circle-o"></i> Survey</a></li>' : ''?>
                                        <?php echo ($value['module_code'] == 'SRVY_SETUP') ? '<li><a href="caresurvey/caresetup"><i class="fa fa-circle-o"></i> Survey Setup</a></li>' : ''?>
                                        <?php echo ($value['module_code'] == 'SRVY_RPRT') ? '<li><a href="careeval/surveyreport"><i class="fa fa-circle-o"></i> Survey Report</a></li>' : ''?>
                                        <?php echo ($value['module_code'] == 'RNDMZR') ? '<li><a href="carerand"><i class="fa fa-circle-o"></i> Randomizer</a></li>' : ''?>
                                        <?php echo ($value['module_code'] == 'RPRTS') ? 
                                            '<li><a href="#"><i class="fa fa-circle-o"></i> Reports<i class="fa fa-angle-left pull-right"></i></a>
                                                 <ul class="treeview-menu" style="display: none;">
                                                     <li><a href="report/summary"><i class="fa fa-circle-o"></i> Summary Report</a></li>
                                                     <li><a href="report/inpatient"><i class="fa fa-circle-o"></i> Inpatient</a></li>
                                                     <li><a href="report/ambulatory"><i class="fa fa-circle-o"></i> Ambulatory</a></li>
                                                 </ul>
                                            </li>
                                            ' 
                                            : ''?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php //} ?> -->
                        <!-- END OF PER CLIENT MODULE--> 

                        
                        <?php foreach($this->session->userdata('modules') as $row):?>
                        
                        <?php
                            $icon = ($row->module_icon == '') ? 'fa-circle-o' : $row->module_icon;
                            $module = '<li><a href="'.$row->module_url.'/"><i class="fa '.$icon.'"></i> '.$row->module_name.'</a></li>';
                        ?>

                        <?php if($row->parent == 0 && $row->is_menu == 1):?>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa <?php echo ($row->module_icon == '') ? 'fa-circle-o' : $row->module_icon;?>"></i> <span> <?php echo $row->module_name;?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <?php foreach($this->session->userdata('modules') as $row2):?>
                                <?php if($row2->parent == $row->module_id && $row2->is_menu == 0):?>
                                    <li><a href="<?php echo $row2->module_url;?>/"><i class="fa <?php echo ($row2->module_icon == '') ? 'fa-circle-o' : $row2->module_icon;?>"></i> <?php echo $row2->module_name;?></a></li>
                                <?php endif;?>
                                <?php endforeach;?>
                            </ul>
                        </li>
                        <?php endif;?>

                        <?php if($row->parent == 0 && $row->is_menu == 0):?>
                            <?php echo $module;?>
                        <?php endif;?>
                        
                        <?php endforeach;?>
                        <li><a href="account/"><i class="fa fa-gear"></i> Account Settings</a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- Main content -->
                <section class="content">
                    <?=$content;?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1
                </div>
                <strong>Copyright &copy; 2016 <a href="#!">Health-links</a>.</strong> All rights reserved.
            </footer>
        </div>
        <!-- ./wrapper -->   
    
    <script>var baseURL = '<?=base_url()?>';</script>
    <script src="<?php echo PATH_JS ?>bootstrap.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo PATH_JS ?>jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo PATH_JS ?>fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo PATH_JS ?>app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo PATH_JS ?>demo.js"></script>
   
    <?php 
    $url = $this->uri->segment(1);
    switch($url)
    {
        case 'caresurvey':
        case 'report':
            echo '<script type="text/javascript" src="'.PATH_JS.'jquery.datetimepicker.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/prettify.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/jquery.bootstrap.wizard.min.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/jquery.dataTables.min.js"></script>';
            echo '<script src="node_modules/socket.io-client/socket.io.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/common.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/care_1.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/care_2.js"></script>';
            break;

        case 'client':
            echo '<script type="text/javascript" src="assets/care/js/jquery.dataTables.min.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/common.js"></script>';
            echo '<script type="text/javascript" src="assets/client/js/client.js"></script>';
            break;
        case 'account':
            echo '<script type="text/javascript" src="assets/care/js/common.js"></script>';
            echo '<script type="text/javascript" src="assets/account/js/account.js"></script>';
            break;

        case 'user_access':
            echo '<script type="text/javascript" src="assets/care/js/jquery.dataTables.min.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/common.js"></script>';
            echo '<script type="text/javascript" src="assets/user_access/js/user_access.js"></script>';
            break;
        case 'module':
            echo '<script type="text/javascript" src="assets/care/js/jquery.dataTables.min.js"></script>';
            echo '<script type="text/javascript" src="assets/care/js/common.js"></script>';
            echo '<script type="text/javascript" src="assets/module/js/module.js"></script>';
            break;

        case 'dashboard':
            echo '<script type="text/javascript" src="'.PATH_JS.'jquery.datetimepicker.js"></script>';
            break;
            
        case 'carerand':
            echo '<script type="text/javascript" src="'.PATH_JS.'jquery.datetimepicker.js"></script>';
            echo '<script type="text/javascript">
                    $("#dtpicker-from-er").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-to-er").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-from-op").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-to-op").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-from-as").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-to-as").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-from-ip").datetimepicker({timepicker:false, format:"m/d/Y"});
                    $("#dtpicker-to-ip").datetimepicker({timepicker:false, format:"m/d/Y"});
                </script>
            ';
            break;

        default:
            // nothing
            break;
    }
    ?>
    <?php if($url == 'carerand'):?>
        <script type="text/javascript">
            $('#form-submit-er').submit(function(){ 
                $('#msg-wait-er').html('Please wait... <img src="<?=base_url();?>assets/img/712.gif" width="25">'); 
                $('.upload-file-er').attr('disabled', 'disabled');
            });
   
   
            $('#form-submit-op').submit(function(){ 
                $('#msg-wait-op').html('Please wait... <img src="<?=base_url();?>assets/img/712.gif" width="25">');
                $('.upload-file-op').attr('disabled', 'disabled');
            });
   
            $('#form-submit-as').submit(function(){ 
                $('#msg-wait-as').html('Please wait... <img src="<?=base_url();?>assets/img/712.gif" width="25">');
                $('.upload-file-as').attr('disabled', 'disabled');
            });
   
    
            $('#form-submit-ip').submit(function(){ 
                $('#msg-wait-ip').html('Please wait... <img src="<?=base_url();?>assets/img/712.gif" width="25">');
                $('.upload-file-ip').attr('disabled', 'disabled');
            });
        </script>
    <?php endif;?>
    <script src="assets/js/chart.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
   
    </body>
</html>

