<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
		<li class="active">Care</li>
		<li class="active">Evaluation</li>
		<li class="active">Evaluation Report</li>
    </ul>

    	<div class="content-div login-wrapper report-survery">
			<div class="survey-div">
				<h2>Survery Report</h2>
				<div class="report-wrapper">
				<!--    <form method="post" action="careeval/searchevaluation"> -->
						<div class="form">
			                <label>Client:</label>
			                <select class="form-control form__btn client-txt" name="client" id="client">
                               <option value="0">Select Client</option>
                                    <?php foreach($clients as $row) 
                                        {
                                           echo '<option value="' . $row['client_id'] . '">' . $row['client_name'] ."</option>";
                                        }
                                    ?>
                            </select>
			            </div>
			            <div class="form">
			                <label>Surveyor:</label>
			                <select class="form-control form__btn" name="surveyor" id="surveyor" disabled>
                             		<option value="0">Select Surveyor</option>
                            </select>
			            </div>
	            		<div class="form form-date">
			                <label>Date From:</label>
			                <div class="input-group">
			                    <div class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </div>
			                    <input type="date" id="start_date" name="start_date" class="form-control pull-right">
			                </div>
			            </div>
			            <div class="form form-date">
			                <label>Date To:</label>
			                <div class="input-group">
			                    <div class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </div>
			                    <input type="date" id="end_date" name="end_date" class="form-control pull-right">
			                </div><!-- /.input group -->
			            </div>								
			            <div class="buycredits-btn">
			                <button class="btn btn-primary submit-btn survey-btn" id="submit">Submit</button>
			            </div>
		    	<!--    </form> -->
				</div>
				<div class="surverytable-wrapper">
					<div class="line-txt clearfix">
					  <?php 
					     if ($search_result) { ?>
						 <h2 id="num_records"><?php echo count($search_result);?> records</h2>
					  <?php }
					      else
					      { ?>
					       
					        <h2 id="num_records">0 records</h2>
					      <?php } ?>	
					     	
					</div>
					<div class="container">
						<table class="table table-bordered survey-tble">
						  <thead>
						    <tr>
						      <th>Client</th>
						      <th>Surveyor</th>
						      <th>Evaluation Date</th>
						      <th>View</th>
						    </tr>
						  </thead>
						  <tbody id="display_records">
						  		<?php
						  		      if ($search_result) 
						  		      {
						  		      	foreach($search_result as $row) 
						  		        {
						  		        	echo "<tr><td>" . $row['client_name'] . "</td>"; 
						  		        	echo "<td>" . $row['surveyor_name'] . "</td>";
						  		        	echo "<td>" . $row['completeddate']  . "</td>";
						  		        ?>
						  		        	<td><a href="careeval/viewreport?code=<?php echo $row['code'];?>">View Report</a></td></tr>
						  		        <?php
						  		        }
						  		      }

						  		   ?>			 
						   
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

</div>

