<style type="text/css">
textarea {
    resize: none;
}
.white-bg{
    background: #fff !important;
}
.green-bg {
    background: #1ABB9C;
} 
.darkgreen-bg {
    background: #95BE8C;
} 
.darkgrey-bg {
    background: #808080;
} 
.violet-bg{
    background: #D2ACCD !important;
} 

</style>
<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li class="active">Care</li>
    <li class="active">Evaluation</li>
    </ul>

      <div class="page-container">
         <form method="POST" action="careeval/submiteditevaluation" name="eval">
            <div class="p-t-md">
                <?php if (isset($_SESSION['flashmessage']))
                { ?>
                    <center><font color="red" size="2"><b><?php echo $_SESSION['flashmessage'];?></b></font></center>
                <?php }
                ?>
                <center><h2 class="quality-evaluation">Quality Evaluation Form</h2></center>
                <div class="container m-t-xl">
                    <div class="content_name">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form">
                                <label>Client:</label>
                                <select class="form-control form__btn" name="client" id="client">
                                    <option value="0">Select Client</option>
                                    <?php foreach($clients as $row) 
                                        {
                                          if ($row['client_id'] == $ccdata->client)
                                          {
                                           echo '<option value="' . $row['client_id'] . '" selected>' . $row['client_name'] ."</option>";
                                          }
                                          else
                                          {
                                            echo '<option value="' . $row['client_id'] . '">' . $row['client_name'] ."</option>";
                                          }
                                        }
                                    ?>

                                </select>
                            </div>
                            <div class="form">
                                <label>Surveyor Name:</label>
                                <select class="form-control form__btn" name="surveyor" id="surveyor">
                                    <option value="0">Select Surveyor</option>
                                     <?php foreach($surveyors as $row) 
                                        {
                                          if ($row['surveyor_id'] == $ccdata->surveyor)
                                          {
                                           echo '<option value="' . $row['surveyor_id'] . '" selected>' . $row['surveyor_name'] ."</option>";
                                          }
                                          else
                                          {
                                            echo '<option value="' . $row['surveyor_id'] . '">' . $row['surveyor_name'] ."</option>";
                                          }
                                        }
                                    ?>
                                </select>
                            </div>
                             <div class="form">
                                <label>Called Number:</label>
                                <input class="form-control form__btn" type="text" name="mobile" value="<?php echo $ccdata->mobile;?>" required>
                            </div>
                             <div class="form">
                                <label>Evaluation Date:</label>
                                <input class="form-control form__btn" type="text" name="completeddate" value="<?php echo $ccdata->completeddate;?>" readonly>
                            </div>
                             <div class="form">
                                <label>Evaluator:</label>
                                <input class="form-control form__btn" type="text" name="completedby" value="<?php echo $evaluator_name;?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            
                                <div class="form">
                                    <label>Achievable Score:</label>
                                    <input class="form-control forms__btn" type="text" name="ach_score" id="ach_score" value="" readonly size="1">
                                </div>
                                <div class="form">
                                    <label>Total Score:</label>
                                    <input class="form-control forms__btn" type="text" name="total_score" id="total_score" value="" readonly size="1">
                                </div>
                                <div class="form">
                                    <label class="critical-txt">No. of Critical Errors:</label>
                                    <input class="form-control forms__btn" type="text" name="critical_error" id="critical_error" value="" readonly size="1">
                                </div>
                                <div class="form">
                                    <label>Final Score:</label>
                                    <input class="form-control forms__btn" type="text" name="final_score" id="final_score" value="" readonly size="1">
                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
      </div>

</div>
<div class="table-responsive p-md m-t-lg">
                           <table class="table table-striped">
                              <thead>
                                <tr class="blue-bg">
                                  <th><strong>#Attribute</strong></th>
                                  <th class="complaint-txt"><strong>Complaint</strong></th>
                                  <th class="scored-txt"><strong>Scored</strong></th>
                                  <th class="achievable-txt"><strong>Achievable</strong></th>
                                  <th class="comments-txt"><strong>Comments</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="darkblue-bg">
                                  <th><strong>1. INTRODUCTION</strong></th>
                                  <td>YES / NO</td>
                                  <td class="input-intro"><input type="text" name="intros" id="intros" value="" readonly size="1"></td>
                                  <td class="input-intro"><input type="text" name="introc" id="introc" value="" readonly size="1"></td>
                                  <td></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content ">
                                  1.1 Focused/prepared for call:<br/>
                                  [Indication for not being prepared for
                                  the call is the silence after the answer
                                  of the respondent or the stumbling while
                                  giving the intro.]
                                  </th>
                                  <td class="text-center">
                                    <select name="intro1" id="i1">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="intros1" id="intros1" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="introc1" id="introc1" value="" readonly size="1"></td>
                                  <td><textarea name="introsm1"><?php echo $ccdata->q1_1_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    1.2 Proactive in the introduction: <br/>
                                    [If the respondent for example mentioned his name
                                    before being asked; or gave any direct information;
                                    this information shall not be asked again in the same context]
                                  </th>
                                  <td class="text-center">
                                    <select name="intro2" id="i2">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="intros2" id="intros2" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="introc2" id="introc2" value="" readonly size="1"></td>       
                                  <td><textarea name="introsm2"><?php echo $ccdata->q1_2_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content content-red">
                                    1.3 Identifies self and hospital correctly: <br/>
                                    [Following the script and ensures components of intro.
                                    is covered. If mentioned a wrong hospital name,
                                    then the score of the call is ZERO]
                                  </th>
                                  <td class="text-center">
                                    <select name="intro3" id="i3">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="intros3" id="intros3" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="introc3" id="introc3" value="" readonly size="1"></td>
                                  <td><textarea name="introsm3"><?php echo $ccdata->q1_3_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content content-red">
                                    1.4 Confirms speaking to correct respondent <br/>
                                    & obtains appropriate permissions. (HIPAA compliant):
                                    [If broke this rule of continuing with appropriate person following HIPAA rules; the score of this call will be ZERO]
                                  </th>
                                 <td class="text-center">
                                    <select name="intro4" id="i4">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="intros4" id="intros4" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="introc4" id="introc4" value="" readonly size="1"></td>
                                  <td><textarea name="introsm4"><?php echo $ccdata->q1_4_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    <strong>1.5 Did Surveyor refer to service provider by saying: <br/>
                                    "Our records show that you were discharged from [Hospital Name]</strong>
                                  </th>
                                 <td class="text-center">
                                    <select name="intro5" id="i5">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="intros5" id="intros5" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="introc5" id="introc5" value="" readonly size="1"></td>
                                  <td><textarea name="introsm5"><?php echo $ccdata->q1_5_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    <strong>1.6 Did Surveyor confirm visit date by saying:<br/> 
                                    "Our records show that you were discharged from [Hospital Name}  on [DATE] </strong>
                                  </th>
                                 <td class="text-center">
                                    <select name="intro6" id="i6">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="intros6" id="intros6" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="introc6" id="introc6" value="" readonly size="1"></td>
                                  <td><textarea name="introsm6"><?php echo $ccdata->q1_6_c;?></textarea></td>
                                </tr>
                                <tr class="green-bg">
                                  <th><strong>2. Survey</strong></th>
                                  <td></td>
                                  <td class="input-intro"><input type="text" name="surveys" id="surveys" value="" readonly size="1"></td>
                                  <td class="input-intro"><input type="text" name="surveyc" id="surveyc" value="" readonly size="1"></td>
                                  <td></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    2.1 Interviewer DOES NOT add fillers to Survey Question:<br/>
                                    [Minor wording additions/omissions/changes
                                    that don't effect the meaning of a statement
                                    or question or cause the respondent to be confused,
                                    but are noticeably different from the script
                                    (Ex:  Scripts says, "Was this your first hospital
                                    stay at this hospital," but interviewer says,
                                    "Was this the first time you stayed at this hospital."
                                    - This is acceptable if used less than three
                                    times in the survey. If more than 3 times,
                                    this field will fail in achieving the desired score.
                                  </th>
                                  <td class="text-center">
                                   <select name="survey1" id="s1">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="surveys1" id="surveys1" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="surveyc1" id="surveyc1" value="" readonly size="1"></td>
                                  <td><textarea name="surveym1"><?php echo $ccdata->q2_1_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    2.2 Follows script: <br/>
                                    [Significant wording additions/omissions/changes
                                    that effect the meaning of a question or statement,
                                    or causes the respondent to misunderstand or be confused]
                                  </th>
                                  <td class="text-center">
                                    <select name="survey2" id="s2">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="surveys2" id="surveys2" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="surveyc2" id="surveyc2" value="" readonly size="1"></td>
                                  <td><textarea name="surveym2"><?php echo $ccdata->q2_2_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    2.3 Provides clarification for respondent without definition: <br/>
                                    [Clarifications should only be made if they don't
                                    effect neutrality or the respondent's
                                    understanding of the question or statement]
                                  </th>
                                  <td class="text-center">
                                      <select name="survey3" id="s3">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="surveys3" id="surveys3" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="surveyc3" id="surveyc3" value="" readonly size="1"></td>
                                  <td><textarea name="surveym3"><?php echo $ccdata->q2_3_c;?></textarea></td>
                                </tr>
                                <tr class="darkgrey-bg">
                                  <th><strong>3. INTERVIEWING SKILLS</strong></th>
                                  <td></td>
                                  <td class="input-intro"><input type="text" name="inters" id="inters" value="" readonly size="1"></td>
                                  <td class="input-intro"><input type="text" name="interc" id="interc" value="" readonly size="1"></td>
                                  <td></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content"><strong>3.1 Speaks clearly & confidently</strong></th>
                                  <td class="text-center">
                                  <select name="inter1" id="in1">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters1" id="inters1" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc1" id="interc1" value="" readonly size="1"></td>                          
                                  <td><textarea name="interm1"><?php echo $ccdata->q3_1_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">3.2 Maintains good tone & rate of speech</th>
                                  <td class="text-center">
                                  <select name="inter2" id="in2">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters2" id="inters2" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc2" id="interc2" value="" readonly size="1"></td> 
                                  <td><textarea name="interm2"><?php echo $ccdata->q3_2_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content"><strong>3.3 Shows empathy when necessary</strong></th>
                                  <td class="text-center">
                                  <select name="inter3" id="in3">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters3" id="inters3" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc3" id="interc3" value="" readonly size="1"></td> 
                                  <td><textarea name="interm3"><?php echo $ccdata->q3_3_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.4 Displays active listening skills<br/>
                                    while maintaining call control:
                                    [Respondent's tone suggests he/she is frustrated
                                    by survey for some reason and interviewer gives
                                    appropriate response (Ex:  "We're almost done,"
                                    "Your answers are actually very helpful," etc.]
                                  </th>
                                 <td class="text-center">
                                  <select name="inter4" id="in4">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters4" id="inters4" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc4" id="interc4" value="" readonly size="1"></td> 
                                  <td><textarea name="interm4"><?php echo $ccdata->q3_4_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.5 Mentioning Respondent Name:<br/>
                                    [Mentions the respondent name throughout the call
                                    appropriately. Once at the beginning, in the middle
                                    of the call and at the end of the call]
                                  </th>
                                 <td class="text-center">
                                  <select name="inter5" id="in5">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters5" id="inters5" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc5" id="interc5" value="" readonly size="1"></td> 
                                  <td><textarea name="interm5"><?php echo $ccdata->q3_5_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.6 Does not argue with patients:<br/>
                                    [No arguing or fighting over phone with patient.
                                    If patient cursed or said whatever that could be
                                    frustrating; respond following the rules of
                                    asking patient to commit to the etiquettes
                                    and code of conduct through the call, else shall
                                    inform patient that she will hang up in return of no good response]
                                  </th>
                                   <td class="text-center">
                                  <select name="inter6" id="in6">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters6" id="inters6" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc6" id="interc6" value="" readonly size="1"></td> 
                                  <td><textarea name="interm6"><?php echo $ccdata->q3_6_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.7 Does not Open or participate in unnecessary<br/>
                                    dialogues and does not participate negatively while
                                    receiving patient's comments:
                                    [E.g. Patient is saying that the physician has
                                    not visited him. The surveyor responds: Really, how possible.
                                    In this way we are causing extreme dissatisfaction
                                    to patients and this could lead to shut down of business]
                                  </th>
                                  <td class="text-center">
                                  <select name="inter7" id="in7">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters7" id="inters7" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc7" id="interc7" value="" readonly size="1"></td> 
                                  <td><textarea name="interm7"><?php echo $ccdata->q3_7_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.8 Maintains Confidentiality:<br/>
                                    [Any secret or confidential information leakage,
                                    would lead to terminating employee contract]
                                  </th>
                                <td class="text-center">
                                  <select name="inter8" id="in8">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters8" id="inters8" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc8" id="interc8" value="" readonly size="1"></td> 
                                  <td><textarea name="interm8"><?php echo $ccdata->q3_8_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.9 Handles objections effectively:<br/>
                                    [Respondent may object to continue for several
                                    reasons and may ask questions that shall be answered
                                    and guided properly. If does not know an answer for certain
                                    question; it was checked with the supervisor.
                                    If felt respondent does not want to continue the survey
                                    and feels so disturbed with it, we can offer to end the call]
                                  </th>
                                  <td class="text-center">
                                  <select name="inter9" id="in9">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters9" id="inters9" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc9" id="interc9" value="" readonly size="1"></td> 
                                  <td><textarea name="interm9"><?php echo $ccdata->q3_9_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    3.10 Professional communication:<br/>
                                    Does not interrupt and uses professional language
                                  </th>
                                 <td class="text-center">
                                  <select name="inter10" id="in10">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters10" id="inters10" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc10" id="interc10" value="" readonly size="1"></td> 
                                  <td><textarea name="interm10"><?php echo $ccdata->q3_10_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content content-red">
                                    3.11 Does not Lead respondent to answer choice:
                                    [Must not guide patient to choose an answer. E.g.,
                                    So do you want me to put 'very poor'? Or 'are you sure
                                    you do not want to answer this question?']
                                  </th>
                                  <td class="text-center">
                                  <select name="inter11" id="in11">
                                      <option value="Yes">Yes
                                      <option value="No">No
                                      <option value="N/A" selected>N/A
                                  </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="inters11" id="inters11" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="interc11" id="interc11" value="" readonly size="1"></td> 
                                  <td><textarea name="interm11"><?php echo $ccdata->q3_11_c;?></textarea></td>
                                </tr>
                                <tr class="darkgreen-bg">
                                  <th><strong>4. VERBATIM COMMENTING</strong></th>
                                  <td></td>
                                  <td class="input-intro"><input type="text" name="verbas" id="verbas" value="" readonly size="1"></td>
                                  <td class="input-intro"><input type="text" name="verbac" id="verbac" value="" readonly size="1"></td>
                                  <td></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    4.1 Verifies answers with respondent<br/>
                                    when answers are not clear:
                                    [If the patient answer was not clear,
                                    ensures that answer is verified]
                                  </th>
                                  <td class="text-center">
                                    <select name="verba1" id="v1">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas1" id="verbas1" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac1" id="verbac1" value="" readonly size="1"></td>
                                  <td><textarea name="verbam1"><?php echo $ccdata->q4_1_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content content-red">
                                    4.2 Codes responses correctly:<br/>
                                    [Any patient answer that is logged mistakenly
                                    will result in evaluation scoring ZERO]
                                  </th>
                                  <td class="text-center">
                                    <select name="verba2" id="v2">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas2" id="verbas2" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac2" id="verbac2" value="" readonly size="1"></td>
                                  <td><textarea name="verbam2"><?php echo $ccdata->q4_2_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    4.3 Uses correct spelling:<br/>
                                    [If more than 5 spelling mistakes; score will be lost]
                                  </th>
                                  <td class="text-center">
                                     <select name="verba3" id="v3">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas3" id="verbas3" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac3" id="verbac3" value="" readonly size="1"></td>
                                  <td><textarea name="verbam3"><?php echo $ccdata->q4_3_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    4.4 Applies appropriate punctuation:<br/>
                                    [If punctuation is not properly used with
                                    3 mistakes or more, score will be lost]
                                  </th>
                                  <td class="text-center">
                                  <select name="verba4" id="v4">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas4" id="verbas4" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac4" id="verbac4" value="" readonly size="1"></td>
                                  <td><textarea name="verbam4"><?php echo $ccdata->q4_4_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    4.5 Captures respondent feedback accurately (verbatim):<br/>
                                    [All feedback should be captured verbatim]
                                  </th>
                                  <td class="text-center">
                                   <select name="verba5" id="v5">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas5" id="verbas5" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac5" id="verbac5" value="" readonly size="1"></td>
                                  <td><textarea name="verbam5"><?php echo $ccdata->q4_5_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content"><strong>4.6 Comment verified with respondent</strong></th>
                                  <td class="text-center">
                                  <select name="verba6" id="v6">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas6" id="verbas6" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac6" id="verbac6" value="" readonly size="1"></td>
                                  <td><textarea name="verbam6"><?php echo $ccdata->q4_6_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    <strong>4.7 Spelling of names/medications/facilities<br/> 
                                    verified with respondent when they are not clear</strong>
                                  </th>
                                  <td class="text-center">
                                <select name="verba7" id="v7">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas7" id="verbas7" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac7" id="verbac7" value="" readonly size="1"></td>
                                  <td><textarea name="verbam7"><?php echo $ccdata->q4_7_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    <strong>4.8 Comment rated correctly:<br/>
                                    [Currently this field is not being used. Will apply soon]</strong>
                                  </th>
                                  <td class="text-center">
                                  <select name="verba8" id="v8">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="verbas8" id="verbas8" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="verbac8" id="verbac8" value="" readonly size="1"></td>
                                  <td><textarea name="verbam8"><?php echo $ccdata->q4_8_c;?></textarea></td>
                                </tr>
                                <tr class="violet-bg">
                                  <th>
                                    <strong>5. Call Closure</strong>
                                  </th>
                                  <td></td>
                                  <td class="input-intro"><input type="text" name="closures" id="closures" value="" readonly size="1"></td>
                                  <td class="input-intro"><input type="text" name="closurec" id="closurec" value="" readonly size="1"></td>
                                  <td></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    5.1 Did the surveyor say: "We appreciate your feedback Mr___ or Mrs___"
                                  </th>
                                  <td class="text-center">
                                    <select name="closure1" id="clo1">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="closures1" id="closures1" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="closurec1" id="closurec1" value="" readonly size="1"></td>
                                  <td><textarea name="closurem1"><?php echo $ccdata->q5_1_c;?></textarea></td>
                                </tr>
                                <tr class="white-bg">
                                  <th class="th-content">
                                    5.2 Did the surveyor say: "Thank you for your time, we wish you good day"
                                  <td class="text-center">
                                       <select name="closure2" id="clo2">
                                        <option value="Yes">Yes
                                        <option value="No">No
                                        <option value="N/A" selected>N/A
                                    </select>
                                  </td>
                                  <td class="text-center"><input type="text" name="closures2" id="closures2" value="" readonly size="1"></td>
                                  <td class="text-center"><input type="text" name="closurec2" id="closurec2" value="" readonly size="1"></td>
                                  <td><textarea name="closurem2"><?php echo $ccdata->q5_2_c;?></textarea></td>
                                </tr>
                              </tbody>
                           </table>
                           <div class="buycredits-btn">
                             <input type="hidden" name="code" value="<?php echo $code;?>">
                              <button class="btn btn-primary submit-btn">Submit</button>
                          </div>
                     
</div>
         </form>
<script type="text/javascript">
$(document).ready(function() {

    document.getElementById("i1").value = "<?php echo $ccdata->q1_1;?>";
    $("#i1").change();
    document.getElementById("i2").value = "<?php echo $ccdata->q1_2;?>";
    $("#i2").change();
    document.getElementById("i3").value = "<?php echo $ccdata->q1_3;?>";
    $("#i3").change();
    document.getElementById("i4").value = "<?php echo $ccdata->q1_4;?>";
    $("#i4").change();
    document.getElementById("i5").value = "<?php echo $ccdata->q1_5;?>";
    $("#i5").change();
    document.getElementById("i6").value = "<?php echo $ccdata->q1_6;?>";
    $("#i6").change();
    document.getElementById("s1").value = "<?php echo $ccdata->q2_1;?>";
    $("#s1").change();
    document.getElementById("s2").value = "<?php echo $ccdata->q2_2;?>";
    $("#s2").change();
    document.getElementById("s3").value = "<?php echo $ccdata->q2_3;?>";
    $("#s3").change();
    document.getElementById("in1").value = "<?php echo $ccdata->q3_1;?>";
    $("#in1").change();
    document.getElementById("in2").value = "<?php echo $ccdata->q3_2;?>";
    $("#in2").change();
    document.getElementById("in3").value = "<?php echo $ccdata->q3_3;?>";
    $("#in3").change();
    document.getElementById("in4").value = "<?php echo $ccdata->q3_4;?>";
    $("#in4").change();
    document.getElementById("in5").value = "<?php echo $ccdata->q3_5;?>";
    $("#in5").change();
    document.getElementById("in6").value = "<?php echo $ccdata->q3_6;?>";
    $("#in6").change();
    document.getElementById("in7").value = "<?php echo $ccdata->q3_7;?>";
    $("#in7").change();
    document.getElementById("in8").value = "<?php echo $ccdata->q3_8;?>";
    $("#in8").change();
    document.getElementById("in9").value = "<?php echo $ccdata->q3_9;?>";
    $("#in9").change();
    document.getElementById("in10").value = "<?php echo $ccdata->q3_10;?>";
    $("#in10").change();
    document.getElementById("in11").value = "<?php echo $ccdata->q3_11;?>";
    $("#in11").change();
    document.getElementById("v1").value = "<?php echo $ccdata->q4_1;?>";
    $("#v1").change();
    document.getElementById("v2").value = "<?php echo $ccdata->q4_2;?>";
    $("#v2").change();
    document.getElementById("v3").value = "<?php echo $ccdata->q4_3;?>";
    $("#v3").change();
    document.getElementById("v4").value = "<?php echo $ccdata->q4_4;?>";
    $("#v4").change();
    document.getElementById("v5").value = "<?php echo $ccdata->q4_5;?>";
    $("#v5").change();
    document.getElementById("v6").value = "<?php echo $ccdata->q4_6;?>";
    $("#v6").change();
    document.getElementById("v7").value = "<?php echo $ccdata->q4_7;?>";
    $("#v7").change();
    document.getElementById("v8").value = "<?php echo $ccdata->q4_8;?>";
    $("#v8").change();
    document.getElementById("clo1").value = "<?php echo $ccdata->q5_1;?>";
    $("#clo1").change();
    document.getElementById("clo2").value = "<?php echo $ccdata->q5_2;?>";
    $("#clo2").change();

});
</script>
