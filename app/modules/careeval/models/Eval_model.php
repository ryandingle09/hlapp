<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Eval_model extends CI_Model
{
   
    public function get_clients()
    {
        $qstring = "SELECT clients FROM evaluator WHERE evaluator_id = '" . $this->session->id . "'";
        $query = $this->db->query($qstring);
        $result = $query->row();

        if ($result)
        {
            $client_array = explode( ',', $result->clients);
            $this->db->select('client_id,client_name');
            $this->db->from('clients');
            $this->db->where_in('client_id', $client_array);
            $query = $this->db->get();
            $result = $query->result_array(); 
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function get_client_surveyor($client = "")
    {
        $qstring = "SELECT surveyor_id,surveyor_name FROM surveyor WHERE (clients like '" . $client  . ",%') or (clients like '%," . $client . ",%') or  (clients like '%," . $client . "')";
        $query = $this->db->query($qstring);
        $result = $query->result_array();   

        return $result;
    }

    public function insert_evaluation()
    {
       $insert_data = array(
        'code'   => rand(10000000,99999999),
        'status' => 'Completed',
        'addedby'=> $this->session->id,
        'addeddate' => date("Y-m-d H:i:s"),
        'amendby'=> $this->session->id,
        'amenddate' => date("Y-m-d H:i:s"),
        'completedby'=> $this->session->id,
        'completeddate' => date("Y-m-d H:i:s"),
        'client' => $this->input->post('client'),
        'surveyor' => $this->input->post('surveyor'),
        'mobile' => $this->input->post('mobile'),
        'q1_1' => $this->input->post('intro1'),
        'q1_2' => $this->input->post('intro2'),
        'q1_3' => $this->input->post('intro3'),
        'q1_4' => $this->input->post('intro4'),
        'q1_5' => $this->input->post('intro5'),
        'q1_6' => $this->input->post('intro6'),
        'q1_1_c' => $this->input->post('introsm1'),
        'q1_2_c' => $this->input->post('introsm2'),
        'q1_3_c' => $this->input->post('introsm3'),
        'q1_4_c' => $this->input->post('introsm4'),
        'q1_5_c' => $this->input->post('introsm5'),
        'q1_6_c' => $this->input->post('introsm6'),
        'q2_1' => $this->input->post('survey1'),
        'q2_2' => $this->input->post('survey2'),
        'q2_3' => $this->input->post('survey3'),
        'q2_1_c' => $this->input->post('surveym1'),
        'q2_2_c' => $this->input->post('surveym2'),
        'q2_3_c' => $this->input->post('surveym3'),
        'q3_1' => $this->input->post('inter1'),
        'q3_2' => $this->input->post('inter2'),
        'q3_3' => $this->input->post('inter3'),
        'q3_4' => $this->input->post('inter4'),
        'q3_5' => $this->input->post('inter5'),
        'q3_6' => $this->input->post('inter6'),
        'q3_7' => $this->input->post('inter7'),
        'q3_8' => $this->input->post('inter8'),
        'q3_9' => $this->input->post('inter9'),
        'q3_10' => $this->input->post('inter10'),
        'q3_11' => $this->input->post('inter11'),
        'q3_1_c' => $this->input->post('interm1'),
        'q3_2_c' => $this->input->post('interm2'),
        'q3_3_c' => $this->input->post('interm3'),
        'q3_4_c' => $this->input->post('interm4'),
        'q3_5_c' => $this->input->post('interm5'),
        'q3_6_c' => $this->input->post('interm6'),
        'q3_7_c' => $this->input->post('interm7'),
        'q3_8_c' => $this->input->post('interm8'),
        'q3_9_c' => $this->input->post('interm9'),
        'q3_10_c' => $this->input->post('interm10'),
        'q3_11_c' => $this->input->post('interm11'),
        'q4_1' => $this->input->post('verba1'),
        'q4_2' => $this->input->post('verba2'),
        'q4_3' => $this->input->post('verba3'),
        'q4_4' => $this->input->post('verba4'),
        'q4_5' => $this->input->post('verba5'),
        'q4_6' => $this->input->post('verba6'),
        'q4_7' => $this->input->post('verba7'),
        'q4_8' => $this->input->post('verba8'),
        'q4_1_c' => $this->input->post('verbam1'),
        'q4_2_c' => $this->input->post('verbam2'),
        'q4_3_c' => $this->input->post('verbam3'),
        'q4_4_c' => $this->input->post('verbam4'),
        'q4_5_c' => $this->input->post('verbam5'),
        'q4_6_c' => $this->input->post('verbam6'),
        'q4_7_c' => $this->input->post('verbam7'),
        'q4_8_c' => $this->input->post('verbam8'),
        'q5_1' => $this->input->post('closure1'),
        'q5_2' => $this->input->post('closure2'),
        'q5_1_c' => $this->input->post('closurem1'),
        'q5_2_c' => $this->input->post('closurem2'),  

        );

      $this->db->insert('cc_evaluation', $insert_data); 

    }

    public function get_search_cc_evaluation($client,$surveyor,$start_date,$end_date)
    {
        $qstring = "";
        if ($client != "")
        {
           $qstring =  " client = '" . $client . "'";
        }
         if ($surveyor != "")
        {
          if ($qstring == "") 
          {
           $qstring =  " surveyor = '" . $surveyor . "'";
          }
          else
          {
            $qstring =  $qstring . " and surveyor = '" . $surveyor . "'";
          }

        }

        if  (($start_date != "") && ($end_date != ""))
        {
           if ($qstring == "") 
          {
           $qstring =  " ( (completeddate >= '" . $start_date . "') and (completeddate <= '" . $end_date . " 11:59:59'))";
          }
          else
          {
            $qstring =  $qstring . " and ( (completeddate >= '" . $start_date . "') and (completeddate <= '" . $end_date . " 11:59:59'))";
          }
        }
       
        if ($qstring == "")
        {
          $qstring = "SELECT client_name,surveyor_name,completeddate,code FROM cc_evaluation a join clients b on client = client_id join surveyor c on a.surveyor = c.surveyor_id ";
        }
        else
        {
           $qstring = "SELECT client_name,surveyor_name,completeddate,code FROM cc_evaluation a join clients b on client = client_id join surveyor c on a.surveyor = c.surveyor_id where " . $qstring;
        }
        
        $query = $this->db->query($qstring);
        $result = $query->result_array();   

        return $result;
    }

    public function get_cc_evaluation_row($code)
    {
        $qstring = "SELECT * FROM cc_evaluation where code = '" . $code . "'";
        $query = $this->db->query($qstring);
        $result = $query->row();

        return $result;
    }

    public function get_surveyor_name($surveyor)
    {
        $qstring = "SELECT * FROM surveyor where surveyor_id = '" . $surveyor . "'";
        $query = $this->db->query($qstring);
        $result = $query->row();
        return $result;
    }

    public function get_user_name($addedby)
    {
        $qstring = "SELECT * FROM userinfo where employee_id = '" . $addedby . "'";
        $query = $this->db->query($qstring);
        $result = $query->row();

        return $result;
    }

    public function update_evaluation()
    {
        $update_data = array(
        'status' => 'Completed',
        'amendby'=> $this->session->id,
        'amenddate' => date("Y-m-d H:i:s"),
        'client' => $this->input->post('client'),
        'surveyor' => $this->input->post('surveyor'),
        'mobile' => $this->input->post('mobile'),
        'q1_1' => $this->input->post('intro1'),
        'q1_2' => $this->input->post('intro2'),
        'q1_3' => $this->input->post('intro3'),
        'q1_4' => $this->input->post('intro4'),
        'q1_5' => $this->input->post('intro5'),
        'q1_6' => $this->input->post('intro6'),
        'q1_1_c' => $this->input->post('introsm1'),
        'q1_2_c' => $this->input->post('introsm2'),
        'q1_3_c' => $this->input->post('introsm3'),
        'q1_4_c' => $this->input->post('introsm4'),
        'q1_5_c' => $this->input->post('introsm5'),
        'q1_6_c' => $this->input->post('introsm6'),
        'q2_1' => $this->input->post('survey1'),
        'q2_2' => $this->input->post('survey2'),
        'q2_3' => $this->input->post('survey3'),
        'q2_1_c' => $this->input->post('surveym1'),
        'q2_2_c' => $this->input->post('surveym2'),
        'q2_3_c' => $this->input->post('surveym3'),
        'q3_1' => $this->input->post('inter1'),
        'q3_2' => $this->input->post('inter2'),
        'q3_3' => $this->input->post('inter3'),
        'q3_4' => $this->input->post('inter4'),
        'q3_5' => $this->input->post('inter5'),
        'q3_6' => $this->input->post('inter6'),
        'q3_7' => $this->input->post('inter7'),
        'q3_8' => $this->input->post('inter8'),
        'q3_9' => $this->input->post('inter9'),
        'q3_10' => $this->input->post('inter10'),
        'q3_11' => $this->input->post('inter11'),
        'q3_1_c' => $this->input->post('interm1'),
        'q3_2_c' => $this->input->post('interm2'),
        'q3_3_c' => $this->input->post('interm3'),
        'q3_4_c' => $this->input->post('interm4'),
        'q3_5_c' => $this->input->post('interm5'),
        'q3_6_c' => $this->input->post('interm6'),
        'q3_7_c' => $this->input->post('interm7'),
        'q3_8_c' => $this->input->post('interm8'),
        'q3_9_c' => $this->input->post('interm9'),
        'q3_10_c' => $this->input->post('interm10'),
        'q3_11_c' => $this->input->post('interm11'),
        'q4_1' => $this->input->post('verba1'),
        'q4_2' => $this->input->post('verba2'),
        'q4_3' => $this->input->post('verba3'),
        'q4_4' => $this->input->post('verba4'),
        'q4_5' => $this->input->post('verba5'),
        'q4_6' => $this->input->post('verba6'),
        'q4_7' => $this->input->post('verba7'),
        'q4_8' => $this->input->post('verba8'),
        'q4_1_c' => $this->input->post('verbam1'),
        'q4_2_c' => $this->input->post('verbam2'),
        'q4_3_c' => $this->input->post('verbam3'),
        'q4_4_c' => $this->input->post('verbam4'),
        'q4_5_c' => $this->input->post('verbam5'),
        'q4_6_c' => $this->input->post('verbam6'),
        'q4_7_c' => $this->input->post('verbam7'),
        'q4_8_c' => $this->input->post('verbam8'),
        'q5_1' => $this->input->post('closure1'),
        'q5_2' => $this->input->post('closure2'),
        'q5_1_c' => $this->input->post('closurem1'),
        'q5_2_c' => $this->input->post('closurem2'),  
         );
        
        $this->db->where('code', $this->input->post('code'));
        $this->db->update('cc_evaluation', $update_data);

        
    }

    public function get_all_evaluation()
    {
        $qstring = "SELECT client_name,surveyor_name,completeddate,code FROM cc_evaluation a join clients b on client = client_id join surveyor c on a.surveyor = c.surveyor_id order by completeddate desc limit 0,20";
        $query = $this->db->query($qstring);
        $result = $query->result_array();   

        return $result;
    }

    

}