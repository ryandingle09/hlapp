<style>
  #overall-table {display:none;}
  .spinner {
        position: absolute;
        margin-left: 35%; 
        margin-top: 15%;
        text-align:center;
        z-index:1234;
        overflow: auto;
        width: 100px; /* width of the spinner gif */
        height: 102px; /*hight of the spinner gif +2px to fix IE8 issue */
    }
</style>
<input type="hidden" name="client_id" class="client_id" value="<?php echo $this->uri->segment(3);?>">
<input type="hidden" name="client_name" class="client_name" value="<?php echo $client_name;?>">
<center> <h2><?php echo $client_description;?></h2></center>
<div class="p-lg p-t-lg">
    <center>    
        <form class="form-inline" method="GET">
            <div class="form-group">
                <label for="email">FROM : </label>
                <div class="input-group" style="margin-left:10px;">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input name="from" type="text" id="from" value="<?php echo (isset($_GET['from'])) ? $_GET['from'] : date('m/d/Y') ;?>" class="form-control pull-right">
                </div><!-- /.input group -->
            </div>
            <div class="form-group">
                <label for="pwd">To : </label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input name="to" type="text" id="to" value="<?php echo (isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y') ;?>" class="form-control pull-right">
                </div><!-- /.input group -->
            </div>
            <button class="btn btn-primary btn-flat" id="submit" style="margin-left:10px;">Submit</button>
        </form>
    </center>
</div>
    
<div class="row p-r-lg p-l-lg">
    <div class="col-lg-12 col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title p-l-sm p-t-xs">Overall</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart" id="overall-graph">
                    <div id="barchart1" style="height: 230px; width: 100%;" width="100%" height="230"></div>
                </div>
                <div id="overall-table" style="display: none">
                    <div class="x_content">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                  <th>Service</th>
                                  <th>Surveys Conducted</th>
                                  <th>Responses</th>
                                  <th>Completes</th>
                                  <th>Comments</th>
                                </tr>
                            </thead>
                            <tbody class="types-row">
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div><!-- /.box-body -->
                
        </div>
    </div>
</div>

<div class="row p-t-sm p-b-lg">
    <center>
        <button data-show="#overall-graph" data-hide="#overall-table" class="btn btn-info btn-sm showmenigga"><i class="fa fa-bar-chart"></i>&nbsp;Show Graph</button>
        <button data-show="#overall-table" data-hide="#overall-graph" class="btn btn-info btn-sm showmenigga"><i class="fa fa-table"></i>&nbsp;&nbsp;Show Table</button>
    </center>
</div>

<div class="row p-r-lg p-l-lg">
    <div id="spinner" class="spinner" style="display:none;">
        <img id="img-spinner" src="assets/img/ajax-loader.gif" alt="Loading"/>
    </div>
    <div class="col-lg-12 col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title p-l-sm p-t-xs">TRENDING</h3>
                <div class="box-tools pull-right">
                    <form class="form-inline">
                        <div class="form-group p-r-sm">
                            <label for="year">Year:</label>
                            <input type="number" value="<?php echo date('Y'); ?>" min="1995" max="<?php echo date('Y'); ?>" class="form-control input-sm" id="year">
                        </div>
                        <label class="radio-inline"><input class="showmenigga autoclick load-graph" data-graph="month" data-show=".month_data" data-hide=".quarter_data" type="radio" name="optradio">Monthly</label>
                        <label class="radio-inline"><input class="showmenigga load-graph" data-graph="quarter" data-show=".quarter_data" data-hide=".month_data" type="radio" name="optradio" checked="checked">Quarterly</label>
                    </form>
                </div>
            </div>
            <div class="box-body month_data">
                <div class="col-lg-12 col-xs-12">
                    <div id="month_line" style="height: 300px; width: 100%;">   
                    </div>
                </div>
                <div class="table" id="month_table" style="display: none">
                    <div class="x_content">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                  <th></th>
                                  <th>JAN</th>
                                  <th>FEB</th>
                                  <th>MAR</th>
                                  <th>APR</th>
                                  <th>MAY</th>
                                  <th>JUN</th>
                                  <th>JUL</th>
                                  <th>AUG</th>
                                  <th>SEP</th>
                                  <th>OCT</th>
                                  <th>NOV</th>
                                  <th>DEC</th>
                                </tr>
                            </thead>
                            <tbody class="month_table">
                                <tr class="row_data_e">
                                    <td>Eligible</td>
                                </tr>
                                <tr class="row_data_c">
                                    <td>Surveys Conducted</td>
                                </tr>
                                <tr class="row_data_r">
                                    <td>Responses</td>
                                </tr>
                                <tr class="row_data_c2">
                                    <td>Completes</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>

            <div class="box-body quarter_data">
                <div class="col-lg-12 col-xs-12">
                    <div id="quarter_line" style="height: 300px; width: 100%">   
                    </div>
                </div>
                <div class="table" id="quarter_table" style="display: none">
                    <div class="x_content">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                  <th></th>
                                  <th>1st QUARTER</th>
                                  <th>2nd QUARTER</th>
                                  <th>3rd QUARTER</th>
                                  <th>4th QUARTER</th>
                                </tr>
                            </thead>
                            <tbody class="quarter_table">
                                <tr class="row_data_e">
                                    <td>Eligible</td>
                                </tr>
                                <tr class="row_data_c">
                                    <td>Surveys Conducted</td>
                                </tr>
                                <tr class="row_data_r">
                                    <td>Responses</td>
                                </tr>
                                <tr class="row_data_c2">
                                    <td>Completes</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row p-t-sm p-b-lg month_data" style="display: none">
    <center>
        <button data-show="#month_line" data-hide="#month_table" class="showmenigga btn btn-info btn-sm"><i class="fa fa-bar-chart"></i>&nbsp;Show Graph</button>
        <button data-show="#month_table" data-hide="#month_line" class="showmenigga btn btn-info btn-sm"><i class="fa fa-table"></i>&nbsp;&nbsp;Show Table</button>
    </center>
</div>
<div class="row p-t-sm p-b-lg quarter_data">
    <center>
        <button data-show="#quarter_line" data-hide="#quarter_table" class="showmenigga btn btn-info btn-sm"><i class="fa fa-bar-chart"></i>&nbsp;Show Graph</button>
        <button data-show="#quarter_table" data-hide="#quarter_line" class="showmenigga btn btn-info btn-sm"><i class="fa fa-table"></i>&nbsp;&nbsp;Show Table</button>
    </center>
</div>


<base href="<?=base_url();?>" />
<script type="text/javascript" src="<?php echo PATH_JS ?>canvasjs.min.js"></script>
<script>
$(function () {

    $('#spinner').show();

    $('.month_data').fadeOut();

    $('.showmenigga').click(function(){
        var dis = $(this)
        $(dis.data('show')).fadeIn('slow');
        $(dis.data('hide')).fadeOut('slow');
    });

    $("#from, #to").datetimepicker(
        {
            timepicker:false, format:"m/d/Y"
        }
    );

    var range = '';
    var client_id       = $('.client_id').val();
    var client_name     = $('.client_name').val();

    <?php if(isset($_GET['from']) && isset($_GET['to'])):?>
        range = 'from=<?php echo $_GET['from'];?>&to=<?php echo $_GET['to'];?>';
    <?php else:?>
        range = 'from='+$('#from').val()+'&to='+$('#to').val()+'';
    <?php endif;?>

    var activate_surveys = [];
    var activate_surveys2 = [];
    var activate_surveys3 = [];
    var activate_surveys4 = [];

    var response_surveys4 = [];
    var total_surveys_conducted = 0,
        total_surveys_responses = 0,
        total_surveys_completes = 0;

    // BAR GRAPH
    $.ajax({
        url : baseURL+'dashboard/ajax_request/get_survey_activated_types_by_client/'+client_id+'', 
        async: false,
        dataType: 'json',
        success : function(activated_types){

            $.each(activated_types, function(key, val){

                var url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?'+range;

                if(client_name == 'ICLDC_AD' || client_name == 'ICLDC_AA')
                    url_get = ''+val.webservice_url+'overall.php?'+range;
                if(client_name == 'CCAD')
                    if(val.survey_type_name.toLowerCase() == 'md' || val.survey_type_name.toLowerCase() == 'ou')
                        url_get = ''+val.webservice_url+'op/overall.php?'+range;
                if(client_name == 'RCH')//hosp1
                    url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?hosp=1&'+range;
                if(client_name == 'RNH')//hosp2
                    url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?hosp=2&'+range;
                if(client_name == 'AHD')
                    url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'service/?type=overall&'+range;

                $.ajax({
                    url : url_get, 
                    async: false,
                    dataType: 'json',
                    success : function(data){
                        if(data instanceof Array)
                        {
                            $.each(data, function(kel, result){

                                if(client_name == 'CCAD')
                                {
                                    result.Dept = ''+val.survey_type_name+' - '+val.description+'';
                                }
                                if(client_name == 'AHD')
                                {
                                    result.Dept = ''+val.survey_type_name+' - '+val.description+'';
                                }

                                if(!result.Comments)
                                {
                                    $.ajax({
                                        url : ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'service/?type=comments&'+range, 
                                        async: false,
                                        dataType: 'json',
                                        success : function(data2){
                                            if(data2 instanceof Array)
                                            {
                                                $.each(data2, function(kel, result2){
                                                    result.Comments = result2.comments;
                                                });
                                            }
                                            else
                                            {
                                                result.Comments = data2.comments;
                                            }
                                        }
                                    });
                                }

                                total_surveys_conducted         = parseInt(total_surveys_conducted) + parseInt(result.Surveys_Conducted);
                                total_surveys_responses         = parseInt(total_surveys_responses) + parseInt(result.Responses);
                                total_surveys_completes         = parseInt(total_surveys_completes) + parseInt(result.Completes);

                                var percent                     = Math.round(parseInt(result.Responses) / parseInt(result.Surveys_Conducted) * 100);
                                var percent2                    = Math.round(parseInt(result.Completes) / parseInt(result.Responses) * 100);

                                if(isNaN(percent)) percent      = 0;
                                if(isNaN(percent2)) percent2    = 0;

                                var row_data = '<tr>';
                                    row_data += '<td><a href="'+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+result.Comments+'">'+result.Dept+'</a></td>';
                                    row_data += '<td>'+result.Surveys_Conducted+'</td>';
                                    row_data += '<td>'+result.Responses+' <span style="font-size:10px;"><i>( '+percent+'% )</i></td>';
                                    row_data += '<td>'+result.Completes+' <span style="font-size:10px;"><i>( '+percent2+'% )</i></td>';
                                    row_data += '<td>'+result.Comments+'</td>';
                                    row_data += '</tr>';
                                $('.types-row').append(row_data);

                                activate_surveys.push({
                                    name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+result.Comments,
                                    y               : parseInt(result.Eligible_Data),
                                    eligible        : result.Eligible_Data,
                                    conducted       : result.Surveys_Conducted,
                                    responses       : result.Responses,
                                    completes       : result.Completes,
                                    label           : ''+result.Dept+'',
                                    name2           : ''+val.survey_type_name+'',
                                    name3           : ''+val.description+'',
                                    name4           : ''+result.Dept+'',
                                });

                                activate_surveys2.push({
                                    name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+result.Comments,
                                    y               : parseInt(result.Surveys_Conducted),
                                    eligible        : result.Eligible_Data,
                                    conducted       : result.Surveys_Conducted,
                                    responses       : result.Responses,
                                    completes       : result.Completes,
                                    label           : ''+result.Dept+'',
                                    name2           : ''+val.survey_type_name+'',
                                    name3           : ''+val.description+'',
                                    name4           : ''+result.Dept+'',
                                });

                                activate_surveys3.push({
                                    name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+result.Comments,
                                    y               : parseInt(result.Responses),
                                    eligible        : result.Eligible_Data,
                                    conducted       : result.Surveys_Conducted,
                                    responses       : result.Responses,
                                    completes       : result.Completes,
                                    label           : ''+result.Dept+'',
                                    name2           : ''+val.survey_type_name+'',
                                    name3           : ''+val.description+'',
                                    name4           : ''+result.Dept+'',
                                });

                                activate_surveys4.push({
                                    name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+result.Comments,
                                    y               : parseInt(result.Completes),
                                    eligible        : result.Eligible_Data,
                                    conducted       : result.Surveys_Conducted,
                                    responses       : result.Responses,
                                    completes       : result.Completes,
                                    label           : ''+result.Dept+'',
                                    name2           : ''+val.survey_type_name+'',
                                    name3           : ''+val.description+'',
                                    name4           : ''+result.Dept+'',
                                });

                            });

                            var chart = new CanvasJS.Chart("barchart1",{

                                toolTip: 
                                {
                                    shared: true,
                                },
                                data: [
                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#34495E",
                                        toolTipContent: "<a href = {name}> {name4}</a><br>Eligible Data: {eligible}",
                                        name: "Eligible Data",      
                                        dataPoints: activate_surveys
                                    },
                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#26B99A",
                                        toolTipContent: "Surveys Conducted: {conducted}",
                                        name: "Conducted",        
                                        dataPoints: activate_surveys2
                                    },
                                  
                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#777778",
                                        toolTipContent: "Responses: {responses}",
                                        name: "Responses",        
                                        dataPoints: activate_surveys3
                                    },

                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#BF89B8",
                                        toolTipContent: "Completes: {completes}",
                                        name: "Completes",      
                                        dataPoints: activate_surveys4
                                    }
                                    
                                ]
                            });

                            chart.render();
                        }
                        else
                        {

                            if(!data.Comments)
                            {
                                $.ajax({
                                    url : ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'service/?type=comments&'+range, 
                                    async: false,
                                    dataType: 'json',
                                    success : function(data2){
                                        if(data2 instanceof Array)
                                        {
                                            $.each(data2, function(kel, result2){
                                                data.Comments = result2.comments;
                                            });
                                        }
                                        else
                                        {
                                            data.Comments = data2.comments;
                                        }
                                    }
                                });
                            }

                            total_surveys_conducted         = parseInt(total_surveys_conducted) + parseInt(data.Surveys_Conducted);
                            total_surveys_responses         = parseInt(total_surveys_responses) + parseInt(data.Responses);
                            total_surveys_completes         = parseInt(total_surveys_completes) + parseInt(data.Completes);

                            var percent                     = Math.round(parseInt(data.Responses) / parseInt(data.Surveys_Conducted) * 100);
                            var percent2                    = Math.round(parseInt(data.Completes) / parseInt(data.Responses) * 100);

                            if(isNaN(percent)) percent      = 0;
                            if(isNaN(percent2)) percent2    = 0;

                            var row_data = '<tr>';
                                row_data += '<td><a href="'+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+data.Comments+'">'+val.description+'</a></td>';
                                row_data += '<td>'+data.Surveys_Conducted+'</td>';
                                row_data += '<td>'+data.Responses+' <span style="font-size:10px;"><i>( '+percent+'% )</i></td>';
                                row_data += '<td>'+data.Completes+' <span style="font-size:10px;"><i>( '+percent2+'% )</i></td>';
                                row_data += '<td>'+data.Comments+'</td>';
                                row_data += '</tr>';
                            $('.types-row').append(row_data);

                            activate_surveys.push({
                                name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+data.Comments,
                                y               : parseInt(data.Eligible_Data),
                                eligible        : data.Eligible_Data,
                                conducted       : data.Surveys_Conducted,
                                responses       : data.Responses,
                                completes       : data.Completes,
                                label           : ''+val.survey_type_name+' - '+val.description+'',
                                name2           : ''+val.survey_type_name+'',
                                name3           : ''+val.description+'',
                                name4           : ''+val.Dept+'',
                            });

                            activate_surveys2.push({
                                name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+data.Comments,
                                y               : parseInt(data.Surveys_Conducted),
                                eligible        : data.Eligible_Data,
                                conducted       : data.Surveys_Conducted,
                                responses       : data.Responses,
                                completes       : data.Completes,
                                label           : ''+val.survey_type_name+' - '+val.description+'',
                                name2           : ''+val.survey_type_name+'',
                                name3           : ''+val.description+'',
                                name4           : ''+val.Dept+'',
                            });

                            activate_surveys3.push({
                                name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+data.Comments,
                                y               : parseInt(data.Responses),
                                eligible        : data.Eligible_Data,
                                conducted       : data.Surveys_Conducted,
                                responses       : data.Responses,
                                completes       : data.Completes,
                                label           : ''+val.survey_type_name+' - '+val.description+'',
                                name2           : ''+val.survey_type_name+'',
                                name3           : ''+val.description+'',
                                name4           : ''+val.Dept+'',
                            });

                            activate_surveys4.push({
                                name            : ''+baseURL+'dashboard/dashboard_client_survey/'+val.client_id+'/'+val.survey_type_id+'/?'+range+'&comments_rated='+data.Comments,
                                y               : parseInt(data.Completes),
                                eligible        : data.Eligible_Data,
                                conducted       : data.Surveys_Conducted,
                                responses       : data.Responses,
                                completes       : data.Completes,
                                label           : ''+val.survey_type_name+' - '+val.description+'',
                                name2           : ''+val.survey_type_name+'',
                                name3           : ''+val.description+'',
                                name4           : ''+val.Dept+'',
                            });

                            var chart = new CanvasJS.Chart("barchart1",{

                                toolTip: 
                                {
                                    shared: true,
                                },
                                data: [
                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#34495E",
                                        toolTipContent: "<a href = {name}> {label}</a><br>Eligible Data: {eligible}",
                                        name: "Eligible Data",      
                                        dataPoints: activate_surveys
                                    },
                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#26B99A",
                                        toolTipContent: "Surveys Conducted: {conducted}",
                                        name: "Conducted",        
                                        dataPoints: activate_surveys2
                                    },
                                  
                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#777778",
                                        toolTipContent: "Responses: {responses}",
                                        name: "Responses",        
                                        dataPoints: activate_surveys3
                                    },

                                    {            
                                        type: "column",
                                        showInLegend: true,
                                        color: "#BF89B8",
                                        toolTipContent: "Completes: {completes}",
                                        name: "Completes",      
                                        dataPoints: activate_surveys4
                                    }
                                    
                                ]
                            });

                            chart.render();
                        }

                    }/*,
                    error : function()
                    {
                        alert('Invalid Json Response.');
                    }*/
                });

            });

        },

    });

    var year = $("#year").val();

    function loadLineGraphQuarterly(year)
    {

        var monthname       = [],
            monthname2      = [],
            monthname3      = [],
            monthname4      = [],
            quarter_name    = [],
            quarter_name2   = [],
            quarter_name3   = [],
            quarter_name4   = [],
            date            = new Date(''+year+''), 
            y               = date.getFullYear(), 
            m               = date.getMonth(),
            firstDay        = '',
            lastDay         = '',
            monthly_range   = '',
            quarter_from    = '',
            quarter_to      = '',
            quarter_1       = '',
            quarter_2       = '',
            quarter_3       = '',
            quarter_4       = '',
            quarter_range1  = '',
            quarter_range2  = '',
            quarter_range3  = '',
            quarter_range4  = '',

            m_eligible      = 0,
            m_conducted     = 0,
            m_response      = 0,
            m_complete      = 0,

            q_eligible1      = 0,
            q_conducted1     = 0,
            q_response1      = 0,
            q_completes1     = 0,

            q_eligible2      = 0,
            q_conducted2     = 0,
            q_response2      = 0,
            q_completes2     = 0,

            q_eligible3      = 0,
            q_conducted3     = 0,
            q_response3      = 0,
            q_completes3     = 0,

            q_eligible4      = 0,
            q_conducted4     = 0,
            q_response4      = 0,
            q_completes4     = 0,

            //--QUARTER  RANGE
            quarter_1   = 1,
            start_1     = new Date(y,quarter_1*3-3,1),
            end_1       = new Date(y,quarter_1*3,0),

            quarter_2   = 2,
            start_2     = new Date(y,quarter_2*3-3,1),
            end_2       = new Date(y,quarter_2*3,0),

            quarter_3   = 3,
            start_3     = new Date(y,quarter_3*3-3,1),
            end_3       = new Date(y,quarter_3*3,0),

            quarter_4   = 4,
            start_4     = new Date(y,quarter_4*3-3,1),
            end_4       = new Date(y,quarter_4*3,0);

        <?php for($c=0;$c<=11;$c++){?>
            var m_eligible_<?php echo $c;?>      = 0,
                m_conducted_<?php echo $c;?>     = 0,
                m_response_<?php echo $c;?>      = 0,
                m_completes_<?php echo $c;?>     = 0;
        <?php }?>

        
        $.ajax({
            url : baseURL+'dashboard/ajax_request/get_survey_activated_types_by_client/'+client_id+'', 
            async: false,
            dataType: 'json',
            success : function(sat){
                $.each(sat, function(key, val){

                    <?php for($a = 1;$a<=4;$a++){?>
                        quarter_range<?php echo $a;?> = 'from='+(start_<?php echo $a;?>.getMonth()+1)+'/'+start_<?php echo $a;?>.getDate()+'/'+y+'&to='+(end_<?php echo $a;?>.getMonth()+1)+'/'+end_<?php echo $a;?>.getDate()+'/'+y;

                        var url_get2 = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?'+quarter_range<?php echo $a;?>;

                        if(client_name == 'ICLDC_AD' || client_name == 'ICLDC_AA')
                            url_get2 = ''+val.webservice_url+'overall.php?'+quarter_range<?php echo $a;?>;
                        if(client_name == 'CCAD')
                            if(val.survey_type_name.toLowerCase() == 'md' || val.survey_type_name.toLowerCase() == 'ou')
                                url_get2 = ''+val.webservice_url+'op/overall.php?'+quarter_range<?php echo $a;?>;
                        if(client_name == 'RCH')//hosp1
                            url_get2 = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?hosp=1&'+quarter_range<?php echo $a;?>;
                        if(client_name == 'RNH')//hosp2
                            url_get2 = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?hosp=2&'+quarter_range<?php echo $a;?>;
                        if(client_name == 'AHD')
                            url_get2 = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'service/?type=overall&'+quarter_range<?php echo $a;?>;

                        $.ajax({
                            url : url_get2, 
                            async: false,
                            dataType: 'json',
                            success : function(data){

                                if(data instanceof Array)
                                {
                                    $.each(data, function(key, res){
                                        if(isNaN(res.Eligible_Data)) res.Eligible_Data = 0;
                                        if(isNaN(res.Surveys_Conducted)) res.Surveys_Conducted = 0;
                                        if(isNaN(res.Responses)) res.Responses = 0;
                                        if(isNaN(res.Completes)) res.Completes = 0;

                                        q_eligible<?php echo $a;?>      = parseInt(q_eligible<?php echo $a;?>) + parseInt(res.Eligible_Data);
                                        q_conducted<?php echo $a;?>     = parseInt(q_conducted<?php echo $a;?>) + parseInt(res.Surveys_Conducted);
                                        q_response<?php echo $a;?>      = parseInt(q_response<?php echo $a;?>) + parseInt(res.Responses);
                                        q_completes<?php echo $a;?>     = parseInt(q_completes<?php echo $a;?>) + parseInt(res.Completes);
                                    });
                                }
                                else
                                {
                                    if(isNaN(data.Eligible_Data)) data.Eligible_Data = 0;
                                    if(isNaN(data.Surveys_Conducted)) data.Surveys_Conducted = 0;
                                    if(isNaN(data.Responses)) data.Responses = 0;
                                    if(isNaN(data.Completes)) data.Completes = 0;

                                    q_eligible<?php echo $a;?>      = parseInt(q_eligible<?php echo $a;?>) + parseInt(data.Eligible_Data);
                                    q_conducted<?php echo $a;?>     = parseInt(q_conducted<?php echo $a;?>) + parseInt(data.Surveys_Conducted);
                                    q_response<?php echo $a;?>      = parseInt(q_response<?php echo $a;?>) + parseInt(data.Responses);
                                    q_completes<?php echo $a;?>     = parseInt(q_completes<?php echo $a;?>) + parseInt(data.Completes);
                                }

                            },

                        });

                    <?php }?>
                });
            },

        });

        var row_data_e  = '';
        var row_data_c  = '';
        var row_data_r  = '';
        var row_data_c2 = '';

        var row_data_ee  = '';
        var row_data_cc  = '';
        var row_data_rr  = '';
        var row_data_c22 = '';

        <?php for($p=0;$p<=11;$p++){?>
            monthname.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_eligible_<?php echo $p;?>
            });

            monthname2.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_conducted_<?php echo $p;?>
            });

            monthname3.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_response_<?php echo $p;?>
            });

            monthname4.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_completes_<?php echo $p;?>
            });

            row_data_e  = '<td class="ondata">'+m_eligible_<?php echo $p;?>+'</td>';
            row_data_c  = '<td class="ondata">'+m_conducted_<?php echo $p;?>+'</td>';
            row_data_r  = '<td class="ondata">'+m_response_<?php echo $p;?>+'</td>';
            row_data_c2 = '<td class="ondata">'+m_completes_<?php echo $p;?>+'</td>';

            $('.month_table .row_data_e').append(row_data_e);
            $('.month_table .row_data_c').append(row_data_c);
            $('.month_table .row_data_r').append(row_data_r);
            $('.month_table .row_data_c2').append(row_data_c2);
        <?php }?>
        

        quarter_name.push({label: '1st Quarter '+y+'', y: q_eligible1});
        quarter_name.push({label: '2nd Quarter '+y+'', y: q_eligible2});
        quarter_name.push({label: '3rd Quarter '+y+'', y: q_eligible3});
        quarter_name.push({label: '4th Quarter '+y+'', y: q_eligible4});

        quarter_name2.push({label: '1st Quarter '+y+'', y: q_conducted1});
        quarter_name2.push({label: '2nd Quarter '+y+'', y: q_conducted2});
        quarter_name2.push({label: '3rd Quarter '+y+'', y: q_conducted3});
        quarter_name2.push({label: '4th Quarter '+y+'', y: q_conducted4});

        quarter_name3.push({label: '1st Quarter '+y+'', y: q_response1});
        quarter_name3.push({label: '2nd Quarter '+y+'', y: q_response2});
        quarter_name3.push({label: '3rd Quarter '+y+'', y: q_response3});
        quarter_name3.push({label: '4th Quarter '+y+'', y: q_response4});

        quarter_name4.push({label: '1st Quarter '+y+'', y: q_completes1});
        quarter_name4.push({label: '2nd Quarter '+y+'', y: q_completes2});
        quarter_name4.push({label: '3rd Quarter '+y+'', y: q_completes3});
        quarter_name4.push({label: '4th Quarter '+y+'', y: q_completes4});

        row_data_ee  += '<td class="ondata">'+q_eligible1+'</td>';
        row_data_ee  += '<td class="ondata">'+q_eligible2+'</td>';
        row_data_ee  += '<td class="ondata">'+q_eligible3+'</td>';
        row_data_ee  += '<td class="ondata">'+q_eligible4+'</td>';

        row_data_cc  += '<td class="ondata">'+q_conducted1+'</td>';
        row_data_cc  += '<td class="ondata">'+q_conducted2+'</td>';
        row_data_cc  += '<td class="ondata">'+q_conducted3+'</td>';
        row_data_cc  += '<td class="ondata">'+q_conducted4+'</td>';

        row_data_rr  += '<td class="ondata">'+q_response1+'</td>';
        row_data_rr  += '<td class="ondata">'+q_response2+'</td>';
        row_data_rr  += '<td class="ondata">'+q_response3+'</td>';
        row_data_rr  += '<td class="ondata">'+q_response4+'</td>';

        row_data_c22 += '<td class="ondata">'+q_completes1+'</td>';
        row_data_c22 += '<td class="ondata">'+q_completes2+'</td>';
        row_data_c22 += '<td class="ondata">'+q_completes3+'</td>';
        row_data_c22 += '<td class="ondata">'+q_completes4+'</td>';

        $('.quarter_table .row_data_e').append(row_data_ee);
        $('.quarter_table .row_data_c').append(row_data_cc);
        $('.quarter_table .row_data_r').append(row_data_rr);
        $('.quarter_table .row_data_c2').append(row_data_c22);

        CanvasJS.addColorSet(
            "linecolors",[
                "#95BE8C",
                "#26B99A",
                "#777778",
                "#BF89B8"              
            ]
        );

        var linechart2 = new CanvasJS.Chart("quarter_line", {
            colorSet: "linecolors",
            toolTip: {
            shared: true,
            contentFormatter: function(e){
              var str = "";
              for (var i = 0; i < e.entries.length; i++){
                var  temp = e.entries[i].dataSeries.name + " <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ; 
                str = str.concat(temp);
              }
              return (str);
            },

          },

          data: [
            {
                showInLegend: true,
                type: "line",
                name: "Eligible",
                dataPoints: quarter_name
            },
            {
                showInLegend: true,
                type: "line",
                name: "Surveys Conducted",
                dataPoints: quarter_name2
            },
            {
                showInLegend: true,
                type: "line",
                name: "Responses",
                dataPoints: quarter_name3
            },
            {
                showInLegend: true,
                type: "line",
                name: "Completes",
                dataPoints: quarter_name4
            }
          ]
        });

        linechart2.render();

        $('#spinner').hide();
    }

    function loadLineGraphMonthly(year)
    {

        var monthname       = [],
            monthname2      = [],
            monthname3      = [],
            monthname4      = [],
            quarter_name    = [],
            quarter_name2   = [],
            quarter_name3   = [],
            quarter_name4   = [],
            date            = new Date(''+year+''), 
            y               = date.getFullYear(), 
            m               = date.getMonth(),
            firstDay        = '',
            lastDay         = '',
            monthly_range   = '',
            quarter_from    = '',
            quarter_to      = '',
            quarter_1       = '',
            quarter_2       = '',
            quarter_3       = '',
            quarter_4       = '',
            quarter_range1  = '',
            quarter_range2  = '',
            quarter_range3  = '',
            quarter_range4  = '',

            m_eligible      = 0,
            m_conducted     = 0,
            m_response      = 0,
            m_complete      = 0,

            q_eligible1      = 0,
            q_conducted1     = 0,
            q_response1      = 0,
            q_completes1     = 0,

            q_eligible2      = 0,
            q_conducted2     = 0,
            q_response2      = 0,
            q_completes2     = 0,

            q_eligible3      = 0,
            q_conducted3     = 0,
            q_response3      = 0,
            q_completes3     = 0,

            q_eligible4      = 0,
            q_conducted4     = 0,
            q_response4      = 0,
            q_completes4     = 0;

        <?php for($c=0;$c<=11;$c++){?>
            var m_eligible_<?php echo $c;?>      = 0,
                m_conducted_<?php echo $c;?>     = 0,
                m_response_<?php echo $c;?>      = 0,
                m_completes_<?php echo $c;?>     = 0;
        <?php }?>

        <?php for($d=1;$d<=12;$d++){?>
            var m_<?php echo $d;?> = 0;
        <?php }?>

        $.ajax({
            url : baseURL+'dashboard/ajax_request/get_survey_activated_types_by_client/'+client_id+'', 
            async: false,
            dataType: 'json',
            success : function(sat){
                $.each(sat, function(key, val){

                    <?php for($a=0;$a<=11;$a++){?>
                        //-- MONTHLY RANGE DATES
                        firstDay        = new Date(y, <?php echo ($a <= 9) ? '0'.$a.'' : $a;?>, 1);
                        lastDay         = new Date(y, <?php echo ($a <= 9) ? '0'.$a.'' : $a;?> + 1, 0);
                        monthly_range   = 'from='+(firstDay.getMonth()+1)+'/'+firstDay.getDate()+'/'+firstDay.getFullYear()+'&to='+(firstDay.getMonth()+1)+'/'+lastDay.getDate()+'/'+firstDay.getFullYear();

                        var url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?'+monthly_range;

                        if(client_name == 'ICLDC_AD' || client_name == 'ICLDC_AA')
                            url_get = ''+val.webservice_url+'overall.php?'+monthly_range;
                        if(client_name == 'CCAD')
                            if(val.survey_type_name.toLowerCase() == 'md' || val.survey_type_name.toLowerCase() == 'ou')
                                url_get = ''+val.webservice_url+'op/overall.php?'+monthly_range;
                        if(client_name == 'RCH')//hosp1
                            url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?hosp=1&'+monthly_range;
                        if(client_name == 'RNH')//hosp2
                            url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'/overall.php?hosp=2&'+monthly_range;
                        if(client_name == 'AHD')
                            url_get = ''+val.webservice_url+''+val.survey_type_name.toLowerCase()+'service/?type=overall&'+monthly_range;

                        $.ajax({
                            url : url_get, 
                            async: false,
                            dataType: 'json',
                            success : function(data){

                                if(data instanceof Array)
                                {
                                    $.each(data, function(key, res){

                                        if(isNaN(res.Eligible_Data)) res.Eligible_Data = 0;
                                        if(isNaN(res.Surveys_Conducted)) res.Surveys_Conducted = 0;
                                        if(isNaN(res.Responses)) res.Responses = 0;
                                        if(isNaN(res.Completes)) res.Completes = 0;

                                        m_eligible_<?php echo $a;?>   = parseInt(m_eligible_<?php echo $a;?>) + parseInt(res.Eligible_Data);
                                        m_conducted_<?php echo $a;?>  = parseInt(m_conducted_<?php echo $a;?>) + parseInt(res.Surveys_Conducted);
                                        m_response_<?php echo $a;?>   = parseInt(m_response_<?php echo $a;?>) + parseInt(res.Responses);
                                        m_completes_<?php echo $a;?>  = parseInt(m_completes_<?php echo $a;?>) + parseInt(res.Completes);
                                    });
                                }
                                else
                                {
                                    if(isNaN(data.Eligible_Data)) data.Eligible_Data = 0;
                                    if(isNaN(data.Surveys_Conducted)) data.Surveys_Conducted = 0;
                                    if(isNaN(data.Responses)) data.Responses = 0;
                                    if(isNaN(data.Completes)) data.Completes = 0;

                                    m_eligible_<?php echo $a;?>      = parseInt(m_eligible_<?php echo $a;?>) + parseInt(data.Eligible_Data);
                                    m_conducted_<?php echo $a;?>     = parseInt(m_conducted_<?php echo $a;?>) + parseInt(data.Surveys_Conducted);
                                    m_response_<?php echo $a;?>      = parseInt(m_response_<?php echo $a;?>) + parseInt(data.Responses);
                                    m_completes_<?php echo $a;?>     = parseInt(m_completes_<?php echo $a;?>) + parseInt(data.Completes);
                                }

                            },

                        });
                    <?php } ?>

                });
            },

        });

        var row_data_e  = '';
        var row_data_c  = '';
        var row_data_r  = '';
        var row_data_c2 = '';

        var row_data_ee  = '';
        var row_data_cc  = '';
        var row_data_rr  = '';
        var row_data_c22 = '';

        <?php for($p=0;$p<=11;$p++){?>
            monthname.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_eligible_<?php echo $p;?>
            });

            monthname2.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_conducted_<?php echo $p;?>
            });

            monthname3.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_response_<?php echo $p;?>
            });

            monthname4.push({
                x: new Date(y, <?php echo ($p <= 9) ? '0'.$p.'' : $p;?>, 1), y: m_completes_<?php echo $p;?>
            });

            row_data_e  = '<td class="ondata">'+m_eligible_<?php echo $p;?>+'</td>';
            row_data_c  = '<td class="ondata">'+m_conducted_<?php echo $p;?>+'</td>';
            row_data_r  = '<td class="ondata">'+m_response_<?php echo $p;?>+'</td>';
            row_data_c2 = '<td class="ondata">'+m_completes_<?php echo $p;?>+'</td>';

            $('.month_table .row_data_e').append(row_data_e);
            $('.month_table .row_data_c').append(row_data_c);
            $('.month_table .row_data_r').append(row_data_r);
            $('.month_table .row_data_c2').append(row_data_c2);
        <?php }?>

        CanvasJS.addColorSet(
            "linecolors",[
                "#95BE8C",
                "#26B99A",
                "#777778",
                "#BF89B8"              
            ]
        );
        var linechart = new CanvasJS.Chart("month_line", {
            colorSet: "linecolors",
            toolTip: {
            shared: true,
            contentFormatter: function(e){
              var str = "";
              for (var i = 0; i < e.entries.length; i++){
                var  temp = e.entries[i].dataSeries.name + " <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ; 
                str = str.concat(temp);
              }
              return (str);
            },

          },

          data: [
              {
                showInLegend: true,
                type: "line",
                name: "Eligible",
                dataPoints: monthname
              },
              {
                showInLegend: true,
                type: "line",
                name: "Surveys Conducted",
                dataPoints: monthname2
              },
              {
                showInLegend: true,
                type: "line",
                name: "Responses",
                dataPoints: monthname3
              },
              {
                showInLegend: true,
                type: "line",
                name: "Completes",
                dataPoints: monthname4
              }
          ]
        });

        linechart.render();

        $('#spinner').hide();
    }
    
    loadLineGraphQuarterly(year);

    $('.load-graph').click(function(){

        $('#spinner').show();

        $('#month_table, #quarter_table').hide();
        $('.ondata').remove();

        var graph = $(this).data('graph');
        if(graph == 'month') 
        {
            $('#month_line').show();
            $('.month_data').show();
            $('#quarter_line').hide();
            $('.quarter_data').hide();

            loadLineGraphMonthly($('#year').val());
        }
        else 
        {
            $('#month_line').hide();
            $('.month_data').hide();
            $('#quarter_line').show();
            $('.quarter_data').show();

            loadLineGraphQuarterly($('#year').val());
        }

        $('#spinner').hide();
    });
    
});
</script>
