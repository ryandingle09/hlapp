<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('dashboard_model', 'dashboard');
    }
    
    public function index()
    {
        $this->layouts->view('dashboard');
    }

    public function dashboard_client($client_id = NULL)
    {
        $data = [
            'client_id'             => $client_id,
            'client_description'    => $this->dashboard->get_client_detail($this->uri->segment(3))->client_description,
            'client_name'           => $this->dashboard->get_client_detail($this->uri->segment(3))->client_name
        ];

        $this->layouts->view('dashboard_client', $data);
    }

    public function dashboard_client_survey($client_id = NULL, $survey_stype = NULL)
    {   
        $client_id                  = $this->uri->segment(3);
        $survey_type                = $this->uri->segment(4);

        $data = [
            'client_id'             => $client_id,
            'survey_type'           => $survey_stype,
            'survey_urls'           => $this->dashboard->get_survey_type_urls($client_id, $survey_type),
            'client_description'    => $this->dashboard->get_client_detail($this->uri->segment(3))->client_description,
            'client_name'           => $this->dashboard->get_client_detail($this->uri->segment(3))->client_name
        ];

        $this->layouts->view('dashboard_1', $data);
    }
}
