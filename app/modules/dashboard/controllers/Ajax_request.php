<?php

class Ajax_request extends MX_Controller{
	
	/** ajax request process goes here**/

	function __construct()
	{
		parent::__construct();

		$this->load->model('ajax_request_model', 'ajax');
	}

	public function get_survey_activated_types_by_client()
	{
		$client_id = $this->uri->segment(4);
		echo json_encode($this->ajax->get_survey_activated_types_by_client($client_id));
	}

	public function get_survey_type_by_client()
	{
		$client_id = $this->uri->segment(4);
		echo json_encode($this->ajax->get_survey_type_by_client($client_id));
	}

	public function get_client_dashboard_over_all()
	{
		echo json_encode($this->ajax->get_client_dashboard_over_all());
	}

}

?>