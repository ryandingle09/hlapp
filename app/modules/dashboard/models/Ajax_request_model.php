<?php


class Ajax_request_model extends DBCARE_Model{
	
	
	public function get_survey_activated_types_by_client($client_id)
	{
		return $this->db->query('
			SELECT 
			stac.*, 
			st.*,  
			cs.webservice_url,
			cs.web_url,
			cs.client_id  
			FROM '.DBCARE_model::tbl_survey_activated_types.' stac 
			JOIN '.DBCARE_model::tbl_survey_type.' st 
			ON st.survey_type_id = stac.survey_type_id 
			LEFT JOIN '.DBCARE_model::tbl_clients.' cs  
			ON cs.client_id = '.$client_id.'
			WHERE stac.client_id = '.$client_id.'
		')->result();
	}

	public function get_survey_type_by_client($client_id, $survey_type_id)
	{
		$this->db->where(['client_id' => $client_id]);
		return $this->db->get(DBCARE_model::tbl_survey_activated_types)->result();
	}

	public function get_client_dashboard_over_all()
	{
		return $this->db->query('
			SELECT  
			a.client_id as dashboard_client, 
			b.client_name
			FROM '.DBCARE_model::tbl_user_clients.' a 
			JOIN '.DBCARE_model::tbl_clients.' b ON b.client_id = a.client_id 
			WHERE 
			a.employee_id = '.$this->session->userdata('id').' 

		')->result();
	}

}


?>