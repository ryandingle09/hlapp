<?php


class Dashboard_model extends DBCARE_Model{
	
	
	public function get_survey_type_urls($client_id, $survey_type)
	{
		$this->db->where(['client_id' => $this->uri->segment(3), 'survey_type_id' => $survey_type]);
		return $this->db->get(DBCARE_model::tbl_survey_type_urls);
	}

	public function get_client_detail($id)
	{
		$this->db->where(['client_id' => $this->uri->segment(3)]);
		$data = $this->db->get(DBCARE_model::tbl_clients);
		return $data->row();
	}
}



?>