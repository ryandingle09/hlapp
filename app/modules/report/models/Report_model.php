<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Report_model extends CI_Model
{
   
    public function general_select($table = NULL, $fields = NULL, $where = NULL, $group_by = NULL, $order_by = NULL)
    {
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($order_by);
        $query  = $this->db->get();
        $result = $query->result_array();   

        return $result;
    }

    public function amendby($table = NULL, $fields = NULL, $where = NULL, $group_by = NULL, $order_by = NULL)
    {
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($order_by);
        $this->db->group_by($group_by);
        $query  = $this->db->get();
        $result = $query->result_array();   

        return $result;
    }

    public function survey($table = NULL, $fields = NULL, $where = NULL, $order_by = NULL, $id = NULL)
    {
        $this->db->select($fields);
        $this->db->from($table." as A");
        $this->db->join('hosp as B', 'A.hosp = B.code', 'left');
        $this->db->where($where);
        $this->db->order_by($order_by);
        $query  = $this->db->get();
        $result = $query->result_array();   

        return $result;
    }

    public function generate_report($start_date = NULL, $end_date = NULL)
    {
       
        $select ="
        SELECT concat('AS') as survey, A.survey_type, A.code, A.status, A.addedby, A.addeddate,
        A.amendby, A.amenddate, A.completedby, A.completeddate,
        A.initial_response, A.unit, A.refuse_reason, A.refuse_notes
        FROM  as_survey A
        WHERE amenddate >='".$start_date."' && amenddate <= '".$end_date."'

        UNION

        SELECT concat('ER') as survey, B.survey_type, B.code, B.status, B.addedby, B.addeddate, B.amendby, B.amenddate, B.completedby, B.completeddate, B.initial_response, B.unit, B.refuse_reason, B.refuse_notes
        FROM er_survey B
        WHERE amenddate >='".$start_date."' && amenddate <= '".$end_date."'

        UNION 

        SELECT concat('IP') as survey, C.survey_type, C.code, C.status, C.addedby, C.addeddate, C.amendby, C.amenddate, C.completedby, C.completeddate, C.initial_response, C.unit, C.refuse_reason, C.refuse_notes
        FROM ip_survey C
        WHERE amenddate >='".$start_date."' && amenddate <= '".$end_date."'

        UNION 

        SELECT concat('OP') as survey, D.survey_type, D.code, D.status, D.addedby, D.addeddate, D.amendby, D.amenddate, D.completedby, D.completeddate, D.initial_response, D.unit, D.refuse_reason, D.refuse_notes
        FROM op_survey D
        WHERE amenddate >='".$start_date."' && amenddate <= '".$end_date."'
        ";

        $query = $this->db->query($select);
        $result = $query->result_array();

        return $result;
    }
}