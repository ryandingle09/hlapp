<?php
/*
 * Health Links Team
 * 
 * Author: Patricia Ayrah Otero
 * 
 * 
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Report_model','report');
    }
    

    public function inpatient()
    {       

        $table             = "wards";
        $fields            = "hosp, description";
        $where             = array();
        $result            = $this->report->general_select($table, $fields, $where);
        $data['unit']      = $result;
        
        $table             = "ip_survey";
        $fields            = "*, DATE_FORMAT(amenddate,'%Y-%m-%d') as date";
        $where             = array();
        $result            = $this->report->general_select($table, $fields, $where);
        $data['ip_survey'] = $result;
        
        $table             = "ip_survey";
        $fields            = "amendby";
        $where             = array();
        $group_by          = "amendby";
        $result            = $this->report->amendby($table, $fields, $where, $group_by);
        $data['amendby']   = $result;

        $this->layouts->view('inpatient', $data);
    }

    public function ambulatory()
    {   
        $table             = "wards";
        $fields            = "hosp, description";
        $where             = array();
        $result            = $this->report->general_select($table, $fields, $where);
        $data['unit']      = $result;
        
        $table             = "as_survey";
        $fields            = "*, DATE_FORMAT(amenddate,'%Y-%m-%d') as date";
        $where             = array();
        $result            = $this->report->general_select($table, $fields, $where);
        $data['as_survey'] = $result;

        $table             = "as_survey";
        $fields            = "amendby";
        $where             = array();
        $group_by          = "amendby";
        $result            = $this->report->amendby($table, $fields, $where, $group_by);
        $data['amendby']   = $result;

        $this->layouts->view('ambulatory', $data);
    }

    public function answer($id = NULL, $survey_type = NULL)
    {
        
        $table           = $survey_type;
        $fields          = "A.*, DATE_FORMAT(A.visitdate,'%m-%d-%Y') as visit_date, B.description, B.description_ar";
        $where           = array();
        $where['A.code'] = $id;
        $order_by        = array();
        $result          = $this->report->survey($table, $fields, $where, $order_by, $id);
        $data['answer']  = array_shift($result);
        $data['table']   = $table;


        $this->layouts->view($survey_type.'_answer', $data);
    }

    public function summary()
    {
        $this->layouts->view('summary');
    }

    public function summary_generate($start_date = NULL, $end_date = NULL)
    {

        
        $data = array();

        $ddate = date("Y-m-d");

        $date  = DateTime::createFromFormat("Y-m-d", $ddate);
        $year  = substr($date->format("Y"), 3);
        $week  = $date->format("W");
        $month = strtoupper(substr($date->format("M"), 0,-2));

        $name = "CCAD".$year.$month."_".$week;

        $result = $this->report->generate_report($start_date, $end_date);
        $data['records'] = $result;
        $data['start_date'] = $start_date;
        $data['end_date'] =$end_date;

        $this->load->view('download', $data);
            
        $echo = ob_get_contents();
            
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=".$name.".xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        
        echo $echo;

    }
}
