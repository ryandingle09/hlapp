<!--<img src="<?php echo PATH_IMG?>careimg.jpg" width="250" style="float: right"/>-->
<div class="p-md">
    <div class="row white" style="margin-top: -10px">

        <div class="col-md-12 text-center p-t-md">
            <h1>
                <p dir="rtl">استبيان جراحة اليوم الواحد / الإجراءات التداخلية</p>Ambulatory Surgery Survey
            </h1>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-6 col-md-offset-3 data survey top-info">
            <table class="" width="100%" cellpadding="30">
                <tr>
                    <td class="text-left">Added Date</td>
                    <td class="text-center padding-5"><b><?php echo $answer['addeddate']?></b><br></td>
                    <td class="text-right"> التاريخ</td>
                </tr>
                <tr>
                    <td class="text-left">Patient's Name</td>
                    <td class="text-center padding-5"><b><?php echo $answer['patname']?></b></td>
                    <td class="text-right"> اسم المريض</td>
                </tr>
                <tr>
                    <td class="text-left">Language</td>
                    <td class="text-center padding-5"><b><?php echo $answer['nationality']?></b><br></td>
                    <td class="text-right"> اللغة</td>
                </tr>
                <tr>
                    <td class="text-left">Mobile No</td>
                    <td class="text-center padding-5"><b><?php echo $answer['mobile']?></b><br></td>
                    <td class="text-right"> رقم الجوال</td>
                </tr>
                <tr>
                    <td class="text-left">Secondary Number</td>
                    <td class="padding-5 text-center"><input name="second_number" type="text" class="text-center" placeholder="<?php //echo $survey->second_number;?>"></td>
                    <td class="text-right"> رقم ثانوي</td>
                </tr>
            </table>
        </div>

    </div>

    <div class="row white survey">

        <div class="row padding-50 p-t-n" style="padding-top: -50px">

            <div id="rootwizard" class="tabbable tabs-left">

                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="container">
                            <small>
                            <ul style="list-style: none">
                                <li><a class="hide" href="#tab1" data-toggle="tab">Step 1</a></li>
                                <li><a class="hide" href="#tab2" data-toggle="tab">Step 2</a></li>
                                <li><a class="hide" href="#tab3" data-toggle="tab">Step 3</a></li>
                                <li><a class="hide" href="#tab4" data-toggle="tab">Step 4</a></li>
                                <li><a class="hide" href="#tab5" data-toggle="tab">Step 5</a></li>
                                <li><a class="hide" href="#tab6" data-toggle="tab">Step 6</a></li>
                                <li><a class="hide" href="#tab7" data-toggle="tab">Step 7</a></li>
                                <li><a class="hide" href="#tab8" data-toggle="tab">Step 8</a></li>
                            </ul>
                            </small>
                        </div>
                    </div>
                </div>

                <!-- KEYS -->
                <input type="hidden" name="type" id="type" value="<?php //echo $this->uri->segment(3);?>">
                <input type="hidden" name="hosp" id="hosp" value="<?php //echo $this->uri->segment(4);?>">
                <input type="hidden" name="code" id="code" value="<?php //echo $this->uri->segment(5);?>">
                <input type="hidden" name="random" id="random" value="<?php //echo $this->uri->segment(6);?>">
                <!-- END -->

                <div class="tab-content margin-bottom-20 data" style="margin-top: -50px">

                    <div class="tab-pane" id="tab1">
                        
                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        <b>Good Morning / Good Afternoon</b>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        <b>السلام عليكم ورحمة الله وبركاته..</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        This is <b><?php echo ($this->session->userdata('fullname') != '') ? $this->session->userdata('fullname') : '[Surveyor Name]';?></b> from <b>Health.Links / Press Ganey</b>, we are calling on behalf of <?php echo $answer['description'];?>.
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        معكم <b><?php echo ($this->session->userdata('fullname') != '') ? $this->session->userdata('fullname') : '[Surveyor Name]';?></b>، أتحدث معك من شركة روابط للحلول الصحية / Press Ganey، ونحن نتصل بك بالنيابة عن مستشفى <?php echo $answer['description_ar'];?>.
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        May I speak to <b><?php echo $answer['patname'];?> ?</b> <br>
                                        <small>
                                        Speak to the patient / Guardian only. If you were answered by any other person, ask to talk to patient / Guardian.
                                        End the call if not available or if the number you called is a wrong number.
                                        </small>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        هل أستطيع التحدث مع <b><?php echo $answer['patname'];?> ؟</b><br>
                                        <small>
                                            عليك التحدث مع المريض / الوصي فقط. في حالة تمت الإجابة عليك من قبل شخص آخر، اطلب التحدث مع المريض / الوصي مباشرة.
                                             قم بانهاء المكالمة في حال لم تستطع الوصول للمريض / الوصي، أو في حالة كون الرقم خاطئ.
                                        </small>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        You have been selected to share your experience at <?php echo $answer['description']; ?> and to provide an opportunity for improvement of the hospital’s services.
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        لقد تم اختيارك لمشاركة تجربتك في مستشفى <?php echo $answer['description_ar']; ?> وتوفير الفرصة لتحسين الخدمات التي يقدمها المستشفى.
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        Is it a convenient time to speak?
                                        <small>
                                        If yes, continue to survey.
                                        </small>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        هل الوقت مناسب للتحدث؟
                                        <small>
                                        في حالة الموافقة، قم بمواصلة الاستيبان.
                                        </small>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        <b>INITIAL RESPONSE</b>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        هل الوقت مناسب للتحدث؟
                                        <small>
                                        الإجابة الأولية
                                        </small>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <?php 
                                                    $text = '';
                                                    $result = '';
                                                    for($count = 1;$count <=5;$count++){ 

                                                    switch ($count) {
                                                        case '1':
                                                            $result = ($answer['initial_response'] == "1") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>نعم</p><br>Yes<br>";
                                                            break;
                                                        case '2':
                                                            $result = ($answer['initial_response'] == "2") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>إعادة جدولة</p><br>Reschedule<br>";
                                                            break;
                                                        case '3':
                                                            $result = ($answer['initial_response'] == "3") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>رفض </p><br>Refused<br>";
                                                            break;
                                                        case '4':
                                                            $result = ($answer['initial_response'] == "4") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>عدم الإجابة</p><br>No Response<br>";
                                                            break;
                                                        default:
                                                            $result = ($answer['initial_response'] == "5") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>ارسل ايميل</p><br>Send Email<br>";
                                                            break;
                                                    }
                                                ?>
                                                    <td class="text-center" width="20%">
                                                        <div class="col-md-12">
                                                            <input class="to-labelauty synch-icon initial" data-show="<?php echo $count;?>" value="<?php echo $count;?>" type="radio" name="initial_response" <?php echo $result ?>
                                                            data-labelauty="
                                                            <?php echo $text;?><br>
                                                            " />
                                                        </div>
                                                        <?php if($count == 2): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Date<p dir="rtl">التاريخ</p></label>
                                                                <input type="text" value="<?php echo (!EMPTY($answer['rs_day'])) ? $answer['rs_day'] : date('m/d/Y');?>" class="datepicker text-center datesave" id="date" name="rs_day" <?php echo (!EMPTY($answer['rs_day'])) ? "disabled" : "";?>>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Time<br><p dir="rtl">الوقت</p></label>
                                                                <select class="selectize" name="rs_time">
                                                                    <?php 
                                                                        if(!EMPTY($answer['rs_time']))
                                                                        {
                                                                            echo '<option>'.$answer['rs_time'].'</option>';
                                                                        }
                                                                        else
                                                                        {
                                                                            for($hours=8; $hours<19; $hours++)
                                                                            { 
                                                                                $time = $hours % 12 ? $hours % 12 : 12;

                                                                                for($mins=0; $mins<60; $mins+=30)
                                                                                {
                                                                                    $AP     = ($hours < 12) ? 'AM' : 'PM';
                                                                                    $show   = $time.':'.str_pad($mins,2,'0',STR_PAD_LEFT).' '.$AP.'';
                                                                                    if($show != '6:30 PM')
                                                                                        echo '<option>'.$show.'</option>';
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>

                                                        <?php if($count == 3): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Reason <br><p dir="rtl">السبب</p></label>
                                                                <select class="selectize" name="refuse_reason">
                                                                    <?php 
                                                                        if(!EMPTY($answer['refuse_reason']))
                                                                        {
                                                                            echo '<option value="">'.$answer['refuse_reason'].'</option>';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<option value="">SELECT</option>'; 
                                                                            echo '<option value="Wrong Number |  رقم خاطئ">Wrong Number |  رقم خاطئ</option>';
                                                                            echo '<option value="Not the Patient/Guardian |  ليس المريض/الوصي">Not the Patient/Guardian |  ليس المريض/الوصي</option>';
                                                                            echo '<option value="Does not want to participate |  لا يرغب في المشاركة">Does not want to participate |  لا يرغب في المشاركة</option>';
                                                                            echo '<option value="Already participated |  سبقت له المشاركة">Already participated |  سبقت له المشاركة</option>';
                                                                            echo '<option value="Others | أخرى">Others | أخرى</option>';  
                                                                        }
                                                                    ?>
                                                                    
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>

                                                        <?php if($count == 4): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Reason <br><p dir="rtl">السبب</p></label>
                                                                <select class="selectize" name="na_reason">
                                                                    <?php 
                                                                        if(!EMPTY($answer['na_reason']))
                                                                        {
                                                                            echo '<option value="">'.$answer['na_reason'].'</option>';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<option value="">SELECT</option>'; 
                                                                            echo '<option value="No Answer |  عدم الرد ">No Answer |  عدم الرد </option>';
                                                                            echo '<option value="Call Rejected |  رفض المكالمه">Call Rejected |  رفض المكالمه</option>';
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>

                                                        <?php if($count == 5): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="text" class="text-center" id="date" name="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Respondent Name</label>
                                                                <input type="text" class="text-center" id="date" name="">
                                                            </div>
                                                        </div>
                                                        <?php endif;?>
                                                    </td>

                                                <?php } ?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="padding-10">
                                        This call may be recorded for quality monitoring purposes and your identity will remain confidential unless you give permission to share it with the hospital
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        قد يتم تسجيل هذه المكالمة لغرض مراقبة الجودة، وسنحافظ على سرية هويتك إلا في حالة موافقتكم على مشاركتها مع المستشفى
                                    </td>
                                </tr>

                                <td colspan="5" class="border-bottom">

                                    <table width="100%">
                                        <tbody><tr>
                                            <td>
                                                Our records show that you visited <b><?php echo $answer['description'];?></b> on
                                            </td>
                                            <td dir="rtl">
                                                 تشير سجلاتنا إلى أنك زرت
                                                <b>مستشفى رعاية الرياض</b>
                                                ال (يوم ... بتاريخ ...)، هل هذا صحيح؟
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <center class="p-t-sm">

                                        <b>Discharge Date :</b> <input disabled="" name="visitdate" class="text-center datepicker" id="date2" value="<?php echo $answer['visit_date'];?>" type="text"><b> : تاريخ الخروج من المستشفى</b>

                                    </center>

                                    <table class="noborder_table" width="100%">
                                        <tbody><tr>
                                            <td>
                                                is that correct?
                                            </td>
                                            <td dir="rtl">
                                                هل هذا صحيح؟
                                            </td>
                                        </tr>
                                    </tbody></table>


                                    <br>
                                    <table class="noborder_table" width="100%">
                                    <tbody><tr>
                                        <td>
                                            <b>
                                            The questions of this survey are designed to evaluate your visit to Inpatient in
                                            <span ><?php echo $answer['visit_date'];?></span>.                                  All answers should relate to this specific visit.
                                            </b>
                                        </td>
                                        <td class="ar">
                                            

                                            <b>
                                            .<?php echo $answer['visit_date'];?> جميع أسئلة هذا الاستبيان خاصة بزيارتك لقسم المرضى المنومين بتاريخ
                                                                             ويجب أن تكون أجوبتك مرتبطة بهذه الزيارة فقط.
                                            </b>

                                        </td>
                                    </tr></tbody></table>

                                </td>


                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        <b>INSTRUCTIONS:</b><br>
                                        Choose the response that best describes your experience. If a question does not apply to you, please skip to the next question. Space is provided for you to comment on your experiences
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        <b>تعليمات:</b><br>
                                اختر الإجابة التي تصف تجربتك بأفضل وصف. إذا كان السؤال لا ينطبق على حالتك، الرجاء التجاوز إلى السؤال التالي. نوفر لك  المساحة للتعليق على تجربتك.
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab2">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>REGISTRATION</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>التسجيل</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        1. Did you speak with the hospital through our Call Center?
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- هل تواصلت مع المستشفى عبر مركز الحجوزات؟
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <td colspan="5"  class="text-center padding-20">
                                        <form class="form-inline">
                                            <div class="form-group p-r-sm">
                                                <input class="to-labelauty synch-icon" value="no" type="radio" name="1" <?php echo ($answer['q1'] == "1") ? "checked" : "disabled"?>
                                                data-labelauty="
                                                <br>
                                                <span dir='rtl'>لا (انتقل للسؤال 3)</span>
                                                <br>NO [Proceed to Q3]<br>
                                                " />
                                            </div>
                                            <div class="form-group">
                                                <input class="to-labelauty synch-icon" value="yes" type="radio" name="1" <?php echo ($answer['q1'] == "2") ? "checked" : "disabled"?>
                                                data-labelauty="
                                                <br>
                                                <span dir='rtl'>نعم</span>
                                                <br>Yes<br>
                                                " />
                                            </div>
                                        </form>
                                    </td>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        2. Helpfulness of the person you spoke with
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- مساعدة الشخص الذي تحدثت معه 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q2'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q2'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q2'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q2'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q2'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="2" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        3. Ease of getting an appointment for the surgery / procedure when you wanted 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- سهولة تحديد تاريخ العملية / الإجراء الطبي في الموعد الذي تفضله
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q3'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q3'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q3'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q3'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q3'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="3" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        4. Information you received prior to surgery / procedure (i.e., time of surgery, how to prepare)
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        4- المعلومات المقدمة لك قبل العملية / الإجراء الطبي (على سبيل المثال: موعد العملية، كيفية الإستعداد)
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q4'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q4'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q4'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q4'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q4'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="4" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        5. Helpfulness of the person at the registration desk 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        5- مساعدة الشخص لك عند مكتب التسجيل
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q5'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q5'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q5'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q5'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q5'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="5" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments1" disabled><?php echo $answer['comments1']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab3">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>FACILITY</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>المرافق</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        1. Comfort of the waiting area
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- الراحة في منطقة الانتظار
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q6'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q6'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q6'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q6'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q6'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="6" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        2. Comfort of your room
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- راحتك في غرفتك الخاصة
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q7'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q7'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q7'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q7'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q7'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="7" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        3. Cleanliness of the Hospital
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- نظافة المستشفى
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q8'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q8'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q8'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q8'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q8'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="8" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        4. Availability of things to do while you waited (TV, magazines … etc)
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        4- وجود وسائل الترفيه أثناء فترة انتظارك (تلفاز، مجلات ... الخ) 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q9'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q9'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q9'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q9'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q9'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="9" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments2" disabled><?php echo $answer['comments2']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab4">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>BEFORE YOUR SURGERY OR PROCEDURE</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>قبل العملية / الإجراء الطبي</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        1. Waiting time before your surgery or procedure began 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- فترة الانتظار قبل بدء العميلة / الإجراء الطبي
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q9'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q9'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q9'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q9'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q9'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="10" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        2. Friendliness/courtesy of the physician 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- اهتمام الطبيب 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q11'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q11'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q11'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q11'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q11'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="11" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        3. Explanation the physician gave you about what the surgery or procedure would be like 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- الشرح الذي قدمه لك الطبيب عن كيف ستكون العملية أو الإجراء الطبي
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q12'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q12'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q12'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q12'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q12'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="12" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        4. Instructions you were given by our staff about how to prepare for your surgery or<br> procedure 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        4- التعليمات التي قدمها لك موظفونا عن كيفية التحضير للعملية أو الإجراء الطبي<br>&nbsp;
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q14'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="13" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        5. Friendliness/courtesy of the nurses 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        5- اهتمام طاقم التمريض 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q14'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q14'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q14'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q14'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q14'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="14" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        6. Skill of the nurse starting IV 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        6- مهارة الممرض(ة) الذي (التي) قام(ت) بوضع المحلول الوريدي 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q13'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="15" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        7. Information the nurses gave you on the day of your surgery or procedure
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        7- المعلومات التي قدمها لك طاقم التمريض يوم العملية / الإجراء الطبي
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q13'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="16" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        8. Your confidence that OR staff correctly identified you and your procedure prior to<br> surgery
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    8- ثقتك بأن طاقم غرفة العمليات تأكدوا من هويتك وإجراءك الطبي بشكل صحيح قبل العملية<br>&nbsp;
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q13'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="17" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        9. Extent to which nurses checked your ID bracelet before giving you medications
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        9- مدى فحص طاقم التمريض لأسوارة التعريف الخاصة بك قبل إعطائك أي أدوية
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q13'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="18" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        10. Extent to which staff washed their hands
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        10- مدى التزام الفريق الطبي بغسل أيديهم
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q13'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="19" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments3" disabled><?php echo $answer['comments3']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab5">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>ANESTHESIA</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>التخدير</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        1. Anesthesiologist's explanation
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- الشرح الذي قدمه لك طبيب التخدير
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q20'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q20'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q20'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q20'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q20'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="20" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        2. Courtesy and friendliness of the anesthesiologist
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- اهتمام طبيب التخدير
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q21'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q21'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q21'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q21'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q21'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%"><?php echo $result; ?>
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="21" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        3. Rate overall anesthesia experience
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- تقييمك العام لتجربة التخدير
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q22'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q22'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q22'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q22'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q22'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="22" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments4" disabled><?php echo $answer['comments4']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab6">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>AFTER YOUR SURGERY OR PROCEDURE</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>عد العملية / الإجراء الطبي</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        1. Nurses' concern for your comfort after the surgery or procedure 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- اهتمام طاقم التمريض براحتك بعد العملية / الإجراء الطبي
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q22'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q22'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q22'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q22'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q22'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="23" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        2. Information the physician provided about what was done during your surgery or procedure 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- المعلومات التي قدمها لك الطبيب عن ما تم إجراؤه خلال العملية / الإجراء الطبي
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q24'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q24'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q24'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q24'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q24'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="24" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        3. Information nurses gave your family about your surgery or procedure
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- المعلومات التي قدمها طاقم التمريض لعائلتك عن العملية / الإجراء الطبي
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q22'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q22'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q22'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q22'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q22'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="25" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        4. Instructions nurses gave you about caring of yourself at home  </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        4- التعليمات التي قدمها لك طاقم التمريض حول كيفية الاعتناء بنفسك في المنزل
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q26'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q26'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q26'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q26'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q26'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="26" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        5. Your confidence in the skill of the nurses 
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        5- ثقتك في مهارة طاقم التمريض
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q27'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q27'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q27'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q27'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q27'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="27" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        6. Your confidence in the skill of the physician  
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        6- ثقتك في مهارة الطبيب
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q28'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q28'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q28'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q28'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q28'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="28" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments5" disabled><?php echo $answer['comments5']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab7">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>PERSONAL ISSUES</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>أمور شخصية</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        1. Information provided about delays (if you experienced delays)
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- المعلومات المقدمة حول التأخيرات (إذا واجهت أي تأخيرات) 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q28'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q28'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q28'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q28'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q28'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="29" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        2. Our concern for your privacy
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- مراعاتنا لخصوصيتك 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q28'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q28'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q28'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q28'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q28'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="30" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        3. Degree to which your pain was controlled
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- ما مدى السيطرة على شعورك بالألم
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q31'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q31'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q31'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q31'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q31'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="31" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10" colspan="2">
                                        4. Response to concerns/complaints made during your visit
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        4- الاستجابة للمخاوف والشكاوى التي أعربت عنها خلال زيارتك 
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q32'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q32'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q32'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q32'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q32'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="32" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments6" disabled><?php echo $answer['comments6']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab8">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10" colspan="2">
                                        <b>OVERALL ASSESSMENT</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>التقييم العام</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                    1. Overall rating of care received during your visit
                                    </td>
                                        <td dir="rtl" class="text-right" colspan="3">
                                        1- التقييم العام للرعاية التي تلقيتها خلال زيارتك
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q33'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q33'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q33'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q33'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q33'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="33" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        2. How well staff worked together to care for you
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2-  ما مدى تعاون العاملين لتقديم الرعاية لك
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q34'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q34'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q34'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q34'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q34'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="34" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        3. Likelihood of you recommending this hospital to others
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- احتمالية أن توصي بهذه المستشفى للآخرين
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q35'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q35'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q35'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q35'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q35'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="35" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        4. Was there any team member that provided exceptional care / service? Who?
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        4- هل قدّم لك أحد الموظفين رعاية مميزة تذكر؟ هل تذكر اسمهـ(ا)؟
                                    </td>
                                </tr>
                                <tr class="border-bottom text-center">
                                    <td colspan="3">
                                        <input class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="no" name="hl_qtm" <?php echo ($answer['hl_qtm'] == "1") ? "checked" : "disabled" ?>
                                            data-labelauty="<br>
                                            <p dir='rtl'>لا</p>
                                            <br>
                                            No<br><br>
                                            " />
                                    </td>
                                    <td colspan="2">
                                        <div class="col-md-12 text-center">
                                            <p>
                                                <input class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="yes" name="hl_qtm" <?php echo ($answer['hl_qtm'] == "2") ? "checked" : "disabled" ?>
                                                 data-labelauty="
                                                    <br>
                                                    <p dir='rtl'>نعم (اذكر الإسم والقسم)</p>
                                                    <br>
                                                    Yes (Mention name & division)<br><br>
                                                 "
                                                />
                                            </p>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2" id="ND">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" placeholder="Name" value="<?php echo $answer['hl_atm']; ?>" name="hl_atm">
                                            </div>
                                            <div class="form-group">
                                                <label>Division</label>
                                                <input type="text" class="form-control" placeholder="Division"value="<?php echo $answer['hl_atmd']; ?>"  name="hl_atmd">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" data-show="extra" name="comments7" disabled><?php echo $answer['comments7']; ?></textarea>
                                    </td>
                                </tr>

                                <tr class="border-bottom extra" style="display: none">
                                        <td colspan="2">
                                            Would you like to share your contact information along the comments you gave with the hospital?
                                        </td>
                                        <td dir="rtl" class="text-right" colspan="3">
                                        سيتم إرسال تعليقاتك للمستشفى، هل ترغب بإرفاق اسمك ورقم هاتفك معها؟<br>&nbsp;
                                        </td>
                                </tr>

                                <tr class="border-bottom text-center extra" style="display: none">
                                    <td colspan="3">
                                        <input class="to-labelauty synch-icon skipme" value="no" data-show="false" type="radio" name="36" 
                                        data-labelauty="
                                        <br>
                                        <p dir='rtl'>لا</p>
                                        <br>
                                        No<br><br>
                                        " />
                                    </td>
                                    <td colspan="2">
                                        <input class="to-labelauty synch-icon skipme" data-show="false" value="yes" type="radio" name="36" 
                                        data-labelauty="
                                        <br>
                                        <p dir='rtl'>نعم</p>
                                        <br>
                                        Yes<br><br>
                                        " />
                                    </td>
                                </tr>

                                <tr class="border-bottom extra" style="display: none;">
                                        <td colspan="2">
                                            Have you had any issues with the care you received and would like a hospital official to contact you?
                                        </td>
                                        <td dir="rtl" class="text-right" colspan="3">
                                        هل واجهتك أي مشاكل في العناية الطبية التي تلقيتها وترغب بأن يقوم مسؤول في المستشفى بالتواصل بك؟
                                        </td>
                                </tr>
                                <tr class="border-bottom text-center extra" style="display: none;">
                                    <td colspan="3">
                                        <input class="to-labelauty synch-icon ACTION" data-show="#CD" value="no" type="radio" name="hl_contact" 
                                        data-labelauty="
                                        <br>
                                        <p dir='rtl'>لا</p>
                                        <br>
                                        No<br><br>
                                        " />
                                    </td>
                                    <td colspan="2" class="false">
                                        <div class="col-md-12 text-center">
                                            <p>
                                                <input class="to-labelauty synch-icon ACTION" data-show="#CD" value="yes" type="radio" name="hl_contact" 
                                                data-labelauty="
                                                <br>
                                                <p dir='rtl'>نعم (طريقة التواصل)</p>
                                                <br>
                                                Yes (contact details)<br><br>
                                                " />
                                            </p>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2" id="CD" style="display: none">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" placeholder="Name" name="hl_contact_name ">
                                            </div>
                                            <div class="form-group">
                                                <label>Mobile No.</label>
                                                <input type="text" class="form-control" placeholder="Mobile No." name="hl_contact_no">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row prog">
                        <div id="bar" class="progress">
                            <div class="progress-bar active progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                </td>
                        </div>
                    </div>

                    <ul class="pager wizard">
                        <li class="previous first" style="display:none;"><a href="javascript:;">First</a></li>
                        <li class="previous"><a href="javascript:;">Previous</a></li>
                        <li class="next last" style="display:none;"><a href="javascript:;">Last</a></li>
                        <li class="next"><a href="javascript:;">Next</a></li>
                        <li class="finish pull-right"><a href="javascript:;">Finish <i class="fa fa-check"></i></a></li>
                    </ul>

                    <div class="text-center not-proceeding">
                        <a href="caresurvey/" class="btn btn-info btn-lg">BACK TO SURVEY LIST</a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>