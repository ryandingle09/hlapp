<!--<img src="<?php echo PATH_IMG?>careimg.jpg" width="250" style="float: right"/>-->
<div class="p-md">
    <div class="row white" style="margin-top: -10px">

        <div class="col-md-12 text-center p-t-md">
            <h1>
                <p dir="rtl">استبيان المرضى المنومين</p>Inpatient Survey
            </h1>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-6 col-md-offset-3 data survey top-info">
            <table class="" width="100%" cellpadding="30">
                <tr>
                    <td class="text-left">Added Date</td>
                    <td class="text-center padding-5"><b><?php echo $answer['addeddate']?></b><br></td>
                    <td class="text-right"> التاريخ</td>
                </tr>
                <tr>
                    <td class="text-left">Patient's Name</td>
                    <td class="text-center padding-5"><b><?php echo $answer['patname']?></b></td>
                    <td class="text-right"> اسم المريض</td>
                </tr>
                <tr>
                    <td class="text-left">Language</td>
                    <td class="text-center padding-5"><b><?php echo $answer['nationality']?></b><br></td>
                    <td class="text-right"> اللغة</td>
                </tr>
                <tr>
                    <td class="text-left">Mobile No</td>
                    <td class="text-center padding-5"><b><?php echo $answer['mobile']?></b><br></td>
                    <td class="text-right"> رقم الجوال</td>
                </tr>
                <tr>
                    <td class="text-left">Secondary Number</td>
                    <td class="padding-5 text-center"><input name="second_number" type="text" class="text-center" placeholder="<?php //echo $survey->second_number;?>"></td>
                    <td class="text-right"> رقم ثانوي</td>
                </tr>
            </table>
        </div>

    </div>

    <div class="row white survey ">

        <div class="row padding-50 p-t-n" style="padding-top: -50px">

            <div id="rootwizard" class="tabbable tabs-left">

                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="container">
                            <small>
                            <ul style="list-style: none">
                                <li><a class="hide" href="#tab1" data-toggle="tab">Step 1</a></li>
                                <li><a class="hide" href="#tab2" data-toggle="tab">Step 2</a></li>
                                <li><a class="hide" href="#tab3" data-toggle="tab">Step 3</a></li>
                                <li><a class="hide" href="#tab4" data-toggle="tab">Step 4</a></li>
                                <li><a class="hide" href="#tab5" data-toggle="tab">Step 5</a></li>
                                <li><a class="hide" href="#tab6" data-toggle="tab">Step 6</a></li>
                                <li><a class="hide" href="#tab7" data-toggle="tab">Step 7</a></li>
                                <li><a class="hide" href="#tab8" data-toggle="tab">Step 8</a></li>
                                <li><a class="hide" href="#tab9" data-toggle="tab">Step 9</a></li>
                                <li><a class="hide" href="#tab10" data-toggle="tab">Step 10</a></li>
                                <li><a class="hide" href="#tab11" data-toggle="tab">Step 11</a></li>
                                <li><a class="hide" href="#tab12" data-toggle="tab">Step 12</a></li>
                                <li><a class="hide" href="#tab13" data-toggle="tab">Step 13</a></li>
                                <li><a class="hide" href="#tab14" data-toggle="tab">Step 14</a></li>
                            </ul>
                            </small>
                        </div>
                    </div>
                </div>

                <!-- KEYS -->
                <input type="hidden" name="type" id="type" value="<?php //echo $this->uri->segment(3);?>">
                <input type="hidden" name="hosp" id="hosp" value="<?php //echo $this->uri->segment(4);?>">
                <input type="hidden" name="code" id="code" value="<?php //echo $this->uri->segment(5);?>">
                <input type="hidden" name="random" id="random" value="<?php //echo $this->uri->segment(6);?>">
                <!-- END -->

                <div class="tab-content margin-bottom-20 data" style="margin-top: -50px">

                    <div class="tab-pane" id="tab1">
                        
                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        <b>Good Morning / Good Afternoon</b>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        <b>السلام عليكم ورحمة الله وبركاته..</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        This is <b><?php echo ($this->session->userdata('fullname') != '') ? $this->session->userdata('fullname') : '[Surveyor Name]';?></b> from <b>Health.Links / Press Ganey</b>, we are calling on behalf of <?php echo $answer['description'];?>.
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        معكم <b><?php echo ($this->session->userdata('fullname') != '') ? $this->session->userdata('fullname') : '[Surveyor Name]';?></b>، أتحدث معك من شركة روابط للحلول الصحية / Press Ganey، ونحن نتصل بك بالنيابة عن مستشفى <?php echo $answer['description_ar'];?>.
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        May I speak to <b><?php echo $answer['patname'];?> ?</b> <br>
                                        <small>
                                        Speak to the patient / Guardian only. If you were answered by any other person, ask to talk to patient / Guardian.
                                        End the call if not available or if the number you called is a wrong number.
                                        </small>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        هل أستطيع التحدث مع <b><?php echo $answer['patname'];?> ؟</b><br>
                                        <small>
                                            عليك التحدث مع المريض / الوصي فقط. في حالة تمت الإجابة عليك من قبل شخص آخر، اطلب التحدث مع المريض / الوصي مباشرة.
                                             قم بانهاء المكالمة في حال لم تستطع الوصول للمريض / الوصي، أو في حالة كون الرقم خاطئ.
                                        </small>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        You have been selected to share your experience at <?php echo $answer['description']; ?> and to provide an opportunity for improvement of the hospital’s services.
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        لقد تم اختيارك لمشاركة تجربتك في مستشفى <?php echo $answer['description_ar']; ?> وتوفير الفرصة لتحسين الخدمات التي يقدمها المستشفى.
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        Is it a convenient time to speak?
                                        <small>
                                        If yes, continue to survey.
                                        </small>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        هل الوقت مناسب للتحدث؟
                                        <small>
                                        في حالة الموافقة، قم بمواصلة الاستيبان.
                                        </small>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        <b>INITIAL RESPONSE</b>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        هل الوقت مناسب للتحدث؟
                                        <small>
                                        الإجابة الأولية
                                        </small>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <?php 
                                                    $text = '';
                                                    $result = '';
                                                    for($count = 1;$count <=5;$count++){ 

                                                    switch ($count) {
                                                        case '1':
                                                            $result = ($answer['initial_response'] == "1") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>نعم</p><br>Yes<br>";
                                                            break;
                                                        case '2':
                                                            $result = ($answer['initial_response'] == "2") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>إعادة جدولة</p><br>Reschedule<br>";
                                                            break;
                                                        case '3':
                                                            $result = ($answer['initial_response'] == "3") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>رفض </p><br>Refused<br>";
                                                            break;
                                                        case '4':
                                                            $result = ($answer['initial_response'] == "4") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>عدم الإجابة</p><br>No Response<br>";
                                                            break;
                                                        default:
                                                            $result = ($answer['initial_response'] == "5") ? "checked" : "disabled";
                                                            $text = "<br><p dir='rtl'>ارسل ايميل</p><br>Send Email<br>";
                                                            break;
                                                    }
                                                ?>
                                                    <td class="text-center" width="20%">
                                                        <div class="col-md-12">
                                                            <input class="to-labelauty synch-icon initial" data-show="<?php echo $count;?>" value="<?php echo $count;?>" type="radio" name="initial_response" <?php echo $result ?>
                                                            data-labelauty="
                                                            <?php echo $text;?><br>
                                                            " />
                                                        </div>
                                                        <?php if($count == 2): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Date<p dir="rtl">التاريخ</p></label>
                                                                <input type="text" value="<?php echo (!EMPTY($answer['rs_day'])) ? $answer['rs_day'] : date('m/d/Y');?>" class="datepicker text-center datesave" id="date" name="rs_day" <?php echo (!EMPTY($answer['rs_day'])) ? "disabled" : "";?>>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Time<br><p dir="rtl">الوقت</p></label>
                                                                <select class="selectize" name="rs_time">
                                                                    <?php 
                                                                        if(!EMPTY($answer['rs_time']))
                                                                        {
                                                                            echo '<option>'.$answer['rs_time'].'</option>';
                                                                        }
                                                                        else
                                                                        {
                                                                            for($hours=8; $hours<19; $hours++)
                                                                            { 
                                                                                $time = $hours % 12 ? $hours % 12 : 12;

                                                                                for($mins=0; $mins<60; $mins+=30)
                                                                                {
                                                                                    $AP     = ($hours < 12) ? 'AM' : 'PM';
                                                                                    $show   = $time.':'.str_pad($mins,2,'0',STR_PAD_LEFT).' '.$AP.'';
                                                                                    if($show != '6:30 PM')
                                                                                        echo '<option>'.$show.'</option>';
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>

                                                        <?php if($count == 3): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Reason <br><p dir="rtl">السبب</p></label>
                                                                <select class="selectize" name="refuse_reason">
                                                                    <?php 
                                                                        if(!EMPTY($answer['refuse_reason']))
                                                                        {
                                                                            echo '<option value="">'.$answer['refuse_reason'].'</option>';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<option value="">SELECT</option>'; 
                                                                            echo '<option value="Wrong Number |  رقم خاطئ">Wrong Number |  رقم خاطئ</option>';
                                                                            echo '<option value="Not the Patient/Guardian |  ليس المريض/الوصي">Not the Patient/Guardian |  ليس المريض/الوصي</option>';
                                                                            echo '<option value="Does not want to participate |  لا يرغب في المشاركة">Does not want to participate |  لا يرغب في المشاركة</option>';
                                                                            echo '<option value="Already participated |  سبقت له المشاركة">Already participated |  سبقت له المشاركة</option>';
                                                                            echo '<option value="Others | أخرى">Others | أخرى</option>';  
                                                                        }
                                                                    ?>
                                                                    
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>

                                                        <?php if($count == 4): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Reason <br><p dir="rtl">السبب</p></label>
                                                                <select class="selectize" name="na_reason">
                                                                    <?php 
                                                                        if(!EMPTY($answer['na_reason']))
                                                                        {
                                                                            echo '<option value="">'.$answer['na_reason'].'</option>';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<option value="">SELECT</option>'; 
                                                                            echo '<option value="No Answer |  عدم الرد ">No Answer |  عدم الرد </option>';
                                                                            echo '<option value="Call Rejected |  رفض المكالمه">Call Rejected |  رفض المكالمه</option>';
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>

                                                        <?php if($count == 5): ?>
                                                        <div class="col-md-12 <?php echo $count;?>" style="display: none">
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="text" class="text-center" id="date" name="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Respondent Name</label>
                                                                <input type="text" class="text-center" id="date" name="">
                                                            </div>
                                                        </div>
                                                        <?php endif;?>
                                                    </td>

                                                <?php } ?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="padding-10">
                                        This call may be recorded for quality monitoring purposes and your identity will remain confidential unless you give permission to share it with the hospital
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        قد يتم تسجيل هذه المكالمة لغرض مراقبة الجودة، وسنحافظ على سرية هويتك إلا في حالة موافقتكم على مشاركتها مع المستشفى
                                    </td>
                                </tr>

                                <td colspan="5" class="border-bottom">

                                    <table width="100%">
                                        <tbody><tr>
                                            <td>
                                                Our records show that you visited <b><?php echo $answer['description'];?></b> on
                                            </td>
                                            <td dir="rtl">
                                                 تشير سجلاتنا إلى أنك زرت
                                                <b>مستشفى رعاية الرياض</b>
                                                ال (يوم ... بتاريخ ...)، هل هذا صحيح؟
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <center class="p-t-sm">

                                        <b>Discharge Date :</b> <input disabled="" name="visitdate" class="text-center datepicker" id="date2" value="<?php echo $answer['visit_date'];?>" type="text"><b> : تاريخ الخروج من المستشفى</b>

                                    </center>

                                    <table class="noborder_table" width="100%">
                                        <tbody><tr>
                                            <td>
                                                is that correct?
                                            </td>
                                            <td dir="rtl">
                                                هل هذا صحيح؟
                                            </td>
                                        </tr>
                                    </tbody></table>


                                    <br>
                                    <table class="noborder_table" width="100%">
                                    <tbody><tr>
                                        <td>
                                            <b>
                                            The questions of this survey are designed to evaluate your visit to Inpatient in
                                            <span ><?php echo $answer['visit_date'];?></span>.                                  All answers should relate to this specific visit.
                                            </b>
                                        </td>
                                        <td class="ar">
                                            

                                            <b>
                                            .<?php echo $answer['visit_date'];?> جميع أسئلة هذا الاستبيان خاصة بزيارتك لقسم المرضى المنومين بتاريخ
                                                                             ويجب أن تكون أجوبتك مرتبطة بهذه الزيارة فقط.
                                            </b>

                                        </td>
                                    </tr></tbody></table>

                                </td>


                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        <b>INSTRUCTIONS:</b><br>
                                        Choose the response that best describes your experience. If a question does not apply to you, please skip to the next question. Space is provided for you to comment on your experiences
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        <b>تعليمات:</b><br>
                                اختر الإجابة التي تصف تجربتك بأفضل وصف. إذا كان السؤال لا ينطبق على حالتك، الرجاء التجاوز إلى السؤال التالي. نوفر لك  المساحة للتعليق على تجربتك.
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab2">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td class="padding-10">
                                        <b>Basic Questions</b>
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        <b>أسئلة أساسية</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        1. Was this your first admission to this hospital?
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        1- هل كانت هذه أول إقامة لك في المستشفى؟
                                    </td>
                                </tr>
                                <tr class="border-bottom" id="q1">
                                    <td colspan="2"  class="text-center padding-20">
                                        <form class="form-inline">
                                            <div class="form-group p-r-sm">
                                                <input class="to-labelauty synch-icon" value="no" type="radio" name="1" <?php echo ($answer['q1'] == "1") ? "checked" : "disabled"?>
                                                data-labelauty="
                                                <br>
                                                <span dir='rtl'>لا</span>
                                                <br>No<br>
                                                " />
                                            </div>
                                            <div class="form-group">
                                                <input class="to-labelauty synch-icon" value="yes" type="radio" name="1" <?php echo ($answer['q1'] == "2") ? "checked" : "disabled"?>
                                                data-labelauty="
                                                <br>
                                                <span dir='rtl'>نعم</span>
                                                <br>Yes<br>
                                                " />
                                            </div>
                                        </form>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="padding-10">
                                        2. Did you have a private room? 
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        2- هل كانت لك غرفة منفردة؟
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <td colspan="2"  class="text-center padding-20">
                                        <form class="form-inline">
                                            <div class="form-group p-r-sm">
                                                <input class="to-labelauty synch-icon" value="no" type="radio" name="2" <?php echo ($answer['q2'] == "1") ? "checked" : "disabled"?>
                                                data-labelauty="
                                                <br>
                                                <span dir='rtl'>لا</span>
                                                <br>No<br>
                                                " />
                                            </div>
                                            <div class="form-group">
                                                <input class="to-labelauty synch-icon" value="yes" type="radio" name="2" <?php echo ($answer['q2'] == "2") ? "checked" : "disabled"?>
                                                data-labelauty="
                                                <br>
                                                <span dir='rtl'>نعم</span>
                                                <br>Yes<br>
                                                " />
                                            </div>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments1" disabled><?php echo $answer['comments1']?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab3">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td colspan="2">
                                        <b>Admission</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>دخول المستشفى</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        1. Speed of admission process
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- سرعة إجراءات الدخول إلى المستشفى
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q3'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q3'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                 $result = ($answer['q3'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                 $result = ($answer['q3'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                 $result = ($answer['q3'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>"  type="radio" name="3"  <?php echo $result ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        2. Courtesy of the person who admitted you
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- اهتمام الشخص الذي قام بإدخالك المستشفى
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q4'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q4'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q4'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q4'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q4'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="4" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                
                                </tr><tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments2" disabled><?php echo $answer['comments2'];?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab4">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td colspan="2">
                                        <b>Room</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>الغرفة</b>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        1. Appearance of room
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- المظهر العام للغرفة
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q5'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q5'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q5'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q5'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q5'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="5" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        2. Room cleanliness
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        2- نظافة الغرفة
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q6'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q6'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q6'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q6'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q6'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="6" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        3. Courtesy of the person who cleaned your room
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        3- اهتمام الشخص الذي قام بتنظيف الغرفة
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q7'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q7'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q7'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q7'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q7'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="7" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                    4. Room temperature
                                    </td>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    4- درجة حرارة الغرفة
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q8'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q8'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q8'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q8'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q8'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="8" <?php echo $result ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                    5. Noise level in and around room
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    5- هدوء الغرفة وما حولها
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q9'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q9'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q9'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q9'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q9'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="9" <?php echo $result ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments3" disabled><?php echo $answer['comments3'];?></textarea>
                                    </td>
                                </tr>
                            </table>    

                        </div>

                    </div>

                    <div class="tab-pane" id="tab5">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td colspan="2">
                                        <b>Meals</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        <b>الوجبات</b>
                                    </td>
                                </tr>

                                <!--QUESTIONS START -->

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        1. Temperature of the food (cold foods cold, hot foods hot)
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    1- درجة حرارة الطعام (برودة الأطعمة الباردة، وسخونة الأطعمة الساخنة)
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q10'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q10'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q10'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q10'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q10'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="10" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                    2. Quality of the food
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    2-  جودة الطعام
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q11'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q11'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q11'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q11'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q11'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="11" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                    3. Courtesy of the person who served your food
                                    </td>
                                </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    3- اهتمام الشخص الذي قدم الطعام لك</td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q12'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q12'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q12'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q12'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q12'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="12" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                    4. In case you were placed on a special or strict diet, was it explained to you?
                                    </td>
                                </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    4- في حالة أن تكون قد وضعت على حمية خاصة أو محددة، ما مدى شرحها لك؟
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q13'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q13'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q13'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q13'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q13'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="13" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments4" disabled><?php echo $answer['comments4']; ?></textarea>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab6">

                        <div class="row">

                            <table class="table border-bottom">

                                <tr class="border-bottom h-question">
                                    <td colspan="2">
                                    <b>Nurses</b>
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    <b>فريق التمريض</b>
                                    </td>
                                </tr>

                                <!--QUESTIONS START -->

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        1. Friendliness/courtesy of the nurses
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        1- اهتمام فريق التمريض
                                    </td>
                                </tr>
                                <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q14'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q14'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q14'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q14'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q14'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="14" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                2. Promptness in responding to the call button
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                2- سرعة الاستجابة لزر الاستدعاء 
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q15'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q15'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q15'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q15'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q15'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="15" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                3. Nurses' attitude toward your requests
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                3- أسلوب فريق التمريض تجاه طلباتك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q16'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q16'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q16'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q16'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q16'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="16" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                    4. Amount of attention paid to your special or personal needs
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                4- مدى مراعاة فريق التمريض لاحتياجاتك الخاصة أو الشخصية
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q17'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q17'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q17'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q17'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q17'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="17" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                    5. How well the nurses kept you informed
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                5- حرص فريق التمريض بإطلاعك على ما يجري
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q18'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q18'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q18'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q18'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q18'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="18" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                6. Skill of the nurses
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                6- مهارة فريق التمريض
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q19'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q19'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q19'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q19'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q19'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="19" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                    7. Assistance you received with daily personal care (bathing, using bathroom/bedpan, walking, etc.)
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                7- المساعدة التي حصلت عليها للرعاية اليومية (الاستحمام، استخدام الحمام  / المبولة، المشي ... إلخ)
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q20'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q20'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q20'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q20'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q20'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="20" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                8. Extent to which nurses checked ID bracelets before giving you medications
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                8- مدى فحص طاقم التمريض لأسوارة التعريف الخاصة بك قبل إعطائك أي أدوية
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q21'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q21'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q21'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q21'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q21'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="21" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments5" disabled><?php echo $answer['comments5'];?></textarea>
                                </td>
                            </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab7">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>Physician</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>الطبيب</b>
                                </td>
                            </tr>

                            <!--QUESTIONS START -->

                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Time physician spent with you
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- الوقت الذي قضاه الطبيب معك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q22'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q22'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q22'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q22'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q22'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="22" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                2. Physician's concern for your questions and worries
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- اهتمام الطبيب بأسئلتك ودواعي قلقك 
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q23'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q23'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q23'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q23'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q23'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="23" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                3. How well physician kept you informed
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- حرص الطبيب بإطلاعك على ما يجري
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q24'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q24'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q24'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q24'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q24'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="24" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                4. Friendliness/courtesy of physician
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- اهتمام الطبيب
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q25'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q25'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q25'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q25'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q25'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="25" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                5. Skill of physician
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                5- مهارة الطبيب
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q26'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q26'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q26'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q26'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q26'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="26" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments6" disabled><?php echo $answer['comments6']; ?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab8">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>OR Experience</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>تجربتك في غرفة العمليات</b>
                                </td>
                            </tr>

                            <!--QUESTIONS START -->

                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Did you undergo surgery during this hospital stay?
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- هل خضعت لعملية جراحية خلال إقامتك هذه بالمستشفى؟
                                </td>
                            </tr>

                            <tr class="border-bottom">
                               <td colspan="4"  class="text-center padding-20">
                                    <form class="form-inline">
                                        <div class="form-group p-r-sm">
                                            <input class="to-labelauty synch-icon skipme" data-show="false" value="no" type="radio" name="27" <?php echo ($answer['q27'] == "1" ? "checked" : "disabled")?>
                                            data-labelauty="<br><br>
                                            <p dir='ltr'>لا (انتقل للقسم التالي)</p>
                                            <br>
                                            No [Proceed to Next Section]<br><br>
                                            " />
                                        </div>
                                        <div class="form-group">
                                            <input class="to-labelauty synch-icon skipme" data-show="false" value="yes" type="radio" name="27" <?php echo ($answer['q27'] == "2" ? "checked" : "disabled")?>
                                            data-labelauty="<br><br>
                                            <p dir='ltr'>نعم</p>
                                            <br>
                                            Yes<br><br>
                                            " />
                                        </div>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom false" style="display: none">
                                    <td colspan="2">
                                2. Waiting time prior to the start of the surgery
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- مدة الانتظار قبل بدء العملية
                                </td>
                            </tr>
                            <tr class="border-bottom false" style="display: none">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q28'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q28'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q28'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q28'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q28'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                    <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="28" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                    </td>

                                <?php } ?>
                            </tr>

                            <tr class="border-bottom false" style="display: none">
                                    <td colspan="2">
                                3. Instructions you were given by the OR staff about how to prepare for your surgery
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- التعليمات التي قدمها لك طاقم غرفة العمليات عن كيفية التحضير للعملية
                                </td>
                            </tr>
                            <tr class="border-bottom false" style="display: none">

                                    <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q29'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q29'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q29'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q29'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q29'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="29" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom false" style="display: none">
                                    <td colspan="2">
                                4. Your confidence that OR staff correctly identified you and your procedure prior to surgery
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- ثقتك بأن طاقم غرفة العمليات تأكدوا من هويتك وإجراءك الطبي بشكل صحيح قبل العملية<br>&nbsp;
                                </td>
                            </tr>
                            <tr class="border-bottom false" style="display: none">

                                    <?php 
                                        $text = '';
                                        $result = ''; 
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $result = ($answer['q30'] == "1") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $result = ($answer['q30'] == "2") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $result = ($answer['q30'] == "3") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $result = ($answer['q30'] == "4") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $result = ($answer['q30'] == "5") ? "checked" : "disabled";
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="30" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom false" style="display: none">
                                    <td colspan="2">
                                5. Adequacy of information family received during surgery
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                5- المعلومات المقدمة لعائلتك أثناء إجراء العملية
                                </td>
                            </tr>
                            <tr class="border-bottom false" style="display: none">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q31'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q31'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q31'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q31'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q31'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="31" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom false" style="display: none">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom false" style="display: none">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments7" disabled><?php echo $answer['comments7'];?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab9">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>Anaesthesia</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>التخدير</b>
                                </td>
                            </tr>

                            <!--QUESTIONS START -->

                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Did you undergo anaesthesia before your surgery or procedure?
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- هل خضعت للتخدير قبل العملية / الإجراء الطبي ؟
                                </td>
                            </tr>

                            <tr class="border-bottom text-center">
                                <td colspan="4"  class="text-center padding-20">
                                    <form class="form-inline">
                                        <div class="form-group p-r-sm">
                                            <input class="to-labelauty synch-icon skipme" data-show="false" value="no" type="radio" name="32" <?php echo ($answer['q32'] == "1" ? "checked" : "disabled")?>
                                            data-labelauty="<br><br>
                                            <p dir='ltr'>لا (انتقل للقسم التالي)</p>
                                            <br>
                                            No [Proceed to Next Section]<br><br>
                                            " />
                                        </div>
                                        <div class="form-group">
                                            <input class="to-labelauty synch-icon skipme" data-show="false" value="yes" type="radio" name="32" <?php echo ($answer['q32'] == "2" ? "checked" : "disabled")?>
                                            data-labelauty="<br><br>
                                            <p dir='ltr'>نعم</p>
                                            <br>
                                            Yes<br><br>
                                            " />
                                        </div>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom false2" style="display: none">
                                    <td colspan="2">
                                2. Explanation by anesthesia staff
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- الشرح الذي قدمه لك فريق التخدير
                                </td>
                            </tr>
                            <tr class="border-bottom false2" style="display: none">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q33'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q33'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q33'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q33'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q33'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="33" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom false2" style="display: none">
                                    <td colspan="2">
                                3. Friendliness/courtesy of the anesthesia staff
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3-  اهتمام فريق التخدير
                                </td>
                            </tr>
                            <tr class="border-bottom false2" style="display: none">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q34'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q34'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q34'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q34'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q34'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="34" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom false2" style="display: none">
                                    <td colspan="2">
                                4. Your rating of anesthesia experience
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- تقييمك لتجربة التخدير
                                </td>
                            </tr>
                            <tr class="border-bottom false2" style="display: none">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q35'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q35'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q35'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q35'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q35'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="35" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom false2" style="display: none">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom false2" style="display: none">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments8" disabled ><?php echo $answer['comments8']; ?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab10">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>Tests and Treatments</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>الاختبارات والعلاج</b>
                                </td>
                            </tr>

                            <!--QUESTIONS START -->

                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Waiting time for tests or treatments
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- فترة الانتظار للاختبارات والعلاج
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q36'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q36'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q36'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q36'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q36'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="36" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                2. Explanations about what would happen during tests and treatments
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- الشرح عن ما سيحدث خلال الاختبارات والعلاج
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q37'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q37'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q37'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q37'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q37'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="37" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                3. Courtesy of the person who took your blood
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- اهتمام الشخص الذي قام بسحب عينة الدم
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q38'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q38'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q38'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q38'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q38'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="38" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                4. Skill of the person who took your blood (e.g., did it quickly, with minimal pain)
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- مهارة الشخص الذي قام بسحب عينة الدم (قام بسحبها بسرعة، وبأقل ألم)
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q39'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q39'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q39'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q39'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q39'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="39" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                5. Courtesy of the person who started the IV
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                5- اهتمام الشخص الذي قام بوضع المحلول الوريدي
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q40'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q40'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q40'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q40'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q40'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="40" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments9" disabled><?php echo $answer['comments9']; ?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab11">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question" h-question>
                                    <td colspan="2">
                                <b>Personal Issues</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>أمور شخصية</b>
                                </td>
                            </tr>

                            <!-- QUESTION START -->
                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Staff concern for your privacy
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- مراعاه العاملين لخصوصيتك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q41'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q41'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q41'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q41'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q41'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="41" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                2. How well your pain was controlled
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- ما مدى السيطرة على شعورك بالألم
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q42'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q42'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q42'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q42'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q42'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="42" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                3. Degree to which hospital staff addressed your emotional needs
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- استجابة العاملين بالمستشفى لاحتياجاتك النفسية
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q43'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q43'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q43'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q43'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q43'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="43" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                4. Response to concerns/complaints made during your stay
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- الاستجابة للمخاوف والشكاوى التي أعربت عنها خلال إقامتك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q44'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q44'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q44'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q44'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q44'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="44" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                5. Staff effort to include you in decisions about your treatment
                                </td>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                5- الجهد الذي بذله العاملون لإشراكك في القرارات الخاصة بعلاجك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q45'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q45'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q45'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q45'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q45'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="45" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                6. Staff introduction of themselves
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                6- تعريف الموظفين بأنفسهم لك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q46'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q46'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q46'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q46'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q46'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="46" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                7. Staff hand hygiene before examination
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                7- مراعاة العاملين لتعقيم أيديهم قبل فحصك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q47'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q47'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q47'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q47'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q47'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="47" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                8. Friendliness/courtesy of patient transporters
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                8- اهتمام العاملين الذين قاموا بنقلك في المستشفى
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q48'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q48'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q48'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q48'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q48'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="48" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                9. Extent to which you were informed about all of the medications you received in the hospital (including potential side-effects)
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                9- مدى إطلاعك على جميع الأدوية التي أخذتها في المستشفى (بما في ذلك الأعراض الجانبية المحتملة)
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q49'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q49'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q49'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q49'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q49'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="49" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments10" disabled><?php echo $answer['comments10']; ?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab12">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>Visitors and Family</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>الزوار والعائلة</b>
                                </td>
                            </tr>

                            <!-- QUESTION START -->

                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Accommodations and comfort for visitors
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- تجهيزات وراحة الزوار
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q50'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q50'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q50'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q50'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q50'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="50" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                2. Staff attitude toward your visitors
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- أسلوب العاملين تجاه الزوار
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q51'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q51'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q51'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q51'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q51'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>


                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="51" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                3. Rating of waiting areas for your visitors
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- تقييم منطقة انتظار الزوار
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q52'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q52'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q52'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q52'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q52'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="52" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments13" disabled ><?php echo $answer['comments13']; ?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab13">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>Discharge</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>الخروج من المستشفى</b>
                                </td>
                            </tr>

                            <!-- QUESTION START -->
                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. Extent to which you felt ready to be discharged
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- مدى شعورك بالاستعداد للخروج من المستشفى
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q53'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q53'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q53'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q53'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q53'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="53" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                2. Speed of discharge process after you were told you could go home
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- سرعة إجراءات الخروج من المستشفى بعد إخبارك أنه بإمكانك العودة للمنزل
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q54'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q54'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q54'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q54'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q54'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="54" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                3. Instructions given about how to care for yourself at home
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- التعليمات التي حصلت عليها بشأن العناية بنفسك في المنزل
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q55'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q55'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q55'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q55'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q55'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="55" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                4. Explanations regarding taking medicine after discharge including potential side-effects
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- الشرح عن الأدوية التي ستأخذها بعد الخروج، بما في ذلك الأعراض الجانبية المحتملة 
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q56'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q56'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q56'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q56'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q56'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="56" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                5. Your evaluation of the waiting time to receive medications from the pharmacy
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                5- تقييمك لفترة الانتظار للحصول على الأدوية الموصوفة
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q57'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q57'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q57'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q57'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q57'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="57" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                6. Your understanding of follow-up care instructions (appointments, when to call doctor for help) after discharge
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                6- فهمك لتعليمات متابعة العلاج بعد الخروج (موعد المراجعة، متى تتصل بالطبيب للمساعدة)<br>&nbsp;
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q58'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q58'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q58'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q58'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q58'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="58" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                        Comments<br>
                                        (describe good or bad experience):
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                        تعليقات<br>
                                        (صف التجارب جيدة أو سيئة):
                                    </td>
                                    
                                </tr>

                                <tr class="border-bottom">
                                    <td colspan="5">
                                        <textarea rows="5" class="form-control" name="comments12" disabled><?php echo $answer['comments12']; ?></textarea>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="tab-pane" id="tab14">

                        <div class="row">

                            <table class="table border-bottom">

                            <tr class="border-bottom h-question">
                                    <td colspan="2">
                                <b>Overall Assesment</b>
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                <b>التقييم العام</b>
                                </td>
                            </tr>

                            <!-- QUESTION START -->
                            <tr class="border-bottom">
                                    <td colspan="2">
                                1. How well staff worked together to care for you
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                1- مدى تعاون العاملين في تقديم الرعاية لك
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q59'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q59'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q59'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q59'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q59'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="59" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                2. Likelihood of your recommending this hospital to others
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                2- احتمالية أن توصي بهذا المستشفى للآخرين
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q60'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q60'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q60'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q60'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q60'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>
                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="60" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                3. Overall rating of care given at hospital
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                3- تقييمك العام للرعاية التي تلقيتها في المستشفى
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <?php 
                                    $text = '';
                                    $result = ''; 
                                    for($count = 1;$count <=5;$count++){ 

                                    switch ($count) {
                                        case '1':
                                            $result = ($answer['q61'] == "1") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                            break;
                                        case '2':
                                            $result = ($answer['q61'] == "2") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                            break;
                                        case '3':
                                            $result = ($answer['q61'] == "3") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                            break;
                                        case '4':
                                            $result = ($answer['q61'] == "4") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                            break;
                                        default:
                                            $result = ($answer['q61'] == "5") ? "checked" : "disabled";
                                            $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                            break;
                                    }
                                ?>

                                        <td class="text-center" width="20%">
                                            <input class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="61" <?php echo $result; ?>
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </td>

                                    <?php } ?>
                                </tr>

                            <tr class="border-bottom">
                                    <td colspan="2">
                                4. Was there any team member that provided exceptional care / service? Who?
                            </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                4- هل قدّم لك أحد الموظفين رعاية مميزة تذكر؟ هل تذكر اسمهـ(ا)؟
                                </td>
                            </tr>
                            <tr class="border-bottom text-center">
                                <td colspan="3">
                                    <input class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="no" name="hl_qtm" <?php echo ($answer['hl_qtm'] == "1") ? "checked" : "disabled" ?>
                                        data-labelauty="<br>
                                        <p dir='rtl'>لا</p>
                                        <br>
                                        No<br><br>
                                        " />
                                </td>
                                <td colspan="2">
                                    <div class="col-md-12 text-center">
                                        <p>
                                            <input class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="yes" name="hl_qtm" <?php echo ($answer['hl_qtm'] == "2") ? "checked" : "disabled" ?>
                                             data-labelauty="
                                                <br>
                                                <p dir='rtl'>نعم (اذكر الإسم والقسم)</p>
                                                <br>
                                                Yes (Mention name & division)<br><br>
                                             "
                                            />
                                        </p>
                                    </div>
                                    <div class="col-md-8 col-md-offset-2" id="ND">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="Name" name="hl_atm" value="<?php echo $answer['hl_atm']; ?>" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Division</label>
                                            <input type="text" class="form-control" placeholder="Division" name="hl_atmd" value="<?php echo $answer['hl_atmd']; ?>" disabled>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="2">
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right" colspan="3">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" data-show="extra" name="comments11" disabled ><?php echo $answer['comments11'];?></textarea>
                                </td>
                            </tr>

                            <tr class="border-bottom extra" style="display: none">
                                    <td colspan="2">
                                        Would you like to share your contact information along the comments you gave with the hospital?
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    سيتم إرسال تعليقاتك للمستشفى، هل ترغب بإرفاق اسمك ورقم هاتفك معها؟<br>&nbsp;
                                    </td>
                            </tr>

                            <tr class="border-bottom text-center extra" style="display: none">
                                <td colspan="3">
                                    <input class="to-labelauty synch-icon skipme" value="no" data-show="false" type="radio" name="62" 
                                    data-labelauty="
                                    <br>
                                    <p dir='rtl'>لا</p>
                                    <br>
                                    No<br><br>
                                    " />
                                </td>
                                <td colspan="2">
                                    <input class="to-labelauty synch-icon skipme" data-show="false" value="yes" type="radio" name="62" 
                                    data-labelauty="
                                    <br>
                                    <p dir='rtl'>نعم</p>
                                    <br>
                                    Yes<br><br>
                                    " />
                                </td>
                            </tr>

                            <tr class="border-bottom extra" style="display: none;">
                                    <td colspan="2">
                                        Have you had any issues with the care you received and would like a hospital official to contact you?
                                    </td>
                                    <td dir="rtl" class="text-right" colspan="3">
                                    هل واجهتك أي مشاكل في العناية الطبية التي تلقيتها وترغب بأن يقوم مسؤول في المستشفى بالتواصل بك؟
                                    </td>
                            </tr>
                            <tr class="border-bottom text-center extra" style="display: none;">
                                <td colspan="3">
                                    <input class="to-labelauty synch-icon ACTION" data-show="#CD" value="no" type="radio" name="hl_contact" 
                                    data-labelauty="
                                    <br>
                                    <p dir='rtl'>لا</p>
                                    <br>
                                    No<br><br>
                                    " />
                                </td>
                                <td colspan="2" class="false">
                                    <div class="col-md-12 text-center">
                                        <p>
                                            <input class="to-labelauty synch-icon ACTION" data-show="#CD" value="yes" type="radio" name="hl_contact" 
                                            data-labelauty="
                                            <br>
                                            <p dir='rtl'>نعم (طريقة التواصل)</p>
                                            <br>
                                            Yes (contact details)<br><br>
                                            " />
                                        </p>
                                    </div>
                                    <div class="col-md-8 col-md-offset-2" id="CD">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="Name" name="hl_contact_name">
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile No.</label>
                                            <input type="text" class="form-control" placeholder="Mobile No." name="hl_contact_no">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </table>
                        </div>

                    </div>

                     <div class="row prog">
                        <div id="bar" class="progress">
                            <div class="progress-bar active progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                
                            </div>
                        </div>
                    </div>

                    <ul class="pager wizard" >
                        <li class="previous first" style="display:none;"><a href="javascript:;">First</a></li>
                        <li class="previous"><a href="javascript:;">Previous</a></li>
                        <li class="next last" style="display:none;"><a href="javascript:;">Last</a></li>
                        <li class="next"><a href="javascript:;">Next</a></li>
                        <li class="finish pull-right"><a href="javascript:;">Finish <i class="fa fa-check"></i></a></li>
                    </ul>

                    <div class="text-center not-proceeding">
                        <a href="report/inpatient" class="btn btn-info btn-lg">BACK TO SURVEY LIST</a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>