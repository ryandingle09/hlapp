<section class="content-header p-t-xs p-b-md">
    <h1>
        Summary Report
        <small>Summary report of Survey</small>
    </h1>
    <ol class="breadcrumb p-t-n">
        <li><a href="dashboard/">Home</a></li>
        <li><a href="dashboard/">Care</a></li>
        <li><a href="report/summary">Reports</a></li>
        <li><a class="active">Summary</a></li>
    </ol>
</section>
<div class="p-xxl p-t-lg" style="background: white; height: 500px;">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Date From:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" id="start_date" class="form-control pull-right">
                </div><!-- /.input group -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Date To:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" id="end_date" class="form-control pull-right">
                </div><!-- /.input group -->
            </div>
        </div>
    </div>
   
    <div class="row p-b-md">
        <div class="col-md-6 col-md-offset-3"><button class="btn btn-block btn-primary btn-flat" id="submit">Submit</button></div>
    </div>

    <div class="p-t-md">
        <center>
            <h4>Direct Download Link: </h4>
            <h4 id="link"></h4>            
        </center>      
    </div>

</div>

<?php 

$ddate = date("Y-m-d");

$date  = DateTime::createFromFormat("Y-m-d", $ddate);
$year  = substr($date->format("Y"), 3);
$week  = $date->format("W");
$month = strtoupper(substr($date->format("M"), 0,-2));

$name = "CCAD".$year.$month."_".$week.".xls";
?>


<script type="text/javascript">
   
    $('#submit').click(function(){
        var start_date = $( "#start_date" ).val();
        var end_date   = $( "#end_date" ).val();
        var name       = <?php echo json_encode($name) ?>;
        var result     ='';     
        
        if(start_date == "" && end_date == "")
        {   
            result +='<h4>No Result</h4>';
        }
        else
        {
            if(start_date != "" && end_date != "")
            {
                result += '<a target="_blank" href="report/summary_generate/'+start_date+'/'+end_date+'">'+name+'</a>';
            }
        }

        $('#link').html(result);
    });

</script>



