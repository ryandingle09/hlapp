<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config = array(

        'user_access' => array(

                array(
                        'field' => 'employee_id',
                        'label' => 'Employee ID',
                        'rules' => 'required|trim|max_length[20]|is_numeric'
                ),
                array(
                        'field' => 'usertype',
                        'label' => 'User Type',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'username',
                        'label' => 'User Name',
                        'rules' => 'required|trim|max_length[255]'
                ),
                array(
                        'field' => 'fullname',
                        'label' => 'Full Name',
                        'rules' => 'required|trim|max_length[100]'
                ),
                array(
                        'field' => 'emailadd',
                        'label' => 'Email Address',
                        'rules' => 'required|trim|max_length[255]|valid_email'
                ),
                array(
                        'field' => 'jobtitle',
                        'label' => 'Job Title',
                        'rules' => 'required|trim|max_length[255]'
                ),
                array(
                        'field' => 'dept_code',
                        'label' => 'Dept Code',
                        'rules' => 'required|trim|max_length[50]'
                ),
                array(
                        'field' => 'amendby',
                        'label' => 'Amend By',
                        'rules' => 'required|trim|max_length[255]'
                ),
                 array(
                        'field' => 'amenddate',
                        'label' => 'Amend Date',
                        'rules' => 'required|trim'
                ),

        ),

        
);

?>