<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Kolkata');

class User_access extends MX_Controller{
	

	function __construct(){

		parent::__construct();

		$this->load->model('user_access_model', 'user_access');
		$this->form_validation->CI =& $this;
	}


	public function index()
	{
		$data = [
			'' => ''
		];

		$this->layouts->view('user_list', $data);
	}

	public function activate()
	{
		$action 		= $this->input->post('action');
		$identity 		= $this->input->post('identity');
		$id 			= $this->input->post('id');
		$employee_id 	= $this->input->post('employee_id');

		$this->user_access->activate($action, $identity, $id, $employee_id);
	}

	public function process()
	{
		$action 	= $this->uri->segment(3);
		$identity 	= $this->uri->segment(4);
		$id 	 	= $this->input->post('id');

		if($action == 'delete')
		{
			$this->user_access->process($action, $identity, $id);
		}
		else
		{
			if($this->form_validation->run('user_access') == FALSE) echo validation_errors();
			else
			{
				if($action == 'post')
				{
					#check
					$email 			= $this->user_access->check('email');
					$username 		= $this->user_access->check('username');
					$employee_id 	= $this->user_access->check('employee_id');

					if($email == true && $username == true && $employee_id == true) 
						$this->user_access->process($action, $identity, $id);
				}
				else //update
				{
					if($this->input->post('emailadd') != $this->input->post('eold') || $this->input->post('employee_id') != $this->input->post('id') || $this->input->post('username') != $this->input->post('uold'))
					{
						if($this->input->post('emailadd') != $this->input->post('eold'))
						{
							$this->user_access->check('email');
						}
						elseif($this->input->post('employee_id') != $this->input->post('id'))
						{
							$this->user_access->check('employee_id');
						}
						elseif($this->input->post('username') != $this->input->post('uold'))
						{
							$this->user_access->check('username');
						}
						else
						{
							$this->user_access->process($action, $identity, $id);
						}
					}
					else $this->user_access->process($action, $identity, $id);
				}
			}
		}
	}

	public function load_modal()
	{
		$data = [];
		$identity 	= $this->input->post('identity');
		$id 		= $this->input->post('id'); //client id
		$file_path 	= '';
		$file_name 	= '';
		
		$data = [
			'id' 					=> $id,
			'data' 					=> $this->user_access->get_userinfo($id),
			'clients'  				=> $this->user_access->get_clients(),
			'user_clients'  		=> $this->user_access->get_user_clients($id),
			'modules' 				=> $this->user_access->get_modules($id),
			'user_modules' 			=> $this->user_access->get_user_modules($id),
		];

		$this->load->view('modal/'.$this->input->post('content').'', $data);
	}

	public function get_data_table()
    {
		$aColumns = [
			"employee_id", 
			"username", 
			"usertype", 
			"fullname", 
			"jobtitle",
			"emailadd",
			"dept_code",
			"amendby",
			"amenddate"
		];

        $sEcho = $this->input->get_post('sEcho', true);

        $rResult = $this->user_access->get_user_list($aColumns);

        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
        $iTotal = $this->user_access->total_length();

        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );

        foreach($rResult->result_array() as $aRow)
        {
    		$action = '';
    		$color  = '';
            $row = array();
            foreach($aColumns as $col)
            {
            	$color = ($aRow['usertype'] == 'admin') ? 'btn-danger' : 'btn-info';

               	$row[] = $aRow['username'];
               	$row[] = $aRow['fullname']; 
               	$row[] = $aRow['emailadd'];
               	$row[] = '<span class="badge '.$color.' ">'.$aRow['usertype'].'</span>';  
               	$row[] = $aRow['jobtitle'];
               	$row[] = $aRow['dept_code'];
               	$row[] = $aRow['amendby'];
               	$row[] = date('M/d/Y', strtotime($aRow['amenddate']));
               	$action  = ($this->session->userdata('id') == $aRow['employee_id']) ? 
               	'
               		<div class="text-center">
						<a href="javascript:;" class="btn btn-warning show_modal" data-modal="user_access_modal" data-action="get" data-identity="user_access" data-id="'.$aRow['employee_id'].'" data-toggle="tooltip" data-placement="bottom" title="Show User Access"><i class="fa fa-key"></i> </a>
					</div>
               	' 
               	: 
               	'
               		<div class="text-center">
           				<a href="javascript:;" class="btn btn-default show_modal" data-modal="user_access_modal" data-action="edit" data-identity="user_access" data-id="'.$aRow['employee_id'].'" data-toggle="tooltip" data-placement="left" title="Edit User Information"><i class="fa fa-edit"></i></a>
						<a href="javascript:;" class="btn btn-warning show_modal" data-modal="user_access_modal" data-action="get" data-identity="user_access" data-id="'.$aRow['employee_id'].'" data-toggle="tooltip" data-placement="bottom" title="Show User Access"><i class="fa fa-key"></i> </a>
						<a href="javascript:;" class="btn btn-danger show_modal" data-action="delete" data-identity="user_access" data-id="'.$aRow['employee_id'].'" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fa fa-trash"></i></a>
					</div>
               	';
            	$row[] = $action;	 
            }

            $output['aaData'][] = $row;
        }

        echo json_encode($output);

    }

}


?>