<div class="modal-header">
	<button type="button" class="close close_modal" data-modal="user_access_modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title"><i class="fa fa-key"></i> <?php echo ($this->session->userdata('id') == $id) ? 'Your' : 'User';?> Access</h4>
</div>

<div class="modal-body user_access">

	<div class="row">

		<div class="col-md-12">
			<h4>Client Access</h4>
			<table class="table" cellpadding="0" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Client Name</th>
						<th>Description</th>
						<th class="text-center">Status</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($clients as $row): ?>
					<?php 
						$status = '<span class="badge">Deactivated</span>';
						$button = '<button type="button" class="btn btn-info btn-sm activate" data-action="activate" data-identity="client" data-id="'.$row->client_id.'" data-employee_id="'.$id.'">Activate <i class="fa fa-check"></i></button>';

						foreach($user_clients as $row2) : 

						if($row2->client_id == $row->client_id):
							$status = '<span class="badge alert-success">Activated</span>';
							$button = '<button type="button" class="btn btn-danger btn-sm activate" data-action="deactivate" data-identity="client" data-id="'.$row->client_id.'" data-employee_id="'.$id.'">Deactivate <i class="fa fa-close"></i></button>';
						endif;
					?>

					<?php endforeach;?>
					<tr>
						<td><?php echo $row->client_name;?></td>
						<td><?php echo $row->client_description;?></td>
						<td class="text-center status_c_<?php echo $row->client_id;?>">
							<p class="text-center"><?php echo $status;?></p>
						</td>
						<td class="text-center action_c_<?php echo $row->client_id;?>">
						<?php echo $button;?>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>

		<div class="col-md-12">
			<h4>Module Access</h4>
			<table class="table" cellpadding="0" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Module Name</th>
						<th>Description</th>
						<th>Type</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($modules as $row): ?>
					<?php 
						$status = '<span class="badge">Deactivated</span>';
						$button = '<button type="button" class="btn btn-info btn-sm activate" data-action="activate" data-identity="module" data-id="'.$row->module_id.'" data-employee_id="'.$id.'">Activate <i class="fa fa-check"></i></button>';

						foreach($user_modules as $row2) : 

						if($row2->module_id == $row->module_id):
							$status = '<span class="badge alert-success">Activated</span>';
							$button = '<button type="button" class="btn btn-danger btn-sm activate" data-action="deactivate" data-identity="module" data-id="'.$row->module_id.'" data-employee_id="'.$id.'">Deactivate <i class="fa fa-close"></i></button>';
						endif;
					?>

					<?php endforeach;?>
					<tr>
						<td><?php echo $row->module_name;?></td>
						<td><?php echo $row->module_description;?></td>
						<td><?php echo ($row->is_menu == 1) ? 'Dropdown Menu' : 'Module';?></td>
						<td class="text-center status_m_<?php echo $row->module_id;?>">
							<p class="text-center"><?php echo $status;?></p>
						</td>
						<td class="text-center action_m_<?php echo $row->module_id;?>">
						<?php echo $button;?>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default close_modal" data-modal="user_access_modal">Close</button>
</div>