<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="caresurvey/dashboard" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li class="active">User Access</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          	<h3>
				<i class="fa fa-key"></i> User Access -<small>&nbsp; Showing list of Users</small>
				<button class="btn btn-info pull-right show_modal"  data-modal="user_access_modal" data-action="post" data-identity="user_access" data-id=""><i class="fa fa-plus"></i> Add New User</button>
			</h3>
        </div>
    </div>
</div>

<div class="row action">

	<div class="col-md-12">
		<?php if(isset($_GET['message']) && $_GET['message'] == 'post'):?>
		<div class="alert alert-success">
			<p>
				Successfully Added. <i class="fa fa-check"></i><br>
				<i class="fa fa-info"></i> The newly added user will use <b><i>'default'</i></b> for his/her <b><i>password to login in to this system</i></b>.
			</p>
		</div>
		<?php endif;?>
		<?php if(isset($_GET['message']) && $_GET['message'] == 'update'):?>
		<div class="alert alert-success">
			<p>
				Successfully updated. <i class="fa fa-check"></i><br>
			</p>
		</div>
		<?php endif;?>
	</div>

	<div class="col-md-12">
		<table id="users" class="table table-striped" width="100%" cellspacing="0">
			<thead>
			    <tr>
			        <th>Username</th>
			        <th>Full Name</th>
			        <th>Email Address</th>
			        <th>User Type</th>
			        <th>Job Title</th>
			        <th>Dept Code</th>
			        <th>Amend By</th>
			        <th>Ament Date</th>
			        <th width="15%" class="text-center">Action</th>
			    </tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>