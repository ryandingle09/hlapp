<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Module_model extends DBCARE_Model{


	public function process($action, $identity, $id)
	{
		if($identity == 'user_access')
		{
			if(empty($id)) //POST
			{
				$salt                 = md5('health');
	            $pepper               = md5('links');
	            $newpass              = $salt.sha1('default').$pepper;

				$this->db->insert(DBCARE_Model::tbl_userinfo,[
					'usertype' 		=> $this->input->post('usertype'),
					'employee_id' 	=> $this->input->post('employee_id'),
					'username' 		=> $this->input->post('username'),
					'password' 		=> $newpass,
					'fullname' 		=> $this->input->post('fullname'),
					'emailadd' 		=> $this->input->post('emailadd'),
					'jobtitle' 		=> $this->input->post('jobtitle'),
					'dept_code' 	=> $this->input->post('dept_code'),
					'amendby' 		=> $this->input->post('amendby'),
					'amenddate' 	=> date('Y-m-d m:i:s', strtotime($this->input->post('amenddate'))),
				]);

				echo '1';
			}
			else //UPDATE or DELETE
			{
				if($action == 'delete')
				{
					//user_modules activated
					$this->db->where(['employee_id' => $id]);
					$this->db->delete(DBCARE_Model::tbl_user_modules);

					//user_clients activated
					$this->db->where(['employee_id' => $id]);
					$this->db->delete(DBCARE_Model::tbl_user_clients);

					//userinfo table
					$this->db->where(['employee_id' => $id]);
					$this->db->delete(DBCARE_Model::tbl_userinfo);

					echo '1';
				}
				else
				{
					$this->db->where(['employee_id' => $id]);
					$this->db->update(DBCARE_Model::tbl_userinfo,[
						'usertype' 		=> $this->input->post('usertype'),
						'employee_id' 	=> $this->input->post('employee_id'),
						'username' 		=> $this->input->post('username'),
						'fullname' 		=> $this->input->post('fullname'),
						'emailadd' 		=> $this->input->post('emailadd'),
						'jobtitle' 		=> $this->input->post('jobtitle'),
						'dept_code' 	=> $this->input->post('dept_code'),
						'amendby' 		=> $this->input->post('amendby'),
						'amenddate' 	=> date('Y-m-d m:i:s', strtotime($this->input->post('amenddate'))),
					]);

					echo '1';
				}
			}
		}
		else return false;
	}



	public function get_modules()
	{
		return $this->db->get(DBCARE_Model::tbl_modules)->result();
	}

	public function get_module_detail($id)
	{
		$this->db->where(['module_id' => $id]);
		return $this->db->get(DBCARE_Model::tbl_modules)->row();
	}


	public function get_module_list($aColumns)
    {
        $sTable = DBCARE_Model::tbl_modules;

        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);

        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);

        /** FILTERING **/
        if($sSearch && $sSearch != '')
        {
            for($i=0; $i<count($aColumns); $i++)
            {
                $this->db->or_like($aColumns[$i], $sSearch);
            }
        }

        /** PAGING **/
        if(isset($iDisplayStart) && $iDisplayLength != '-1')
        {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        /** ORDERING **/
        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        return $this->db->get($sTable); 
    }   
    
    public function total_length()
    {
        $sTable = DBCARE_Model::tbl_modules;
        return $this->db->count_all($sTable);
    }
	
}

?>