<div class="row" style="margin-top: -32px">

    <ul class="breadcrumb">
        <li><a href="caresurvey/dashboard" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li class="active">Module Setup</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          	<h3>
				<i class="fa fa-gears"></i> Module Setup -<small>&nbsp; Showing list of modules</small>
				<button class="btn btn-info pull-right show_modal"  data-modal="module_modal" data-action="post" data-identity="module" data-id=""><i class="fa fa-plus"></i> Add New Module</button>
			</h3>
        </div>
    </div>
</div>

<div class="row action">

	<div class="col-md-12">
		<?php if(isset($_GET['message']) && $_GET['message'] == 'post'):?>
		<div class="alert alert-success">
			<p>
				Successfully Added. <i class="fa fa-check"></i><br>
			</p>
		</div>
		<?php endif;?>
		<?php if(isset($_GET['message']) && $_GET['message'] == 'update'):?>
		<div class="alert alert-success">
			<p>
				Successfully updated. <i class="fa fa-check"></i><br>
			</p>
		</div>
		<?php endif;?>
	</div>

	<div class="col-md-12">
		<table id="module" class="table table-striped" width="100%" cellspacing="0">
			<thead>
			    <tr>
			        <th>ID</th>
			        <th>Name</th>
			        <th>Description</th>
			        <th>Type</th>
			        <th>Parent</th>
			        <th>Url/Folder/Controler or Function</th>
			        <th>Icon</th>
			        <th>Sorting</th>
			        <th width="15%" class="text-center">Action</th>
			    </tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>