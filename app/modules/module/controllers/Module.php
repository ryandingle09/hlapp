<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Kolkata');

class Module extends MX_Controller{
	

	function __construct(){

		parent::__construct();

		$this->load->model('module_model', 'module');
		$this->form_validation->CI =& $this;
	}


	public function index()
	{
		$data = [
			'' => ''
		];

		$this->layouts->view('list', $data);
	}

	public function activate()
	{
		$action 		= $this->input->post('action');
		$identity 		= $this->input->post('identity');
		$id 			= $this->input->post('id');
		$module_id 	= $this->input->post('module_id');

		$this->module->activate($action, $identity, $id, $module_id);
	}

	public function process()
	{
		$action 	= $this->uri->segment(3);
		$identity 	= $this->uri->segment(4);
		$id 	 	= $this->input->post('id');

		if($action == 'delete')
		{
			$this->module->process($action, $identity, $id);
		}
		else
		{
			if($this->form_validation->run('module') == FALSE) echo validation_errors();
			else
			{
				if($action == 'post')
				{
					#check
					$email 			= $this->module->check('email');
					$username 		= $this->module->check('username');
					$module_id 	= $this->module->check('module_id');

					if($email == true && $username == true && $module_id == true) 
						$this->module->process($action, $identity, $id);
				}
				else //update
				{
					if($this->input->post('emailadd') != $this->input->post('eold') || $this->input->post('module_id') != $this->input->post('id') || $this->input->post('username') != $this->input->post('uold'))
					{
						if($this->input->post('emailadd') != $this->input->post('eold'))
						{
							$this->module->check('email');
						}
						elseif($this->input->post('module_id') != $this->input->post('id'))
						{
							$this->module->check('module_id');
						}
						elseif($this->input->post('username') != $this->input->post('uold'))
						{
							$this->module->check('username');
						}
						else
						{
							$this->module->process($action, $identity, $id);
						}
					}
					else $this->module->process($action, $identity, $id);
				}
			}
		}
	}

	public function load_modal()
	{
		$data = [];
		$identity 	= $this->input->post('identity');
		$id 		= $this->input->post('id'); //client id
		$file_path 	= '';
		$file_name 	= '';
		
		$data = [
			'id' 					=> $id,
			'modules' 				=> $this->module->get_modules(),
			'data' 					=> $this->module->get_module_detail($id),
		];

		$this->load->view('modal/'.$this->input->post('content').'', $data);
	}

	public function get_data_table()
    {
		$aColumns = [
			"module_id",
			"parent", 
			"is_menu", 
			"module_name", 
			"module_description", 
			"parent", 
			"module_url",
			"module_icon",
			"module_sort",
			"addedby",
			"addeddate"
		];

        $sEcho = $this->input->get_post('sEcho', true);

        $rResult = $this->module->get_module_list($aColumns);

        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
        $iTotal = $this->module->total_length();

        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );

        foreach($rResult->result_array() as $aRow)
        {
    		$action = '';
    		$color  = '';
            $row = array();
            foreach($aColumns as $col)
            {
            	$type = ($aRow['is_menu'] == '1') ? 'Dropdown Menu' : 'Module';
            	$type2 = ($aRow['parent'] == '0') ? 'None' : $aRow['parent'];

               	$row[] = $aRow['module_id'];
               	$row[] = $aRow['module_name']; 
               	$row[] = $aRow['module_description'];
               	$row[] = $type; 
               	$row[] = $type2;
               	$row[] = $aRow['module_url'];
               	$row[] = $aRow['module_icon'];
               	$row[] = $aRow['module_sort'];
               	$action  =  
               	'
               		<div class="text-center">
           				<a href="javascript:;" class="btn btn-default show_modal" data-modal="module_modal" data-action="edit" data-identity="module" data-id="'.$aRow['module_id'].'" data-toggle="tooltip" data-placement="left" title="Edit"><i class="fa fa-edit"></i></a>
						<a href="javascript:;" class="btn btn-info show_modal" data-modal="module_modal" data-action="get" data-identity="module" data-id="'.$aRow['module_id'].'" data-toggle="tooltip" data-placement="bottom" title="Details"><i class="fa fa-info"></i> </a>
						<a href="javascript:;" class="btn btn-danger show_modal" data-action="delete" data-identity="module" data-id="'.$aRow['module_id'].'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
					</div>
               	';
            	$row[] = $action;	 
            }

            $output['aaData'][] = $row;
        }

        echo json_encode($output);

    }

}


?>