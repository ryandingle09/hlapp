<?php


class Account_model Extends DBCARE_Model{

	public function process($id, $file_name)
	{
		$data = [
			'fullname' 			=> $this->input->post('fullname'),
			'username' 			=> $this->input->post('username'),
			'emailadd' 			=> $this->input->post('emailadd'),
			'jobtitle' 			=> $this->input->post('jobtitle'),
		];

		if(!empty($file_name))
			$data['photo'] = $file_name;

		$this->db->where(['employee_id' => $id]);
		$this->db->update(DBCARE_Model::tbl_userinfo, $data);

		$this->db->where(['employee_id' => $id]);
		$data = $this->db->get(DBCARE_Model::tbl_userinfo);

		$row = $data->row();

		$user_data = [
			'fullname' 			=> $row->fullname,
			'username' 			=> $row->username,
			'emailadd' 			=> $row->emailadd,
			'jobtitle' 			=> $row->jobtitle,
			'dept_code' 		=> $row->dept_code,
			'photo' 			=> $row->photo,
			'amendby' 			=> $row->amendby,
			'amenddate' 		=> $row->amenddate,
		];

		$this->session->set_userdata($user_data);

		echo '1';
	}

	public function change_password($id)
	{
		$salt                 = md5('health');
        $pepper               = md5('links');
        $newpass              = $salt.sha1($this->input->post('password')).$pepper;

		$this->db->Where(['employee_id'=> $id]);
		$this->db->update(DBCARE_Model::tbl_userinfo,[
			'password' => $newpass
		]);

		echo '1';
	}

	public function oldpass_check($id)
	{
		$salt                 = md5('health');
        $pepper               = md5('links');
        $newpass              = $salt.sha1($this->input->post('old_password')).$pepper;

		$this->db->where(['employee_id' => $id]);
    	$data = $this->db->get(DBCARE_Model::tbl_userinfo);

    	$row = $data->row();

    	if($row->password == $newpass) return '0';
    	else return '1';
	}

	public function email_check()
	{
		$this->db->where(['emailadd' => $this->input->post('emailadd')]);
    	$data = $this->db->get(DBCARE_Model::tbl_userinfo);

    	if($data->num_rows() == 1) return '1';
    	else return '0';
	}

	public function username_check()
	{
		$this->db->where(['username' => $this->input->post('username')]);
    	$data = $this->db->get(DBCARE_Model::tbl_userinfo);

    	if($data->num_rows() == 1) return '1';
    	else return '0';
	}

}


?>