<?php 
echo form_open_multipart('', 'class="form-submit" data-identity="account_password" data-id="'.$account_id.'"');?>
<!--<form method="post" enctype="multipart/form-data" class="form-submit" data-identity="client" data-id="<?php echo (isset($client->client_id)) ? $client->client_id : '';?>">-->
	<div class="modal-header">
		<button type="button" class="close close_modal" data-modal="account_modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><i class="fa fa-edit"></i> Change Password</h4>
	</div>

	<div class="modal-body">
	
		<div class="row">

			<div class="col-md-12">
				<div class="alert alert-danger" style="display: none;">
					<p class="text"></p>
				</div>

				<div class="alert alert-success" style="display: none;">
					<p>Successfully saved. <i class="fa fa-check"></i></p>
				</div>
			</div>

			<div class="col-md-12">

				<div class="form-group">
					<label>Current Password</label>
					<input type="password" name="old_password" class="form-control">
				</div>

				<div class="form-group">
					<label>New Password</label>
					<input type="password" name="password" class="form-control">
				</div>

				<div class="form-group">
					<label>Retype New Password</label>
					<input type="password" name="password2" class="form-control">
				</div>

			</div>

		</div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default close_modal" data-modal="account_modal">Close</button>
		<button type="submit" class="btn btn-primary btn-save" data-loading-text="Saving....." autocomplete="off">Save  <i class="fa fa-save"></i></button>
	</div>
</form>