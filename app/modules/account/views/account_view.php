<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li class="active">Account Settings</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          	<h3>
				<i class="fa fa-cog"></i> Account Settings -<small>&nbsp; Modify your account</small>
			</h3>
        </div>
    </div>
</div>

<div class="row account">
	<div class="text-center col-md-6 col-md-offset-3">
		<ul class="list-group">
			<li class="list-group-item active">
				<a data-toggle="tooltip" data-placement="top" title="Change Password" href="javascript:;" class="btn btn-success pull-left show_modal"  data-modal="account_modal" data-identity="account_password" data-id="<?php echo $this->session->userdata('id');?>">
					<i class="fa fa-key"></i> 
				</a>
				<?php if(empty($this->session->userdata('photo'))):?>
					<h1 style="font-size: 70px"><span class="fa fa-user"></span></h1>
				<?php else: ?>
					<img width="100" style="border-radius: 20px;-webkit-border-radius: 20px;border: 1px solid #ccc" src="assets/uploads/profiles/<?php echo $this->session->userdata('photo');?>">
				<?php endif;?>
				<a data-toggle="tooltip" data-placement="top" title="Edit Account" href="javascript:;" style="margin-right: 10px" class="btn btn-info pull-right show_modal"  data-modal="account_modal" data-identity="account" data-id="<?php echo $this->session->userdata('id');?>">
					<i class="fa fa-edit"></i> 
				</a>
			</a>
			<li class="list-group-item">
				<h3><?php echo $this->session->userdata('username');?><br><small>Username</small></h3>
			</li>
			
			<li class="list-group-item">
				<h3><?php echo $this->session->userdata('fullname');?><br><small>Name</small></h3>
			</li>

			<li class="list-group-item">
				<h3><?php echo $this->session->userdata('emailadd');?><br><small>Email Address</small></h3>
			</li>

			<li class="list-group-item">
				<h3><?php echo $this->session->userdata('jobtitle');?><br><small>Role</small></h3>
			</li>

			<li class="list-group-item">
				<h3><?php echo $this->session->userdata('dept_code');?><br><small>Department</small></h3>
			</li>
		</ul>
		
	</div>
</div>