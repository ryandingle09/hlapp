<?php 
	$random = rand(000000000,999999999);
	$config = [
		'upload_path'        => 'assets/uploads/profiles',
		'allowed_types'      => 'jpeg|jpg|png|gif',
		'file_name'			 => 'IMG_'.$random,
		'max_size'           => 0,
		'max_width'          => 0,
		'max_height'         => 0,
		'remove_spaces'		 => TRUE,
		'overwrite'  		 => TRUE,
	];
?>