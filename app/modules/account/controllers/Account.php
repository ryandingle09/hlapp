<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Kolkata');

class Account Extends MX_Controller{
	

	function __construct(){
		parent::__construct();

		$this->load->model('account_model', 'account');
		$this->form_validation->CI =& $this;
	}

	public function index()
	{
		$this->layouts->view('account_view');
	}

	public function load_modal()
	{
		$data = [];
		$identity 	= $this->input->post('identity');
		$id 		= $this->input->post('id'); //client id
		$data 	= [
			'account_id' 		=> $id,
		];

		$this->load->view('modal/'.$this->input->post('content').'', $data);
	}

	public function process()
	{
		$identity 		= $this->uri->segment(4);
		$id 			= $this->uri->segment(5);


		if($identity == 'account')
		{
			$file_name 		= '';
			$image 			= $_FILES['photo']['name'];

			$this->form_validation->set_rules('fullname', 'Full Name', 'required|trim|max_length[100]');
			$this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[255]|callback_username_check');
			$this->form_validation->set_rules('emailadd', 'Email Address', 'required|trim|max_length[255]|valid_email|callback_email_check');
			$this->form_validation->set_rules('jobtitle', 'Job Title', 'required|trim|max_length[255]');

			if($this->form_validation->run() == FALSE) echo validation_errors();
			else 
			{
				if(!empty($image))
				{
		            if (!$this->upload->do_upload('photo'))
		            {
		                echo $this->upload->display_errors();
		            }
		            else
		            {
		            	$file 			= $this->upload->data();
						$file_name 		= $file['file_name'];

						if(!empty($this->session->userdata('photo')))
			        		if(file_exists('assets/uploads/profiles/'.$this->session->userdata('photo')))
			            		unlink('assets/uploads/profiles/'.$this->session->userdata('photo'));
		            }
					
					echo $this->account->process($id, $file_name);
				}
				else
				{
					echo $this->account->process($id, $file_name);
				}
			}
		}
		else
		{
			$this->form_validation->set_rules('old_password', 'Old Password', 'required|trim|max_length[255]|callback_oldpass_check');
			$this->form_validation->set_rules('password', 'New Password', 'required|trim|max_length[255]|min_length[6]');
			$this->form_validation->set_rules('password2', 'Retype New Password', 'required|trim|min_length[6]|max_length[255]|matches[password]');

			if($this->form_validation->run() == FALSE) echo validation_errors();
			else 
			{
				echo $this->account->change_password($id);
			}
		}
	}

	public function oldpass_check()
	{
		if($this->account->oldpass_check($this->session->userdata('id')) == '1')
		{
			$this->form_validation->set_message('oldpass_check', 'Current Password you entered is incorrect.');
        	return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function email_check()
    {
        if ($this->input->post('emailadd') != $this->session->userdata('emailadd'))
        {
    		if($this->account->email_check() == '1')
    		{
    			$this->form_validation->set_message('email_check', 'Email Address already taken.');
            	return FALSE;
    		}
    		else
    		{
    			return TRUE;
    		}
        }
        else
        {
            return TRUE;
        }
    }


    public function username_check()
    {
        if ($this->input->post('username') != $this->session->userdata('username'))
        {
    		if($this->account->username_check() == '1')
    		{
    			$this->form_validation->set_message('username_check', 'Username already taken.');
            	return FALSE;
    		}
    		else
    		{
    			return TRUE;
    		}
        }
        else
        {
            return TRUE;
        }
    }

}

?>