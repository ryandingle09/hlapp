<?php
/*
 * Health Links Team
 * 
 * Author: Michael Angelo D. Magat
 * Randomizer module for Care Hospital System
 * 
 */
 class Savedata_model extends CI_Model
 {
     public function save_er($doctor_code,
                            $section_code,
                            $visit_date,
                            $patient_phone,
                            $company_id,
                            $medical_no,
                            $insurcode,
                            $nationality,
                            $department_code,
                            $department_desc,
                            $patient_name,
                            $patient_age,
                            $gendr,
                            $uploadno)
     {
         $insert['code']        = uniqid(mt_rand(0000,9999));
         $insert['random_no']   = mt_rand(12345,99999);
         $insert['upload_date'] = date('Y-m-d');
         $insert['upload_no']   = $uploadno; // count on upload no. tbl
         $insert['amendby']     = $this->session->userdata('username');
         $insert['amenddate']   = date('Y-m-d');
         $insert['hosp']        = $company_id;
         $insert['patname']     = $patient_name;
         $insert['mrno']        = $medical_no;
         $insert['drcode']      = $doctor_code;
         $insert['unit']        = $section_code;
         $insert['dept']        = $department_code;
         $insert['mobile']      = $patient_phone;
         $insert['visitdate']   = $visit_date;
         $insert['age']         = $patient_age;
         $insert['gender']      = $gendr;
         $insert['nationality'] = $nationality;
         $insert['fin_cat']     = $insurcode;
         
         $this->db->insert('er_randomized',$insert);
     }

    public function save_op($doctor_code,
                            $section_code,
                            $visit_date,
                            $patient_phone,
                            $company_id,
                            $medical_no,
                            $insurcode,
                            $nationality,
                            $department_code,
                            $department_desc,
                            $patient_name,
                            $patient_age,
                            $gendr,
                            $uploadno)
     {
         $insert['code']        = uniqid(mt_rand(0000,9999));
         $insert['random_no']   = mt_rand(12345,99999);
         $insert['upload_date'] = date('Y-m-d');
         $insert['upload_no']   = $uploadno; // count on upload no. tbl
         $insert['amendby']     = $this->session->userdata('username');
         $insert['amenddate']   = date('Y-m-d');
         $insert['hosp']        = $company_id;
         $insert['patname']     = $patient_name;
         $insert['mrno']        = $medical_no;
         $insert['drcode']      = $doctor_code;
         $insert['unit']        = $section_code;
         $insert['dept']        = $department_code;
         $insert['mobile']      = $patient_phone;
         $insert['visitdate']   = $visit_date;
         $insert['age']         = $patient_age;
         $insert['gender']      = $gendr;
         $insert['nationality'] = $nationality;
         $insert['fin_cat']     = $insurcode;
         
         $this->db->insert('op_randomized',$insert);
     }

    public function save_as($doctor_code,
                            $section_code,
                            $visit_date,
                            $patient_phone,
                            $company_id,
                            $medical_no,
                            $insurcode,
                            $nationality,
                            $department_code,
                            $department_desc,
                            $patient_name,
                            $patient_age,
                            $gendr,
                            $uploadno)
     {
         $insert['code']        = uniqid(mt_rand(0000,9999));
         $insert['random_no']   = mt_rand(12345,99999);
         $insert['upload_date'] = date('Y-m-d');
         $insert['upload_no']   = $uploadno; // count on upload no. tbl
         $insert['amendby']     = $this->session->userdata('username');
         $insert['amenddate']   = date('Y-m-d');
         $insert['hosp']        = $company_id;
         $insert['patname']     = $patient_name;
         $insert['mrno']        = $medical_no;
         $insert['drcode']      = $doctor_code;
         $insert['unit']        = $section_code;
         $insert['dept']        = $department_code;
         $insert['mobile']      = $patient_phone;
         $insert['visitdate']   = $visit_date;
         $insert['age']         = $patient_age;
         $insert['gender']      = $gendr;
         $insert['nationality'] = $nationality;
         $insert['fin_cat']     = $insurcode; // ill ask jade what's this
         
         $this->db->insert('as_randomized',$insert);
     }

     public function save_ip($staff_code,
                            $section_CODE,
                            $vdate,
                            $tel,
                            $COMPANY_ID,
                            $patient_code,
                            $nationality,
                            $department_code,
                            $department_desc,
                            $patient_name,
                            $age,
                            $gender,
                            $fin_cat_code,
                            $fin_cat_desc,
                            $uploadno)
     {
         $insert['code']        = uniqid(mt_rand(0000,9999));
         $insert['random_no']   = mt_rand(12345,99999);
         $insert['upload_date'] = date('Y-m-d');
         $insert['upload_no']   = $uploadno; // count on upload no. tbl
         $insert['amendby']     = $this->session->userdata('username');
         $insert['amenddate']   = date('Y-m-d');
         $insert['hosp']        = $COMPANY_ID;
         $insert['patname']     = $patient_name;
         $insert['mrno']        = $patient_code;
         $insert['drcode']      = $staff_code;
         $insert['unit']        = $section_CODE;
         $insert['dept']        = $department_code;
         $insert['mobile']      = $tel;
         $insert['visitdate']   = $vdate;
         $insert['age']         = $age;
         $insert['gender']      = $gender;
         $insert['nationality'] = $nationality;
         $insert['fin_cat']     = $fin_cat_desc; 
         
         $this->db->insert('ip_randomized',$insert);
     }
	 
	 public function op_updatesenddate($today,$hosp)
     {
        $sqlStr = "UPDATE op_randomizer SET senddate = '$today' WHERE upload_date = '$today' AND hosp = $hosp";
		$this->db->query($sqlStr);
     }

	 public function er_updatesenddate($today,$hosp)
     {
        $sqlStr = "UPDATE er_randomizer SET senddate = '$today' WHERE upload_date = '$today' AND hosp = $hosp";
		$this->db->query($sqlStr);
     }
 }
