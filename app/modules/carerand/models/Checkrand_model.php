<?php
class Checkrand_model extends CI_Model
{
    // er
    public function checkuploader($uploaddt)
    {
        $sqlStr = "SELECT * FROM er_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function er_uploadno1($res)
    {
        $update['upload_no']        = $res+1;
        $this->db->where('uploaded_dt', date('Y-m-d'));
        $this->db->update('er_uploadno', $update);
    }

    public function er_uploadno2()
    {
        $insert['upload_no']    = 1;
        $insert['uploaded_dt']  = date('Y-m-d');
        $this->db->insert('er_uploadno', $insert);
    }

    public function er_updatesenddate($today)
    {
        $update['senddate'] = $today;
        $this->db->where('upload_date', $today);
        $this->db->update('er_randomized', $update);
    }
    
   // op
   public function checkuploadop($uploaddt)
    {
        $sqlStr = "SELECT * FROM op_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function op_uploadno1($res)
    {
        $update['upload_no']        = $res+1;
        $this->db->where('uploaded_dt', date('Y-m-d'));
        $this->db->update('op_uploadno', $update);
    }

    public function op_uploadno2()
    {
        $insert['upload_no']    = 1;
        $insert['uploaded_dt']  = date('Y-m-d');
        $this->db->insert('op_uploadno', $insert);
    }

    public function op_updatesenddate($today)
    {
        $update['senddate'] = $today;
        $this->db->where('upload_date', $today);
        $this->db->update('op_randomized', $update);
    }
    
    // as 
    public function checkuploadas($uploaddt)
    {
        $sqlStr = "SELECT * FROM as_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function as_uploadno1($res)
    {
        $update['upload_no']        = $res+1;
        $this->db->where('uploaded_dt', date('Y-m-d'));
        $this->db->update('as_uploadno', $update);
    }

    public function as_uploadno2()
    {
        $insert['upload_no']    = 1;
        $insert['uploaded_dt']  = date('Y-m-d');
        $this->db->insert('as_uploadno', $insert);
    }

    public function as_updatesenddate($today)
    {
        $update['senddate'] = $today;
        $this->db->where('upload_date', $today);
        $this->db->update('as_randomized', $update);
    }
    
    // ip
    public function checkuploadip($uploaddt)
    {
        $sqlStr = "SELECT * FROM ip_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function ip_uploadno1($res)
    {
        $update['upload_no']        = $res+1;
        $this->db->where('uploaded_dt', date('Y-m-d'));
        $this->db->update('ip_uploadno', $update);
    }

    public function ip_uploadno2()
    {
        $insert['upload_no']    = 1;
        $insert['uploaded_dt']  = date('Y-m-d');
        $this->db->insert('ip_uploadno', $insert);
    }

    public function ip_updatesenddate($today)
    {
        $update['senddate'] = $today;
        $this->db->where('upload_date', $today);
        $this->db->update('ip_randomized', $update);
    }
    
    public function check_dept($code)
    {
        $sqlStr = "SELECT COUNT(*) FROM dept WHERE code = ?";
        $res = $this->db->query($sqlStr,array($code));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function check_doctor($drcode)
    {
        $sqlStr = "SELECT COUNT(*) FROM doctors WHERE drcode = ?";
        $res = $this->db->query($sqlStr,array($drcode));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function check_doctor_clinic($drcode)
    {
        $sqlStr = "SELECT ccode, dcode FROM doctors WHERE drcode = ?";
        $res = $this->db->query($sqlStr,array($drcode));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function check_ward($code)
    {
        $sqlStr = "SELECT COUNT(*) FROM wards WHERE code = ?";
        $res = $this->db->query($sqlStr,array($code));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
	
	public function check_clinic($code)
    {
        $sqlStr = "SELECT COUNT(*) FROM clinics WHERE code = ?";
        $res = $this->db->query($sqlStr,array($code));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
	
	public function getHospitals()
	{
		$sqlStr = "SELECT * FROM hosp";
        $res = $this->db->query($sqlStr);
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function nationality($nalid)
	{
		$sqlStr = "SELECT * FROM nationalities WHERE code = ?";
        $res = $this->db->query($sqlStr, $nalid);
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
    
    public function check_sms_er($uploaddt)
    {
        $sqlStr = "SELECT * FROM er_randomized WHERE upload_date = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function check_sms_op($uploaddt,$hosp)
    {
        $sqlStr = "SELECT * FROM op_randomized WHERE upload_date = ? AND hosp = ?";
        $res = $this->db->query($sqlStr,array($uploaddt,$hosp));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
}
