<?php
/*
 * Health Links Team
 * 
 * Author: Michael Angelo D. Magat
 * Randomizer module for Care Hospital System
 * Count upload via email by wards, clinic etc.
 * 
 */

class Count_model extends CI_Model
{
    // ip
    public function getwards($company_id, $survey_type)
    {
        $sqlStr = "SELECT * FROM wards WHERE hosp = ? AND survey_type = ?";
        $res = $this->db->query($sqlStr,array($company_id, $survey_type));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function checkuploadip($uploaddt)
    {
        $sqlStr = "SELECT upload_no FROM ip_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function countuploadip($code, $company_id, $uploaddt)
    {
        $sqlStr = "SELECT COUNT(*) AS total_count FROM ip_randomized WHERE unit = ? AND hosp = ? AND upload_date = ?";
        $res = $this->db->query($sqlStr,array($code, $company_id, $uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    // end
    
    //AS
    public function checkuploadas($uploaddt)
    {
        $sqlStr = "SELECT upload_no FROM as_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function countuploadas($code, $company_id, $uploaddt)
    {
        $sqlStr = "SELECT COUNT(*) AS total_count FROM as_randomized WHERE unit = ? AND hosp = ? AND upload_date = ?";
        $res = $this->db->query($sqlStr,array($code, $company_id, $uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    //end
    
    
    //OP
    public function getclinics($company_id)
    {
        $sqlStr = "SELECT * FROM clinics WHERE hosp = ?";
        $res = $this->db->query($sqlStr,array($company_id));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function checkuploadop($uploaddt)
    {
        $sqlStr = "SELECT upload_no FROM op_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function countuploadop($code, $company_id, $uploaddt)
    {
        $sqlStr = "SELECT COUNT(*) AS total_count FROM op_randomized WHERE unit = ? AND hosp = ? AND upload_date = ?";
        $res = $this->db->query($sqlStr,array($code, $company_id, $uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    // end
    
    //ER
    public function checkuploader($uploaddt)
    {
        $sqlStr = "SELECT upload_no FROM er_uploadno WHERE uploaded_dt = ?";
        $res = $this->db->query($sqlStr,array($uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    
    public function countuploader($company_id, $uploaddt)
    {
        $sqlStr = "SELECT COUNT(*) AS total_count FROM er_randomized WHERE hosp = ? AND upload_date = ?";
        $res = $this->db->query($sqlStr,array($company_id, $uploaddt));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
}
    