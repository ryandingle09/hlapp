
<div class="alert alert-success success_msg" role="alert">
  <span class="glyphicon glyphicon-ok"></span>
  Your file has been processed.
</div>
<br/>
<a href="carerand" class="btn btn-default" id="button-panel-or">
  <span class="glyphicon glyphicon-home"></span>
  Back to Mainpage
</a>
