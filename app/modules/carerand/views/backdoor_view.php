<a class="btn btn-default" href="carerand" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
  
<br/><br/><br/>

<?=$err_msg?>
 
  <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="active"><a href="#er" aria-controls="home" role="tab" data-toggle="tab">ER</a></li>
<li role="presentation"><a href="#op" aria-controls="profile" role="tab" data-toggle="tab">OP</a></li>
<li role="presentation"><a href="#as" aria-controls="messages" role="tab" data-toggle="tab">AS</a></li>
<li role="presentation"><a href="#ip" aria-controls="messages" role="tab" data-toggle="tab">IP</a></li>
</ul>

  <!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="er">
        <br/>
            
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">ER</div>
          <div class="panel-body" id="body-panel-or">
            <form id="form-submit" role="form" action="carerand/uploadbackdoor/er" enctype="multipart/form-data" method="post">
                
                <div class="form-group">
                    <input type="file" name="file" id="file"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default upload-file" id="button-panel-or">
                        <span class="glyphicon glyphicon-open"></span> Upload
                    </button>
                </div>
            </form>
            
            <br/>
          </div>
        </div>
        
    </div>
    <div role="tabpanel" class="tab-pane" id="op">
        
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">OP</div>
          <div class="panel-body" id="body-panel-or">
            <form id="form-submit" role="form" action="carerand/uploadbackdoor/op" enctype="multipart/form-data" method="post">
                
                <div class="form-group">
                    <input type="file" name="file" id="file"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default upload-file" id="button-panel-or">
                        <span class="glyphicon glyphicon-open"></span> Upload
                    </button>
                </div>
            </form>
            
            <br/>
          </div>
        </div>
        
    </div>
    <div role="tabpanel" class="tab-pane" id="as">
        
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">AS</div>
          <div class="panel-body" id="body-panel-or">
            <form id="form-submit" role="form" action="carerand/uploadbackdoor/as" enctype="multipart/form-data" method="post">
                
                <div class="form-group">
                    <input type="file" name="file" id="file"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default upload-file" id="button-panel-or">
                        <span class="glyphicon glyphicon-open"></span> Upload
                    </button>
                </div>
            </form>
            
            <br/>
          </div>
        </div>
        
    </div>
    <div role="tabpanel" class="tab-pane" id="ip">
        
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">IP</div>
          <div class="panel-body" id="body-panel-or">
            
            <form id="form-submit" role="form" action="carerand/uploadbackdoor/ip" enctype="multipart/form-data" method="post">
                
                <div class="form-group">
                    <input type="file" name="file" id="file"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default upload-file" id="button-panel-or">
                        <span class="glyphicon glyphicon-open"></span> Upload
                    </button>
                </div>
            </form>
            
            <br/>
          </div>
        </div>
        
    </div>
</div>
