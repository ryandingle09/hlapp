<div class="row" style="margin-top: -15px">
    <ul class="breadcrumb">
        <li><a href="carerand" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li class="active"><?=$hospName?></li>
    </ul>
</div>

<?php if($this->uri->segment(3) == 1): ?>
	<img src="<?php echo PATH_IMG ?>careimg.jpg" width="226" style="float: right"/>
<?php else: ?>
	<img src="<?php echo PATH_IMG ?>rnh.jpg" width="200" style="float: right"/>
<?php endif; ?>

<br/><br/><br/>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#er" aria-controls="home" role="tab" data-toggle="tab">ER</a></li>
    <li role="presentation"><a href="#op" aria-controls="profile" role="tab" data-toggle="tab">OP</a></li>
    <li role="presentation"><a href="#as" aria-controls="messages" role="tab" data-toggle="tab">AS</a></li>
    <li role="presentation"><a href="#ip" aria-controls="messages" role="tab" data-toggle="tab">IP</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="er">
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">ER</div>
          <div class="panel-body" id="body-panel-or">
            <?//=$err_msg?>
            
            <form id="form-submit-er" role="form" action="carerand/upload/<?=$this->uri->segment(3)?>/er" method="post">
              <div class="form-group" style="text-align: left">
                <label>From:</label>
                <input style="width: 200px" type="text" name="from" class="form-control" id="dtpicker-from-er" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <label>To:</label>
                <input style="width: 200px" type="text" name="to" class="form-control" id="dtpicker-to-er" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <button type="submit" class="btn btn-default upload-file-er" id="button-panel-or">
                    <span class="glyphicon glyphicon-open"></span> Process
                  </button>
                  <span id="msg-wait-er"></span>
              </div>
            </form>
            <br/>
          </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="op">
        
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">OP</div>
          <div class="panel-body" id="body-panel-or">
            <?//=$err_msg?>
            
            <form id="form-submit-op" role="form" action="carerand/upload/<?=$this->uri->segment(3)?>/op" method="post">
              <div class="form-group" style="text-align: left">
                <label>From:</label>
                <input style="width: 200px" type="text" name="from" class="form-control" id="dtpicker-from-op" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <label>To:</label>
                <input style="width: 200px" type="text" name="to" class="form-control" id="dtpicker-to-op" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <button type="submit" class="btn btn-default upload-file-op" id="button-panel-or">
                    <span class="glyphicon glyphicon-open"></span> Process
                  </button>
                  <span id="msg-wait-op"></span>
              </div>
            </form>
            <br/>
          </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="as">
        
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">AS</div>
          <div class="panel-body" id="body-panel-or">
            <?//=$err_msg?>
            
            <form id="form-submit-as" role="form" action="carerand/upload/<?=$this->uri->segment(3)?>/as" method="post">
              <div class="form-group" style="text-align: left">
                <label>From:</label>
                <input style="width: 200px" type="text" name="from" class="form-control" id="dtpicker-from-as" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <label>To:</label>
                <input style="width: 200px" type="text" name="to" class="form-control" id="dtpicker-to-as" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <button type="submit" class="btn btn-default upload-file-as" id="button-panel-or">
                    <span class="glyphicon glyphicon-open"></span> Process
                  </button>
                  <span id="msg-wait-as"></span>
              </div>
            </form>
            <br/>
          </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="ip">
        
        <br/>
        <div class="panel panel-default panel-upload">
          <div class="panel-heading" id="heading-panel-or">IP</div>
          <div class="panel-body" id="body-panel-or">
            <?//=$err_msg?>
            
            <form id="form-submit-ip" role="form" action="carerand/upload/<?=$this->uri->segment(3)?>/ip" method="post">
              <div class="form-group" style="text-align: left">
                <label>From:</label>
                <input style="width: 200px" type="text" name="from" class="form-control" id="dtpicker-from-ip" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                <label>To:</label>
                <input style="width: 200px" type="text" name="to" class="form-control" id="dtpicker-to-ip" value="<?=date('m/d/Y')?>"/>
              </div>
              <div class="form-group" style="text-align: left">
                  <button type="submit" class="btn btn-default upload-file-ip" id="button-panel-or">
                    <span class="glyphicon glyphicon-open"></span> Process
                  </button>
                  <span id="msg-wait-ip"></span>
              </div>
            </form>
            <br/>
          </div>
        </div>
    </div>
</div>
  