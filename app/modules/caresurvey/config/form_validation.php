<?php

$config = array(

        'section' => array(

                array(
                        'field' => 'survey_type',
                        'label' => 'Survey Type',
                        'rules' => 'required|trim|max_length[50]'
                ),
                array(
                        'field' => 'section_code',
                        'label' => 'Section Id/Code',
                        'rules' => 'required|trim|max_length[50]'
                ),
                array(
                        'field' => 'has_comment_box',
                        'label' => 'Comment Box',
                        'rules' => 'required|trim|max_length[1]'
                ),
        ),

        'choice' => array(

                array(
                        'field' => 'title',
                        'label' => 'Choice Title',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'choice[]',
                        'label' => 'Choice Text',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'choice_trans[]',
                        'label' => 'Choice Text Translation',
                        'rules' => 'required'
                ),

        ),

        'question' => array(

                array(
                        'field' => 'survey_type',
                        'label' => 'Survey Type',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'section_code',
                        'label' => 'Section Id/Code',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'question_text',
                        'label' => 'Question Text',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'question_trans',
                        'label' => 'Question Text Translation',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'choice',
                        'label' => 'Answer Choice',
                        'rules' => 'required|trim|is_numeric'
                ),

        ),
);


?>