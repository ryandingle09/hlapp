<?php

class Count_model extends CI_Model
{
	// ip
	public function getTotalTargetsIP($unit)
	{
		$sqlStr = "SELECT target FROM targets WHERE dept = ? AND survey_type = 'IP' AND (NOW() >= start_date AND NOW() <= end_date)";
        $res = $this->db->query($sqlStr, $unit);
        /*$data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data[0];*/
        return $res->row();
	}
	
	public function getRemainingTargetIP($uploaddt, $unit)
	{
		$sqlStr = "SELECT COUNT(*) AS total_remaining FROM ip_randomized WHERE upload_date = ? AND unit = ? AND senddate IS NULL";
        $res = $this->db->query($sqlStr,array($uploaddt, $unit));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data[0];
	}
	
	public function getTotalCompleteIP($uploaddt, $unit)
	{
		$sqlStr = "SELECT
						COUNT(*) total_comp
					FROM
						ip_survey a,
						targets b
					WHERE
						a.status = 'submitted' AND 
						a.unit = $unit AND
						b.survey_type = 'IP' AND
						(NOW() >= b.start_date AND NOW() <= b.end_date) AND
						(b.dept = a.unit) AND
						(date(a.addeddate) >= b.start_date AND date(a.addeddate) <= end_date)";
        $res = $this->db->query($sqlStr);
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data[0];
	}
	
	
	// AS
	public function getTotalTargetsAS($unit)
	{
		$sqlStr = "SELECT target FROM targets WHERE dept = ? AND survey_type = 'AS' AND (NOW() >= start_date AND NOW() <= end_date)";
        $res = $this->db->query($sqlStr, $unit);
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getRemainingTargetAS($uploaddt, $unit)
	{
		$sqlStr = "SELECT COUNT(*) AS total_remaining FROM as_randomized WHERE upload_date = ? AND unit = ? AND senddate IS NULL";
        $res = $this->db->query($sqlStr,array($uploaddt, $unit));
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data[0];
	}
	
	public function getTotalCompleteAS($uploaddt, $unit)
	{
		$sqlStr = "SELECT
						COUNT(*) total_comp
					FROM
						as_survey a,
						targets b
					WHERE
						a.status = 'submitted' AND 
						a.unit = $unit AND
						b.survey_type = 'AS' AND
						(NOW() >= b.start_date AND NOW() <= b.end_date) AND
						(b.dept = a.unit) AND
						(date(a.addeddate) >= b.start_date AND date(a.addeddate) <= end_date)";
        $res = $this->db->query($sqlStr);
        $data   = array();
        if ($res && $res->num_rows() > 0 ) {
            foreach ($res->result() as $row) {
                $data[] = $row;
            }
        }
        return $data[0];
	}
}