<?php

class Caresetup_model extends DBCARE_Model
{

	public function ajax_form_insert($identity)
	{
		switch ($identity) {
			case 'section':
				if($this->db->insert(DBCARE_Model::tbl_survey_sections, [
					'survey_type' 				=> $this->input->post('survey_type'),
					'section_code' 				=> $this->input->post('section_code'),
					'section_text' 				=> $this->input->post('section_text'),
					'section_text_translation' 	=> $this->input->post('section_text_translation'),
					'added_by'					=> $this->session->userdata('username'),
					'added_date'				=> date('Y-m-d m:i:s')
				])) echo '1';
				break;

			case 'question':
				if($this->db->insert(DBCARE_Model::tbl_survey_questions, [
					'survey_type' 				=> $this->input->post('survey_type'),
					'section_code' 				=> $this->input->post('section_code'),
					'question' 					=> $this->input->post('question_text'),
					'question_translation' 		=> $this->input->post('question_trans'),
					'choices' 					=> $this->input->post('choice'),
					'is_parent' 				=> $this->input->post('is_parent'),
					'added_by'					=> $this->session->userdata('username'),
					'added_date'				=> date('Y-m-d m:i:s')
				])) echo '1';
				break;

			case 'choice':
				$choices = [];

				foreach ($this->input->post('choice') as $key => $row)
				{
					$choices[] = ['choice' => $row, 'choice_trans' => $this->input->post('choice_trans')[$key]];
				}

				/**parsing
				foreach ($choices as $key => $val)
				{
					echo $val['choice'].'<br>';
					echo $val['choice_trans'].'<br>';
				}
				/**echo json_encode($choices);**/


				if($this->db->insert(DBCARE_Model::tbl_survey_question_choices, [
					'title' 					=> $this->input->post('title'),
					'choice' 					=> json_encode($choices, JSON_UNESCAPED_UNICODE),
					'added_by'					=> $this->session->userdata('username'),
					'added_date'				=> date('Y-m-d m:i:s')
				])) echo '1';
				break;

			default:
				return false;
				break;
		}
	}

	public function get_choices()
	{
		return $this->db->get(DBCARE_Model::tbl_survey_question_choices);
	}

	public function get_dropdown($identity, $value)
	{
		switch ($identity) {
			case 'section':
				$this->db->select('section_code, section_text')->where(['survey_type' => $value]);
				$this->db->from(DBCARE_Model::tbl_survey_sections);
				return $this->db->get()->result_array();
				break;

			default:
				return false;
				break;
		}
	}

}

?>