<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h3 class="modal-title"><?php echo $title;?> - Patient List</h3>
</div>
<table id="patients" class="table table-striped" width="100%" cellspacing="0">
<thead>
    <tr>
        <th>Patient</th>
        <th>Mobile No.</th>
        <th>Language</th>
        <th class="text-center">Action</th>
    </tr>
</thead>
<tbody>
</tbody>
</table>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>