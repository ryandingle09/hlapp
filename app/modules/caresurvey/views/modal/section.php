<form class="form-submit" data-identity="section">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><i class="fa fa-plus"></i> Add Section</h4>
	</div>

	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
					<p>
						<i class="fa fa-info"></i> Note :
						If your survey has a blank name section kindly leave empty on both SECTION TEXT and TRANSLATION field.
					</p>
				</div>

				<div class="alert alert-danger" style="display: none;">
					<p class="text"></p>
				</div>

				<div class="alert alert-success" style="display: none;">
					<p>Successfully saved. <i class="fa fa-check"></i></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>SURVEY TYPE</label>
					<select class="form-control" name="survey_type">
						<option value="">select survey type</option>
						<option value="IP">Inpatient Survey</option>
						<option value="AS">Ambulatory Survey</option>
						<option value="OP">Outpatient Survey</option>
						<option value="ER">ER Satisfaction Survey</option>
					</select>
				</div>
			</div>

			<div class="col-md-12">&nbsp;</div>

			<div class="col-md-12">
				<div class="form-group">
					<label>SECTION ID/CODE</label>
					<input type="text" class="form-control" name="section_code">
				</div>

				<div class="form-group">
					<label>SECTION TEXT</label>
					<textarea name="section_text" class="form-control" rows="2"></textarea>
				</div>

				<div class="form-group">
					<label>SECTION TEXT TRANSLATION</label>
					<textarea name="section_text_translation" class="form-control" rows="2"></textarea>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Has Comment Box ?</label>
					<select class="form-control" name="has_comment_box">
						<option value="">Select</option>
						<option value="1">Enable</option>
						<option selected="" value="0">Disable</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary btn-save" data-loading-text="Saving....." autocomplete="off">Save  <i class="fa fa-save"></i></button>
	</div>
</form>