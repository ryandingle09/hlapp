<form class="form-submit" data-identity="choice">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><i class="fa fa-plus"></i> Add Choices to your questions</h4>
	</div>

	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger" style="display: none;">
					<p class="text"></p>
				</div>

				<div class="alert alert-success" style="display: none;">
					<p>Successfully saved. <i class="fa fa-check"></i></p>
				</div>
			</div>
			<div class="col-md-12">

				<div class="form-group">
					<label>CHOICE TITLE</label>
					<input type="text" class="form-control" name="title">
				</div>
				
			</div>

			<div class="col-md-12">

				<div class="row">

					<div class="col-md-12">
						<div class="form-group">
							<label>&nbsp;</label>
							<button type="button" class="btn btn-info btn-add-choice"><i class="fa fa-plus"></i> Add Choice</button>
						</div>
					</div>

				</div>

			</div>

			<div class="col-md-12 choice-fields"></div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary btn-save" data-loading-text="Saving....." autocomplete="off">Save  <i class="fa fa-save"></i></button>
	</div>
</form>