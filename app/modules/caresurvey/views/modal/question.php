<form class="form-submit" data-identity="question">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><i class="fa fa-plus"></i> Add Question</h4>
	</div>

	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger" style="display: none;">
					<p class="text"></p>
				</div>

				<div class="alert alert-success" style="display: none;">
					<p>Successfully saved. <i class="fa fa-check"></i></p>
				</div>
			</div>
			<div class="col-md-6">

				<div class="form-group">
					<label>SURVEY TYPE</label>
					<br>
					<select class="form-control get-dropdown" data-identity="section" data-target=".section_dropdown" name="survey_type">
						<option value="">select survey type</option>
						<option value="IP">Inpatient Survey</option>
						<option value="AS">Ambulatory Survey</option>
						<option value="OP">Outpatient Survey</option>
						<option value="ER">ER Satisfaction Survey</option>
					</select>
				</div>

			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>SECTION</label>
					<br>
					<select class="form-control section_dropdown" name="section_code">
						<option value="">select survey type first</option>
					</select>
				</div>
			</div>

			<div class="col-md-12">

				<div class="form-group">
					<label>QUESTION</label>
					<textarea name="question_text" class="form-control" rows="2"></textarea>
				</div>

				<div class="form-group">
					<label>QUESTION TRANSLATION</label>
					<textarea name="question_trans" class="form-control" rows="2"></textarea>
				</div>

			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label>ANSWER CHOICE</label>
					<br>
					<select class="form-control" name="choice">
						<option value="">select choice</option>
						<?php 
							if($choices->num_rows() != 0)
							{
								foreach($choices->result() as $row):
						?>
								<option value="<?php echo $row->id;?>"><?php echo $row->title;?></option>
						<?php 
								endforeach;
							}
						?>
					</select>
				</div>
			</div>
			<div class="col-md-4 pull-right">
				<div class="col-md-12">&nbsp;</div>
				<div class="checkbox">
			    <label>
			      <input type="checkbox" name="is_parent"> IS PARENT ? check if yes
			    </label>
			  </div>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary btn-save" data-loading-text="Saving....." autocomplete="off">Save  <i class="fa fa-save"></i></button>
	</div>
</form>