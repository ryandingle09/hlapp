<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="caresurvey/">Care</a></li>
        <li class="active">Survey Setup</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          <h3><i class="fa fa-wrench"></i> Survey Setup<small></small></h3>
        </div>
    </div>

</div>

<div class="row deactivate-script">
    <div class="col-md-12">&nbsp;</div>

    <div class="col-md-4 col-md-offset-2">
    <a href="javascript:;" class="show-modal" data-modal="setup_modal" data-mdsize="" data-content="section">
        <div class="border-double">
            <div class="row">
                <div class="col-md-8 pull-left">
                    <h4 class="bar-title">Add Section</h4>
                </div>
                <div class="col-md-4 pull-right">
                    <h4 class="tile-icon"><i class="fa fa-list"></i></h4>
                </div>
                <div class="col-md-12" style="color: #999">
                    Add questions to specific survey.
                </div>
            </div>
        </div>
    </a>
    </div>


    <div class="col-md-4">
        <a href="javascript:;" class="show-modal" data-modal="setup_modal" data-mdsize="" data-content="question">
            <div class="border-double">
                <div class="row">
                    <div class="col-md-8 pull-left">
                        <h4 class="bar-title">Add Questions</h4>
                    </div>
                    <div class="col-md-4 pull-right">
                        <h4 class="tile-icon"><i class="fa fa-question-circle-o"></i></h4>
                    </div>
                    <div class="col-md-12" style="color: #999">
                        Add question to specific survey and  section
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-4 col-md-offset-2">
        <a href="javascript:;" class="show-modal" data-modal="setup_modal" data-mdsize="" data-content="choice">
            <div class="border-double">
                <div class="row">
                    <div class="col-md-8 pull-left">
                        <h4 class="bar-title">Add Choices</h4>
                    </div>
                    <div class="col-md-4 pull-right">
                        <h4 class="tile-icon"><i class="fa fa-sliders"></i></h4>
                    </div>
                    <div class="col-md-12" style="color: #999">
                        Edit questions or section.
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-4">
        <a href="javascript:;" class="show-modal" data-modal="setup_modal" data-mdsize="lg" data-content="modify">
            <div class="border-double">
                <div class="row">
                    <div class="col-md-8 pull-left">
                        <h4 class="bar-title">Modify Contents</h4>
                    </div>
                    <div class="col-md-4 pull-right">
                        <h4 class="tile-icon"><i class="fa fa-edit"></i></h4>
                    </div>
                    <div class="col-md-12" style="color: #999">
                        Edit questions, section or choices.
                    </div>
                </div>
            </div>
        </a>
    </div>

</div>
