<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="caresurvey" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="caresurvey/">Care</a></li>
        <li class="active">Survey</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          <h3><i class="fa fa-file-text-o"></i> Surveys<small>&nbsp; Showing types of surveys to perform</small></h3>
        </div>
    </div>

</div>

<form action="caresurvey/">
    <div class="col-md-4 col-md-offset-4">
        <label>Patient's MRN / Mobile No.</label>
        <div class="form-group">
            <input type="text" class="form-control search-mrn" placeholder="Search for past surveys">
        </div>
    </div>
</form>

<div class="col-md-12">&nbsp;</div>

<div class="row">

    <div class="col-md-12 border-double-2 white search-content" style="display: none">
        <h3>Inpatient</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>MR No.</th>
                    <th>Patient</th>
                    <th>Guardian</th>
                    <th>Mobile</th>
                    <th>Unit</th>
                    <th>Discharge</th>
                    <th>Initial Reponse</th>
                    <th>Reschedule</th>
                    <th>Refuse</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="ip-result">
                
            </tbody>
        </table>

        <h3>Ambulatory</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>MR No.</th>
                    <th>Patient</th>
                    <th>Guardian</th>
                    <th>Mobile</th>
                    <th>Unit</th>
                    <th>Discharge</th>
                    <th>Initial Reponse</th>
                    <th>Reschedule</th>
                    <th>Refuse</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="as-result">
                
            </tbody>
        </table>
    </div>

    <div class="col-md-12">&nbsp;</div>

    <div class="col-md-4 col-md-offset-2">
    <a href="caresurvey/iplist/<?=$this->uri->segment(3)?>">
        <div class="border-double">
            <div class="row">
                <div class="col-md-8 pull-left">
                    <h4 class="bar-title">Inpatient Survey</h4>
                </div>
                <div class="col-md-4 pull-right">
                    <h4 class="tile-icon"><i class="fa fa-wheelchair"></i></h4>
                </div>
                <div class="col-md-12" style="color: #999">
                    Operations Department
                    <br>
                    Surveys Manual
                    <br>
                    Inpatient Survey (IP)
                </div>
            </div>
        </div>
    </a>
    </div>


    <div class="col-md-4">
        <a href="caresurvey/aslist/<?=$this->uri->segment(3)?>">
            <div class="border-double">
                <div class="row">
                    <div class="col-md-8 pull-left">
                        <h4 class="bar-title">Ambulatory Survey</h4>
                    </div>
                    <div class="col-md-4 pull-right">
                        <h4 class="tile-icon"><i class="fa fa-clock-o"></i></h4>
                    </div>
                    <div class="col-md-12" style="color: #999">
                        Operations Department
                        <br>
                        Surveys Manual
                        <br>
                        Ambulatory Surgery Survey (AS)
                    </div>
                </div>
            </div>
        </a>
    </div>


</div>