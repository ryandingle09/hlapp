<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="caresurvey/byhospital/<?=$this->uri->segment(3)?>" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="caresurvey/">Care</a></li>
        <li><a href="caresurvey/byhospital/<?=$this->uri->segment(3)?>">Survey</a></li>
        <li class="active">Survey units</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          <h3><i class="fa fa-list"></i> Survey Units<small>&nbsp; Showing list of survey hospital units</small></h3>
        </div>
    </div>

</div>

<div class="row">

	<div class="col-md-3">
		<label>SELECT UPLOAD DATE</label>
		<select id="upload-date" class="upload-date form-control" data-section="no" data-type="<?php echo $this->uri->segment(2);?>" data-code="<?php echo $this->uri->segment(3);?>">
			<option value="">SELECT OR SEARCH</option>
			<?php
				foreach ($upload_date as $row) {
					echo '<option value="'.$row->upload_date.'">'.$row->upload_date.'</option>';
				}
			?>
		</select>
	</div>
	
</div>

<div class="row">

	<div class="col-md-12">&nbsp;</div>

	<div class="col-md-12" id="unit-list">

	</div>

</div>

<div class="modal fade patient_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  	<div class="modal-dialog modal-lg" role="document" style="width: 80%">
    	<div class="modal-content padding-10">
		  	<h2 class="text-center">Loading...</h2>
    	</div>
  	</div>
</div>