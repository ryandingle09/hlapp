<div class="row text-center" style="padding-top: 50px;padding-bottom: 100px">

	<div class="col-md-12 col-md-8 col-md-offset-2" style="color:#00abc5">
		<div class="col-md-12">
			<h2 dir="rtl">استبيان جراحة اليوم الواحد / الإجراءات التداخلية</h2>
		</div>
		<div class="col-md-12">
			<h1 style="">Ambulatory Surgery Survey</h1>
		</div>

		<div class="col-md-12">
			<h3 dir="rtl">شكرا</h3>
			<h3>THANK YOU.</h3>
		</div>

		<div class="col-md-12">
			<h4>Ambulatory Surgery Survey completed.</h4>
			<h4 dir="rtl">تم اكمال الإستبيان</h4>
		</div>

		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">&nbsp;</div>

		<div class="col-md-12">
			<a href="caresurvey/" class="btn btn-info btn-lg">BACK TO SURVEY LIST</a>
		</div>
	</div>
</div>