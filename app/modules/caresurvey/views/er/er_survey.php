<div class="col-md-6 col-md-offset-3 data survey top-info"><!-- do not remove this elem --></div>

<div class="page-header" style="border: 0px">
	<h2>	
		<h3 class="text-center" style="color:gray;">
			<span class="ar">استبيان رضا مرضى الطوارىء</span>
			<div>ER Satisfaction Survey</div>
		</h3>
	</h2>

	<table class="table top-info" style="background: #eff">
		<tbody>
			<tr>
				<td width="40%" style="font-size:12px;line-height:20px;">
					<div style="font-size:16px;font-weight:bold;color:#00abc5;text-align:left;">
						Dr. <?php echo (isset($doctor->drname)) ? $doctor->drname : 'Abdul Usmani';?>
					</div>
					<div style="text-align:left;font-weight:bold;">
						<?php echo (isset($doctor->grade)) ? $doctor->grade : 'Consultant Internal Medicine';?>
					</div>
				</td>
				<td width="20%">
					<center>	
					<?php if(isset($doctor->imgpath)):?>
						<?php if(is_null($doctor->imgpath)):?>
						<h1 style="font-size: 150px"><i class="fa fa-user-md"></i></h1>
						<?php else: ?>
						<img src="<?php echo $doctor->imgpath;?>" height="100%">
						<?php endif;?>
					</center>
					<?php endif;?>
				</td>
				<td width="40%" class="ar" style="font-size:12px;line-height:20px;">
					<div style="font-size:16px;font-weight:bold;color:#00abc5;text-align:right;">
						د.
						<?php echo (isset($doctor->drname_ar)) ? $doctor->drname_ar : 'Abdul Usmani';?>
					</div>
					<div style="text-align:right;font-weight:bold;">
						<?php echo (isset($doctor->grade_ar)) ? $doctor->grade_ar : 'Consultant Internal Medicine Ar';?>
					</div>									
				</td>
			</tr>
		</tbody>
	</table>

	<div class="col-md-12 alert-info top-info" style="color: #343434;padding-top: 10px;font-size: 12px">
		<table width="100%">
			<tbody>
				<tr>
					<td>
						<b>Welcome to our Satisfaction Survey.</b>
						<br>
						<br>
						Thank you for taking the time to be part in this important survey to help us improve and better serve you.
						<br>
						<br>
						This survey takes 5-10 minutes. Be assured that your identity will remain confidential unless you give permission to share it with the hospital.
						<br>
						<br>
						We at CARE continuously strive to make our patients, family members and guests feel welcome and at home ... We exist to humanize the healthcare industry.
						<br>
						<br>
						<b>
							Dr. <?php echo (isset($doctor->drname)) ? $doctor->drname : 'Abdul Usmani';?><br>
							CEO
						</b>
						<br>
						<br>
					</td>
					<td>
						<p dir="rtl">
							<b>
								استبيان رضا المرضى .. نرتقي بخدمتكم
							</b>
							<br>
							<br>
							نشكر مشاركتكم في هذا الاستبيان، لمساعدتنا على وضع أولوياتنا للتحسين وتطوير خدماتنا.
							<br>
							<br>
							يستغرق هذا الاستبيان 5-10 دقائق. سنحافظ على سرية هويتك إلا في حالة موافقتكم على مشاركتها مع المستشفى.
							<br>
							<br>
							نسعى في "رعاية" للترحيب بمرضانا وأسرهم وضيوفهم وكأنهم في منزلهم ... نعمل لتقديم .. رعاية صحية .. أكثر إنسانية.
							<br>
							<br>
							<b>
								د. <?php echo (isset($doctor->drname_ar)) ? $doctor->drname_ar : 'Abdul Usmani';?>
								<br>
								الرئيس التنفيذي
							</b>
							<br>
							<br>
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<b>
						<u>SURVEY INSTRUCTIONS:</u>
						</b>
						<br>
						Please rate the Outpatient Services you received. Select the response that best describes your experience.
						<br>
						<br>
						If a question does not apply to you, please skip to the next question. Space is provided for you to provide any further feedback you may want to share with us.
						<br>
						<br>
						If you can’t complete the entire survey at once, you may come back to it later. Your previous responses will be saved automatically and you will be able to continue where you left off.
						<br><br>
						When you have finished, please click the “Finish” button.
						<br>
						<br>
						<b>This survey is to evaluate your visit to us on <?php echo (isset($survey->visitdate)) ? date('F j, Y', strtotime($survey->visitdate)) : date('m/d/Y') ;?>. All answers should relate to this visit only.</b>
					</td>
					<td>
						<p dir="rtl">
							<b>
							<u>تعليمات الاستبيان:</u></b><br>
							الرجاء تصنيف الخدمات التي حصلت عليها من عياداتنا. اختر الإجابة التي تنطبق بشكل أفضل على تجربتك.
							<br>
							<br>
							إذا كان هناك أي سؤال لا ينطبق على حالتك، يرجى تخطي هذا السؤال والانتقال إلى السؤال التالي. نوفر لك المساحة لتقديم أي ملاحظات قد ترغب في مشاركته معنا.
							<br>
							<br>
							إذا لم تستطع استكمال الاستبيان مرة واحدة، يمكنك أن تعود إليه لاحقاً. سوف يتم حفظ إجابتك تلقائياً وسيمكنك الاستكمال من حيث توقفت. في أية نقطة في هذا الاستبيان.
							<br>
							<br>
							عند الانتهاء، الرجاء الضغط على زر "إرسال".
							<br>
							<br>
							<b>هذا الاستبيان لتقييم زيارتك لنا في تاريخ <?php echo (isset($doctor->drname)) ? date('F j, Y', strtotime($survey->visitdate)) : date('m/d/Y') ;?>، جميع أجوبتكم ستكون خاصة بهذه الزيارة فقط.</b>
						</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="col-md-12 text-center top-info" style="margin-bottom:  50px;margin-top: 50px">
	<button type="button" class="btn btn-info btn-lg proceed" style="width: 300px;">
		<span dir="rtl">انتقل إلى الاستبيان</span><br>
		Proceed to Survey
	</button>
</div>

<div class="row survey data" style="display: none">

	<div class="col-md-12">
		<div id="rootwizard" class="tabbable tabs-left">
		    <div class="navbar">
			    <div class="navbar-inner">
				    <div class="container">
				    	<small>
					    <ul style="margin-left: -30px;list-style: none">
						    <li><a class="hide" href="#tab1" data-toggle="tab">Step 1</a></li>
						    <li><a class="hide" href="#tab2" data-toggle="tab">Step 2</a></li>
						    <li><a class="hide" href="#tab3" data-toggle="tab">Step 3</a></li>
						    <li><a class="hide" href="#tab4" data-toggle="tab">Step 4</a></li>
						    <li><a class="hide" href="#tab5" data-toggle="tab">Step 5</a></li>
						    <li><a class="hide" href="#tab6" data-toggle="tab">Step 6</a></li>
						    <li><a class="hide" href="#tab7" data-toggle="tab">Step 7</a></li>
						    <li><a class="hide" href="#tab8" data-toggle="tab">Step 8</a></li>
						    <li><a class="hide" href="#tab9" data-toggle="tab">Step 9</a></li>
						    <li><a class="hide" href="#tab10" data-toggle="tab">Step 10</a></li>
						    <li><a class="hide" href="#tab11" data-toggle="tab">Step 11</a></li>
					    </ul>
					    </small>
				    </div>
			    </div>
			</div>

			<!-- KEYS -->
			<input type="hidden" name="type" id="type" value="<?php echo $this->uri->segment(3);?>">
			<input type="hidden" name="hosp" id="hosp" value="<?php echo $this->uri->segment(4);?>">
			<input type="hidden" name="code" id="code" value="<?php echo $this->uri->segment(5);?>">
			<input type="hidden" name="random" id="random" value="<?php echo $this->uri->segment(6);?>">
			<input type="hidden" name="whos_filling_up" id="whos_filling_up" value="<?php echo ($this->session->userdata('username')) ?  : $survey->patname;?>">
            <input type="hidden" name="lastposition" value="<?php echo (isset($survey->lastposition)) ? $survey->lastposition : '';?>">
            <?php 
            	if(isset($survey->lastposition)){
            		if($survey->lastposition != '' || $survey->lastposition = '0'){
            ?>
            		<script type="text/javascript">
            			$(document).ready(function(){
            				$('.top-info').hide();
            				$('.survey').show();
            			});
            		</script>
            <?php }} ;?>
			<!-- END -->

			<div class="tab-content margin-bottom-20">

			    <div class="tab-pane" id="tab1">
			    	
			    	<div class="row">

			    		<table width="100%">

			    			<tr style="display: none;">
			    				<td class="padding-20">
			    					1. Time of day you arrived
			    				</td>
			    				<td dir="rtl">
			    					1- الوقت الذي وصلت فيه
			    				</td>
			    			</tr>
			    			<tr class="border-bottom" style="display: none;">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
									
		    						<form class="form-inline">
			    						<?php 
			    							$visitdate = $survey->visitdate;

					    					$dd_day = substr($visitdate, 2, 2);
											$dd_mon = substr($visitdate, 0, 2);
											$dd_year = substr($visitdate, 4, 4);
											$dd_time = substr($visitdate, 8, 4);

											$hh = substr($visitdate, 8, 2);
											$mm = substr($visitdate, 10, 2);

											/* COMPUTE q1 [time visit] : start */
											$q1 = "";
											if(($hh >= 3) && ($hh < 7)){
											    $q1 = "6";
											}
											if((($hh >= 21) && ($hh <= 23)) || (($hh >= 0) && ($hh < 3))){
											    $q1 = "5";

											}
											if(($hh >= 19) && ($hh < 23)){
											    $q1 = "4";
											}
											if(($hh >= 15) && ($hh < 19)){
											    $q1 = "3";
											}
											if(($hh >= 11) && ($hh < 15)){
											    $q1 = "2";
											}
											if(($hh >= 7) && ($hh < 11)){
											    $q1 = "1";
											}

											echo 'q1 = '.$q1.' <br>';
											/* COMPUTE q1 [time visit] : end */
					    					$text 		= '';
					    					
						    			?>
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
		    								<input class="to-labelauty synch-icon" value="<?php echo $q1;?>" type="radio" name="1" data-labelauty='<?php echo $q1;?>' />
		    							</div>
		    						</form>
		    					</td>
			    			</tr>
			    			<tr>
			    				<td class="padding-20">
			    					2. Who is filling out this survey?
			    				</td>
			    				<td dir="rtl">
			    					2 - من الذي يقوم بالإجابة على هذا الاستبيان؟
			    				</td>
			    			</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
			    						<?php 
					    					$text = '';
					    					for($count = 1;$count <=5;$count++){ 

					    					switch ($count) {
					    						case '1':
					    							$text = '<br><p dir="rtl">شخص آخر</p><br>Others<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">صديق</p><br>Friend<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">فرد من العائلة</p><br>Family<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">أحد الوالدين</p><br>Parent<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">المريض</p><br>Patient<br><br>';
					    							break;
					    					}
					    				?>
						    			<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q2 == $count) ? 'checked' : '';?> class="q1 to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="2" data-labelauty='<?php echo $text;?>' />
			    						</div>
		    							<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab2">

			    	<div class="row">

			    		<table width="100%">

			    			<tr class="border-bottom">
			    				<td>
			    					<h4 class="head-section">ARRIVAL</h4>
			    				</td>
			    				<td>
			    					<h4 dir="rtl" class="head-section">الوصول</h4>
			    				</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			1. Waiting time before staff noticed your arrival
					    		</td>
					    		<td dir="rtl">
					    			1- مدة الانتظار قبل انتباه الموظفين لوصولك
					    		</td>
				    		</tr>
				    		
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
				    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
				    							break;
				    						case '2':
				    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
				    							break;
				    						case '3':
				    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
				    							break;
				    						case '4':
				    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
				    							break;
				    						default:
				    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
				    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q3 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="3" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			2. Helpfulness of the person who first asked you about your condition
					    		</td>
				    			<td dir="rtl">
				    				2- مساعدة أول شخص قام بسؤالك عن حالتك
				    			</td>
				    		</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
				    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
				    							break;
				    						case '2':
				    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
				    							break;
				    						case '3':
				    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
				    							break;
				    						case '4':
				    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
				    							break;
				    						default:
				    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
				    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q4 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="4" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
				    			<td class="padding-20">
					    			3. Comfort of the waiting area
					    		</td>
					    		<td dir="rtl">
					    			3- الراحة في منطقة الانتظار
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
				    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
				    							break;
				    						case '2':
				    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
				    							break;
				    						case '3':
				    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
				    							break;
				    						case '4':
				    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
				    							break;
				    						default:
				    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
				    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q5 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="5" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			4. Waiting time before you were brought to the treatment area
					    		</td>
					    		<td dir="rtl">
			    					4- مدة الانتظار قبل دخولك لمنطقة العلاج 
			    				</td>
			    			</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
				    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
				    							break;
				    						case '2':
				    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
				    							break;
				    						case '3':
				    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
				    							break;
				    						case '4':
				    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
				    							break;
				    						default:
				    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
				    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q6 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="6" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			5. Waiting time in the treatment area, before you were seen by a doctor
					    		</td>
					    		<td dir="rtl">
					    			5- مدة الانتظار في منطقة العلاج قبل أن يفحصك الطبيب
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
				    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
				    							break;
				    						case '2':
				    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
				    							break;
				    						case '3':
				    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
				    							break;
				    						case '4':
				    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
				    							break;
				    						default:
				    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
				    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q7 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="7" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>

			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab3">

			    	<div class="row">

			    		<table width="100%">

				    		<tr>
			    				<td class="border-bottom">
			    					<h4 class="head-section">NURSING</h4>
			    				</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">التمريض</h4>
					    		</td>
					    	</tr>

			    			<tr>
				    			<td class="padding-20">
					    			1. Courtesy of the nurses 
					    		</td>
					    		<td dir="rtl">
					    			1- اهتمام فريق التمريض   
					    		</td>
				    		</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q8 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="8" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
		    							</form>
		    					</td>
			    			</tr>

				    		<tr>
				    			<td class="padding-20">
					    			2. Degree to which the nurses took the time to listen to you
					    		</td>
				    			<td dir="rtl">
				    				2- إلى أي درجة حاول فريق التمريض الإصغاء إليك
				    			</td>
				    		</tr>
							<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q9 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="9" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

				    		<tr>
			    				<td class="padding-20">
					    			3. Nurses' attention to your needs
					    		</td>
					    		<td dir="rtl">
					    			3- مراعاة فريق التمريض لاحتياجاتك
					    		</td>
					    	</tr>
							<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q10 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="10" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
				    			<td class="padding-20">
					    			4. Nurses' concern to keep you informed about your treatment
					    		</td>
					    		<td dir="rtl">
					    			4- حرص فريق التمريض بإطلاعك على تفاصيل علاجك
					    		</td>
					    	</tr>
							<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>
			    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
		    							<input <?php echo ($survey->q11 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="11" data-labelauty='<?php echo $text;?>' />
		    						</div>

			    					<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
				    			<td class="padding-20">
					    			5. Nurses' concern for your privacy
					    		</td>
					    		<td dir="rtl">
					    			5- مراعاة فريق التمريض لخصوصيتك   
					    		</td>
					    	</tr>
							<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q12 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="12" data-labelauty='<?php echo $text;?>' />
			    						</div>
				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab4">

			    	<div class="row">

			    		<table width="100%">

				    		<tr>
			    				<td class="border-bottom">
			    					<h4 class="head-section">DOCTORS</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">الأطباء</h4>
					    		</td>
					    	</tr>
				    		<tr>
				    			<td class="padding-20">
					    			1. Courtesy of the doctor
					    		</td>
					    		<td dir="rtl">
					    			1- اهتمام الطبيب
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q13 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="13" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
				    				2. Doctor’s concern for your comfort while treating you
					    		</td>
					    		<td dir="rtl">
					    			2- حرص الطبيب على راحتك خلال علاجك
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q14 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="14" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>
			    			<tr>
			    				<td class="padding-20">
					    			3. Degree to which the doctor took the time to
									listen to you
					    		</td>
					    		<td dir="rtl">
					    			3- مدى إصغاء الطبيب إليك
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

			    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
		    							<input <?php echo ($survey->q15 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="15" data-labelauty='<?php echo $text;?>' />
		    						</div>

			    					<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
					    		<td class="padding-20">
					    			4. Doctor's concern to keep you informed about
					    		</td>
					    		<td dir="rtl">
					    			4- حرص الطبيب بإطلاعك على تفاصيل علاجك
					    		</td>
					    	</tr>
		    				<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q16 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="16" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab5">

			    	<div class="row">

			    		<table width="100%">

					    	<tr class="border-bottom">
				    			<td>
					    			<h4 class="head-section">TESTS<br>
					    			(Please answer only questions that apply to you)
					    			</h4>
					    		</td>
				    			<td dir="rtl">
				    				<h4 class="head-section">الفحوصات<br>
					    			(الرجاء الإجابة على الأسئلة التي تنطبق عليك فقط)
					    			</h4>
				    			</td>
				    		</tr>
				    		<tr>
				    			<td class="padding-20">
				    				Lab
					    		</td>
					    		<td dir="rtl">
					    			المختبر
					    		</td>
					    	</tr>

			    			<tr>
					    		<td class="padding-20">
					    			1. Did you receive laboratory services?
					    		</td>
					    		<td dir="rtl">
					    			1- هل طلب لك الطبيب أي تحاليل طبية؟
					    		</td>
					    	</tr>
					    	<tr>
					    		<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
					    					<input <?php echo ($survey->q17 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="tests" value="no" type="radio" name="17" 
						    				data-labelauty="
						    				<br>
						    				<span dir='rtl'>لا (انتقل إلى القسم التالي)</span>
						    				<br>No (Go To next section)<br>
						    				" />
					    				</div>
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
					    					<input <?php echo ($survey->q17 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="tests" value="yes" type="radio" name="17" 
				    						data-labelauty="
				    						<br>
						    				<span dir='rtl'>نعم</span>
						    				<br>Yes<br>
						    				" />
						    			</div>
					    			</form>
					    		</td>
					    	</tr>

			    			<tr class="tests" style="display: none">
			    				<td class="padding-20">
					    			2. Explanations of what would happen during Lab tests
					    		</td>
					    		<td dir="rtl">
					    			2-  الشرح المقدم عن ماذا سيحدث أثناء إجراء تحاليل المختبر
					    		</td>
					    	</tr>
			    			<tr class="border-bottom tests" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>
			    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
		    							<input <?php echo ($survey->q18 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="18" data-labelauty='<?php echo $text;?>' />
		    						</div>
			    					<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr class="tests" style="display: none">
				    			<td class="padding-20">
					    			3. How well your blood was taken (quickly, little pain, etc)
					    		</td>
					    		<td dir="rtl">
					    			3- مدى إتقان عملية سحب الدم (بسرعة، وبأقل ألم)
					    		</td>
					    	</tr>
				    		<tr class="border-bottom tests" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q19 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="19" data-labelauty='<?php echo $text;?>' />
			    						</div>
				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr class="tests" style="display: none">
			    				<td class="padding-20">
					    			4. Extent to which you were informed about your test results
					    		</td>
					    		<td dir="rtl">
					    			4- مدى إطلاعك على نتائج التحاليل التي قمت بها
					    		</td>
					    	</tr>
				    		<tr class="border-bottom tests" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q20 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="20" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab6">

			    	<div class="row">

			    		<table width="100%">

				    		<tr class="border-bottom">
			    				<td>
					    			<h4 class="head-section">RADIOLOGY<br>
									(X-ray, ultrasound, CAT scan, MRI)</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">
					    			الأشعة<br>
									(الأشعة السينية، الموجات فوق الصوتية، الأشعة المقطعية، الرنين المغناطيسي)
					    			</h4>
					    		</td>
					    	</tr>
			    			<tr>
			    				<td class="padding-20">
					    			1. Did you receive radiology services?
					    		</td>
					    		<td dir="rtl">
					    			1- هل تلقيت خدمات من قسم الأشعة؟
					    		</td>
					    	</tr>
					    	<tr>
					    		<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
					    					<input <?php echo ($survey->q21 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="radiology" value="no" type="radio" name="21" 
						    				data-labelauty="
						    				<br>
						    				<span dir='rtl'>لا (انتقل إلى القسم التالي)</span>
						    				<br>No (Go To next section)<br>
						    				" />
					    				</div>
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
					    					<input <?php echo ($survey->q21 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="radiology" value="yes" type="radio" name="21" 
				    						data-labelauty="
				    						<br>
						    				<span dir='rtl'>نعم</span>
						    				<br>Yes<br>
						    				" />
						    			</div>
					    			</form>
					    		</td>
					    	</tr>

				    		<tr class="radiology" style="display: none">
				    			<td class="padding-20">
					    			2. How well did the radiology staff introduce themselves to you
					    		</td>
					    		<td dir="rtl">
					    			2- مدى تعريف موظفي الأشعة بأنفسهم لك
					    		</td>
					    	</tr>
				    		<tr class="border-bottom radiology" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>
				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q22 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="22" data-labelauty='<?php echo $text;?>' />
			    						</div>
				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr class="radiology" style="display: none">
			    				<td class="padding-20">
					    			3. Waiting time for radiology test
					    		</td>
					    		<td dir="rtl">
					    			3- فترة الانتظار لإجراء الأشعة   
					    		</td>
					    	</tr>
			    			<tr class="border-bottom radiology" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q23 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="23" data-labelauty='<?php echo $text;?>' />
			    						</div>
				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr class="radiology" style="display: none">
				    			<td class="padding-20">
					    			4. Explanation of radiology procedure
					    		</td>
					    		<td dir="rtl">
					    			4- الشرح المقدم عن إجراء ت الأشعة
					    		</td>
					    	</tr>
			    			<tr class="border-bottom radiology" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q24 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="24" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr class="radiology" style="display: none">
					    		<td class="padding-20">
					    			5. Skill and efficiency of the radiology technologist
					    		</td>
					    		<td dir="rtl">
					    			5- مهارة وإتقان الشخص الذي قام بإجراء الأشعة  
					    		</td>
					    	</tr>
			    			<tr class="border-bottom radiology" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q25 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="25" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab7">

			    	<div class="row">

			    		<table width="100%">

					    	<tr class="border-bottom">
				    			<td>
					    			<h4 class="head-section">Family or Friends
					    			<br>(If you came alone, please skip this section)</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">العائلة أو الأصدقاء
					    			<br>(إذا قدمت للطوارئ لوحدك، الرجاء تخطي هذا القسم.)</h4>
					    		</td>
					    	</tr>

			    			<tr>
				    			<td class="padding-20">
					    			1. Courtesy with which family or friends were treated
					    		</td>
					    		<td dir="rtl">
					    			1- الطريقة التي عومل بهما العائلة والأصدقاء
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q26 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="26" data-labelauty='<?php echo $text;?>' />
			    						</div>
				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
					    		<td class="padding-20">
					    			2. Staff concern to keep family or friends informed about your status<br> during your course of treatment
					    		</td>
					    		<td dir="rtl">
					    			2-  حرص الفريق الطبي لإبقاء العائلة والأصدقاء على علم <br>بمراحل علاجك
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q27 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="27" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			3. Staff concern to let a family member or friend be with you<br> while you were being treated
					    		</td>
					    		<td dir="rtl">
					    			3- مراعاة الفريق الطبي بالسماح لأحد أفراد العائلة أو <br>صديق بمرافقتك خلال تلقيك للعلاج
			    				</td>
			    			</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q28 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="28" data-labelauty='<?php echo $text;?>' />
			    						</div>
				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>	

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab8">

			    	<div class="row">

			    		<table width="100%">

				    		<tr class="border-bottom">
			    				<td>
			    					<h4 class="head-section">SERVICES
			    					<br>
					    			(Please answer only questions that apply to you)</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">الخدمات
					    			<br>(الرجاء الإجابة على الأسئلة التي تنطبق عليك فقط)</h4>
					    		</td>
					    	</tr>
							<tr>
					    		<td class="padding-20">
					    			Insurance office
					    		</td>
					    		<td dir="rtl">
					    			مكتب التأمين
					    		</td>
					    	</tr>

			    			<tr>
			    				<td class="padding-20">
					    			1. Did any of your treatments require an approval from your<br> insurance company during this visit?
					    		</td>
					    		<td dir="rtl">
					    			1- هل تطلب أي من مراحل  علاجك موافقة شركة <br>التأمين خلال هذه الزيارة؟
					    		</td>
					    	</tr>
					    	<tr>
					    		<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
					    					<input <?php echo ($survey->q29 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="services" value="no" type="radio" name="29" 
						    				data-labelauty="
						    				<br>
						    				<span dir='rtl'>لا (انتقل إلى القسم التالي)</span>
						    				<br>No (Go To next section)<br>
						    				" />
					    				</div>
					    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
					    					<input <?php echo ($survey->q29 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="services" value="yes" type="radio" name="29" 
				    						data-labelauty="
				    						<br>
						    				<span dir='rtl'>نعم</span>
						    				<br>Yes<br>
						    				" />
						    			</div>
					    			</form>
					    		</td>
					    	</tr>

			    			<tr class="services" style="display: none">
			    				<td class="padding-20">
					    			2. Time for processing your approval
					    		</td>
					    		<td dir="rtl">
					    			2- الوقت اللازم الحصول على موافقة التأمين
					    		</td>
					    	</tr>
			    			<tr class="border-bottom services" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q30 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="30" data-labelauty='<?php echo $text;?>' />
			    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr class="services" style="display: none">
			    				<td class="padding-20">
					    			3. How well were you informed regarding the outcome <br>of your insurance request
					    		</td>
					    		<td dir="rtl">
					    			3- مدى إطلاعك على نتيجة طلب الموافقة من شركة<br> التأمين
					    		</td>
					    	</tr>
			    			<tr class="border-bottom services" style="display: none">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

			    				<?php 
			    					$text = '';
			    					for($count = 1;$count <=5;$count++){ 

			    					switch ($count) {
			    						case '1':
				    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
				    							break;
				    						case '2':
				    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
				    							break;
				    						case '3':
				    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
				    							break;
				    						case '4':
				    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
				    							break;
				    						default:
				    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
				    							break;
			    					}
			    				?>

			    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
			    							<input <?php echo ($survey->q31 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="31" data-labelauty='<?php echo $text;?>' />
			    						</div>

			    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab9">

			    	<div class="row">

			    		<table width="100%">

				    		<tr class="border-bottom">
			    				<td>
					    			<h4 class="head-section">FACILITIES</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">المرافق</h4>
					    		</td>
					    	</tr>

			    			<tr>
			    				<td class="padding-20">
					    			1. Parking
					    		</td>
					    		<td dir="rtl">
					    			1- المواقف
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q32 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="32" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			2. Room & bed linens cleanliness
					    		</td>
					    		<td dir="rtl">
					    			2- نظافة الغرفة وأغطية الأسرة
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q33 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="33" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			3. Bathroom cleanliness
					    		</td>
					    		<td dir="rtl">
					    			3- نظافة الحمام
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q34 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="34" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>
			    		
			    		</table>

			    	</div>

			    </div>

			    <div class="tab-pane" id="tab10">

			    	<div class="row">

			    		<table width="100%">

				    		<tr class="border-bottom">
			    				<td>
					    			<h4 class="head-section">PERSONAL ISSUES</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">أمور شخصية</h4>
					    		</td>
					    	</tr>

			    			<tr>
			    				<td class="padding-20">
					    			1. How well you were kept informed about delays
					    		</td>
					    		<td dir="rtl">
					    			1- مدى إخبارك بأي تأخير في الإجراءات
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q35 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="35" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
					    		<td class="padding-20">
					    			2. Degree to which staff cared about you as a person
					    		</td>
					    		<td dir="rtl">
					    			2- كشخص، إلى أي درجة اهتم بك الموظفون
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q36 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="36" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			3. How well your pain was controlled
					    		</td>
					    		<td dir="rtl">
					    			3- ما مدى السيطرة على شعورك بالألم
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q37 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="37" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			4. Information you were given about caring for <br>yourself at home (e.g., taking medications, <br>getting follow-up medical care)
					    		</td>
					    		<td dir="rtl">
					    			4- المعلومات التي قدمت لك حول كيفية الاعتناء بنفسك في <br>المنزل (على سبيل المثال، أخذ الدواء، <br>الحصول على عناية طبية لاحقة)
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q38 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="38" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			5. How well staff explained their roles in your care
					    		</td>
					    		<td dir="rtl">
					    			5- ما مدى شرح الموظفين لدورهم الوظيفي في تقديم الرعاية لك
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q39 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="39" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    		</table>

			    	</div>

			    </div>
			    
			    <div class="tab-pane" id="tab11">

			    	<div class="row">

			    		<table width="100%">

				    		<tr class="border-bottom">
			    				<td>
					    			<h4 class="head-section">OVERALL ASSESSMENT</h4>
					    		</td>
					    		<td dir="rtl">
					    			<h4 class="head-section">التقييم العام</h4>
					    		</td>
					    	</tr>
			    			<tr>
			    				<td class="padding-20">
					    			1. Overall rating of care received during your visit
					    		</td>
					    		<td dir="rtl">
					    			1- التقييم العام للرعاية التي تلقيتها خلال زيارتك
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q40 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="40" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			2. Likelihood of your recommending our Emergency Department to others
					    		</td>
					    		<td dir="rtl">
					    			2- احتمالية أن توصي بقسم الطوارئ للآخرين
					    		</td>
					    	</tr>
			    			<tr class="border-bottom">
		    					<td colspan="2" class="text-center" style="padding-bottom: 10px;padding-right: 0;padding-top: 0;padding-right: 0;padding-left: 0">
		    						<form class="form-inline">

				    				<?php 
				    					$text = '';
				    					for($count = 1;$count <=5;$count++){ 

				    					switch ($count) {
				    						case '1':
					    							$text = '<br><p dir="rtl">سيء</p><br>Very Poor<br><br>';
					    							break;
					    						case '2':
					    							$text = '<br><p dir="rtl">ضعيف</p><br>Poor<br><br>';
					    							break;
					    						case '3':
					    							$text = '<br><p dir="rtl">مقبول</p><br>Fair<br><br>';
					    							break;
					    						case '4':
					    							$text = '<br><p dir="rtl">جيد</p><br>Good<br><br>';
					    							break;
					    						default:
					    							$text = '<br><p dir="rtl">جيد جداً</p><br>Very Good<br><br>';
					    							break;
				    					}
				    				?>

				    					<div class="form-group" style="padding: 0px;padding-bottom: 5px">
				    							<input <?php echo ($survey->q41 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="41" data-labelauty='<?php echo $text;?>' />
				    						</div>

				    				<?php } ?>
	    							</form>
		    					</td>
			    			</tr>

			    			<tr>
				    			<td class="padding-20">
					    			4. Was there any team member that provided exceptional care / service? Who?
					    		</td>
				    			<td dir="rtl" class="text-right">
				    				4- هل قدّم لك أحد الموظفين رعاية مميزة تذكر؟ هل تذكر اسمهـ(ا)؟
				    			</td>
				    		</tr>
				    		<tr class="border-bottom text-center">
				    			<td colspan="2">
				    				<center>
					    				<form class="form-inline">
						    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
							    				<input <?php echo ($survey->hl_qtm == 2) ? 'checked' : '';?> class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="no" name="hl_qtm" 
						    				 	data-labelauty="<br><br>
							    				<p dir='rtl'>لا</p>
							    				<br>
							    				No<br><br><br>
						    				 	" />
						    				</div>
						    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
						    					
							    				<input <?php echo ($survey->hl_qtm == 1) ? 'checked' : '';?> class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="yes" name="hl_qtm"
							    				 data-labelauty="
							    				 	<br>
								    				<p dir='rtl'>نعم (اذكر الإسم والقسم)</p>
								    				<br>
								    				Yes (Mention name & division)<br><br>
							    				 "
							    				/>

						    				</div>
					    				</form>
					    				<form class="form-inline" id="ND" style="display: none;padding-bottom: 10px">
						    				<div class="form-group">
						    					Name * <input type="text" value="<?php echo $survey->hl_atm;?>" class="form-control text-center" placeholder="Name" name="hl_atm"> * اسم
						    				</div>
						    				<div>&nbsp;</div>
						    				<div class="form-group">
						    					Division * <input type="text" value="<?php echo $survey->hl_atmd;?>" class="form-control text-center" placeholder="Division" name="hl_atmd"> * القسم
						    				</div>
						    			</form>
						    		</center>
				    			</td>
				    		</tr>

					    	<tr class="border-bottom">
			    				<td class="padding-20">
			    					Comments<br>
									(describe good or bad experience):
			    				</td>
			    				<td dir="rtl" class="text-right">
			    					تعليقات<br>
									(صف التجارب جيدة أو سيئة):
			    				</td>
			    				
			    			</tr>

			    			<tr class="border-bottom">
			    				<td colspan="2">
			    					<textarea rows="5" class="form-control onshowcomment" data-show="extra" name="comments"><?php echo $survey->comments;?></textarea>
			    				</td>
			    			</tr>

			    			<tr>
			    				<td class="padding-20">
					    			<b>I agree on sharing my contact information along the comments with the hospital </b>
					    		</td>
			    				<td dir="rtl" class="text-right">
				    				<b><p dir="rtl">أوافق على إرفاق اسمي ورقم هاتفي مع التعليقات التي سيتم إرسالها إلى المستشفى </p></b>
				    			</td>
				    		</tr>

				    		<tr class="border-bottom text-center">
				    			<td class="padding-10" colspan="2">
				    				<center>
					    				<form class="form-inline" style="margin-top: -50px">
						    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
						    					<input <?php echo ($survey->hl_disclaimer == 1) ? 'checked' : '';?> class="hl_disclaimer" value="yes" type="checkbox" name="hl_disclaimer" style="transform: scale(1.5);" />
						    				</div>
						    			</form>
						    		</center>
				    			</td>
				    		</tr>

			    			<tr class="extra" style="display: none;">
			    				<td class="padding-20">
					    			Have you had any issues with the care you received and would like a hospital official to contact you?
					    		</td>
			    				<td dir="rtl" class="text-right">
				    			هل واجهتك أي مشاكل في العناية الطبية التي تلقيتها وترغب بأن يقوم مسؤول في المستشفى بالتواصل بك؟
				    			</td>
			    			</tr>
			    			<tr class="border-bottom text-center extra" style="display: none;">
				    			<td class="padding-10" colspan="2">

				    				<center>
					    				<form class="form-inline">
						    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
							    				<input <?php echo ($survey->hl_contact == 2) ? 'checked' : '';?> class="to-labelauty synch-icon ACTION" data-show="#CD" value="no" type="radio" name="hl_contact" 
					    						data-labelauty="
					    						<br>
							    				<p dir='rtl'>لا</p>
							    				<br>
							    				No<br><br>
							    				" />
						    				</div>
						    				<div class="form-group" style="padding: 0px;padding-bottom: 5px">
						    					
							    				<input <?php echo ($survey->hl_contact == 1) ? 'checked' : '';?> class="to-labelauty synch-icon ACTION" data-show="#CD" value="yes" type="radio" name="hl_contact" 
					    						data-labelauty="
					    						<br>
							    				<p dir='rtl'>نعم (طريقة التواصل)</p>
							    				<br>
							    				Yes (contact details)<br><br>
							    				" />

						    				</div>
					    				</form>
					    				<form class="form-inline" id="CD" style="display: none;padding-bottom: 10px">
						    				<div class="form-group">
						    					Name * </label>
						    					 <input type="text" value="<?php echo $survey->hl_contact_name;?>" class="form-control text-center" placeholder="Name" name="hl_contact_name"> * اسم
						    				</div>
						    				<div>&nbsp;</div>
						    				<div class="form-group">
						    					Phone No. * &nbsp;<input type="text" value="<?php echo $survey->hl_contact_no;?>" class="form-control text-center" placeholder="Mobile No." name="hl_contact_no"> * رقم الهاتف
					    					</div>
						    			</form>
						    		</center>

				    			</td>
				    		</tr>

			    		</table>

			    	</div>

			    </div>

			    <div class="row" style="margin-top: 20px">
					<div id="bar" class="progress">
			    		<div class="progress-bar active progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
			    			</td>
			    	</div>
			    </div>

			    <ul class="pager wizard">
				    <!--<li class="previous first" style="display:none;"><a href="javascript:;">First</a></li>-->
				    <li class="previous"><a href="javascript:;">Previous</a></li>
				    <!--<li class="next last" style="display:none;"><a href="javascript:;">Last</a></li>-->
				    <li class="next"><a href="javascript:;">Next</a></li>
				    <li class="finish pull-right"><a href="javascript:;">Finish <i class="fa fa-check"></i></a></li>
			    </ul>

		    </div>
	    </div>
	</div>
</div>