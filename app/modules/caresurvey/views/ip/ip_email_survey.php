<div class="row white" style="margin-top: -20px">

    <div class="col-md-12 text-center">
        <h1>
            <p dir="rtl">استبيان المرضى المنومين</p>Inpatient Survey
        </h1>
    </div>

    <div class="col-md-12 top-info">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row white survey">

    <div class="row padding-50" style="padding-top: -50px">

        <div id="rootwizard" class="tabbable tabs-left">

            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <small>
                        <ul style="list-style: none">
                            <li><a class="hide" href="#tab2" data-toggle="tab">Step 2</a></li>
                            <li><a class="hide" href="#tab3" data-toggle="tab">Step 3</a></li>
                            <li><a class="hide" href="#tab4" data-toggle="tab">Step 4</a></li>
                            <li><a class="hide" href="#tab5" data-toggle="tab">Step 5</a></li>
                            <li><a class="hide" href="#tab6" data-toggle="tab">Step 6</a></li>
                            <li><a class="hide" href="#tab7" data-toggle="tab">Step 7</a></li>
                            <li><a class="hide" href="#tab8" data-toggle="tab">Step 8</a></li>
                            <li><a class="hide" href="#tab9" data-toggle="tab">Step 9</a></li>
                            <li><a class="hide" href="#tab10" data-toggle="tab">Step 10</a></li>
                            <li><a class="hide" href="#tab11" data-toggle="tab">Step 11</a></li>
                            <li><a class="hide" href="#tab12" data-toggle="tab">Step 12</a></li>
                            <li><a class="hide" href="#tab13" data-toggle="tab">Step 13</a></li>
                            <li><a class="hide" href="#tab14" data-toggle="tab">Step 14</a></li>
                        </ul>
                        </small>
                    </div>
                </div>
            </div>

            <!-- KEYS -->
            <input type="hidden" name="type" id="type" value="<?php echo $this->uri->segment(3);?>">
            <input type="hidden" name="hosp" id="hosp" value="<?php echo $this->uri->segment(4);?>">
            <input type="hidden" name="code" id="code" value="<?php echo $this->uri->segment(5);?>">
            <input type="hidden" name="random" id="random" value="<?php echo $this->uri->segment(6);?>">
            <input type="hidden" name="view" id="view" value="by_email">
            <input type="hidden" name="whos_filling_up" id="whos_filling_up" value="<?php echo ($this->session->userdata('username')) ?  : $survey->patname;?>">
            <input type="hidden" name="lastposition" value="<?php echo (isset($survey->lastposition)) ? $survey->lastposition : '';?>">
            <!-- END -->

            <div class="tab-content margin-bottom-20 data" style="margin-top: -50px">

                <div class="tab-pane" id="tab2">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                            <tr class="border-bottom">
                                <td class="padding-10">
                                    <h4 class="head-section bold">Basic Questions</h4>
                                </td>
                                <td dir="rtl" class="text-right">
                                    <h4 class="head-section bold">أسئلة أساسية</h4>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td class="padding-10">
                                    1. Was this your first admission to this hospital?
                                </td>
                                <td dir="rtl" class="text-right">
                                    1- هل كانت هذه أول إقامة لك في المستشفى؟
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <input <?php echo ($survey->q1 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon" value="no" type="radio" name="1" 
                                            data-labelauty="
                                            <br>
                                            <span dir='rtl'>لا</span>
                                            <br>No<br>
                                            " />
                                        </div>

                                        <div class="form-group">
                                            <input <?php echo ($survey->q1 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon" value="yes" type="radio" name="1" 
                                            data-labelauty="
                                            <br>
                                            <span dir='rtl'>نعم</span>
                                            <br>Yes<br>
                                            " />
                                        </div>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td class="padding-10">
                                    2. Did you have a private room? 
                                </td>
                                <td dir="rtl" class="text-right">
                                    2- هل كانت لك غرفة منفردة؟
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <input <?php echo ($survey->q2 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon" value="no" type="radio" name="2" 
                                            data-labelauty="
                                            <br>
                                            <span dir='rtl'>لا</span>
                                            <br>No<br>
                                            " />
                                        </div>

                                        <div class="form-group">
                                            <input <?php echo ($survey->q2 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon" value="yes" type="radio" name="2" 
                                            data-labelauty="
                                            <br>
                                            <span dir='rtl'>نعم</span>
                                            <br>Yes<br>
                                            " />
                                        </div>
                                    </form>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments1"><?php echo $survey->comments1;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab3">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                            <tr class="border-bottom">
                                <td>
                                    <h4 class="head-section bold">Admission</h4>
                                </td>
                                <td dir="rtl" class="text-right">
                                    <h4 class="head-section bold">دخول المستشفى</h4>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    1. Speed of admission process
                                </td>
                                <td dir="rtl" class="text-right">
                                    1- سرعة إجراءات الدخول إلى المستشفى
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q3 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="3" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    2. Courtesy of the person who admitted you
                                </td>
                                <td dir="rtl" class="text-right">
                                    2- اهتمام الشخص الذي قام بإدخالك المستشفى
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q4 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="4" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            
                            </tr><tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments2"><?php echo $survey->comments2;?></textarea>
                                </td>
                            </tr>

                        </table>

                        <!-- END -->

                    </div>

                </div>

                <div class="tab-pane" id="tab4">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                            <tr class="border-bottom">
                                <td>
                                    <h4 class="head-section bold">Room</h4>
                                </td>
                                <td dir="rtl" class="text-right">
                                    <h4 class="head-section bold">الغرفة</h4>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    1. Appearance of room
                                </td>
                                <td dir="rtl" class="text-right">
                                    1- المظهر العام للغرفة
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q5 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="5" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    2. Room cleanliness
                                </td>
                                <td dir="rtl" class="text-right">
                                    2- نظافة الغرفة
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q6 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="6" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    3. Courtesy of the person who cleaned your room
                                </td>
                                <td dir="rtl" class="text-right">
                                    3- اهتمام الشخص الذي قام بتنظيف الغرفة
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q7 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="7" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                4. Room temperature
                                </td>
                                </td>
                                <td dir="rtl" class="text-right">
                                4- درجة حرارة الغرفة
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q8 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="8" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                5. Noise level in and around room
                                </td>
                                <td dir="rtl" class="text-right">
                                5- هدوء الغرفة وما حولها
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q9 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="9" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments3"><?php echo $survey->comments3;?></textarea>
                                </td>
                            </tr>

                        </table>    

                    </div>

                </div>

                <div class="tab-pane" id="tab5">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                            <tr class="border-bottom">
                                <td>
                                    <h4 class="head-section bold">Meals</h4>
                                </td>
                                <td dir="rtl" class="text-right">
                                    <h4 class="head-section bold">الوجبات</h4>
                                </td>
                            </tr>

                            <!--QUESTIONS START -->

                            <tr class="border-bottom">
                                <td>
                                    1. Temperature of the food (cold foods cold, hot foods hot)
                                </td>
                                <td dir="rtl" class="text-right">
                                1- درجة حرارة الطعام (برودة الأطعمة الباردة، وسخونة الأطعمة الساخنة)
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q10 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="10" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                2. Quality of the food
                                </td>
                                <td dir="rtl" class="text-right">
                                2-  جودة الطعام
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q11 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="11" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                3. Courtesy of the person who served your food
                                </td>
                            </td>
                                <td dir="rtl" class="text-right">
                                3- اهتمام الشخص الذي قدم الطعام لك</td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q12 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="12" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                4. In case you were placed on a special or strict diet, was it explained to you?
                                </td>
                            </td>
                                <td dir="rtl" class="text-right">
                                4- في حالة أن تكون قد وضعت على حمية خاصة أو محددة، ما مدى شرحها لك؟
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q13 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="13" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments4"><?php echo $survey->comments4;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab6">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                            <tr class="border-bottom">
                                <td>
                                <h4 class="head-section bold">Nurses</h4>
                                </td>
                                <td dir="rtl" class="text-right">
                                <h4 class="head-section bold">فريق التمريض</h4>
                                </td>
                            </tr>

                            <!--QUESTIONS START -->

                            <tr class="border-bottom">
                                <td>
                                    1. Friendliness/courtesy of the nurses
                                </td>
                                <td dir="rtl" class="text-right">
                                    1- اهتمام فريق التمريض
                                </td>
                            </tr>
                            <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q14 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="14" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                            2. Promptness in responding to the call button
                            </td>
                            <td dir="rtl" class="text-right">
                            2- سرعة الاستجابة لزر الاستدعاء 
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q15 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="15" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                            3. Nurses' attitude toward your requests
                            </td>
                            <td dir="rtl" class="text-right">
                            3- أسلوب فريق التمريض تجاه طلباتك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q16 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="16" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                                4. Amount of attention paid to your special or personal needs
                            </td>
                            <td dir="rtl" class="text-right">
                            4- مدى مراعاة فريق التمريض لاحتياجاتك الخاصة أو الشخصية
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q17 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="17" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                                5. How well the nurses kept you informed
                            </td>
                            <td dir="rtl" class="text-right">
                            5- حرص فريق التمريض بإطلاعك على ما يجري
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q18 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="18" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                            6. Skill of the nurses
                            </td>
                            <td dir="rtl" class="text-right">
                            6- مهارة فريق التمريض
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q19 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="19" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                                7. Assistance you received with daily personal care (bathing, using bathroom/bedpan, walking, etc.)
                            </td>
                            <td dir="rtl" class="text-right">
                            7- المساعدة التي حصلت عليها للرعاية اليومية (الاستحمام، استخدام الحمام  / المبولة، المشي ... إلخ)
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q20 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="20" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                                <td>
                            8. Extent to which nurses checked ID bracelets before giving you medications
                        </td>
                                <td dir="rtl" class="text-right">
                            8- مدى فحص طاقم التمريض لأسوارة التعريف الخاصة بك قبل إعطائك أي أدوية
                            </td>
                        </tr>
                        <tr class="border-bottom">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q21 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="21" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                                Comments<br>
                                (describe good or bad experience):
                            </td>
                            <td dir="rtl" class="text-right">
                                تعليقات<br>
                                (صف التجارب جيدة أو سيئة):
                            </td>
                            
                        </tr>

                        <tr class="border-bottom">
                            <td colspan="5">
                                <textarea rows="5" class="form-control" name="comments5"><?php echo $survey->comments5;?></textarea>
                            </td>
                        </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab7">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Physician</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">الطبيب</h4>
                            </td>
                        </tr>

                        <!--QUESTIONS START -->

                        <tr class="border-bottom">
                                <td>
                            1. Time physician spent with you
                        </td>
                                <td dir="rtl" class="text-right">
                            1- الوقت الذي قضاه الطبيب معك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q22 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="22" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            2. Physician's concern for your questions and worries
                        </td>
                                <td dir="rtl" class="text-right">
                            2- اهتمام الطبيب بأسئلتك ودواعي قلقك 
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q23 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="23" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            3. How well physician kept you informed
                        </td>
                                <td dir="rtl" class="text-right">
                            3- حرص الطبيب بإطلاعك على ما يجري
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q24 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="24" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            4. Friendliness/courtesy of physician
                        </td>
                                <td dir="rtl" class="text-right">
                            4- اهتمام الطبيب
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q25 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="25" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            5. Skill of physician
                        </td>
                                <td dir="rtl" class="text-right">
                            5- مهارة الطبيب
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q26 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="26" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments6"><?php echo $survey->comments6;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab8">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">OR Experience</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">تجربتك في غرفة العمليات</h4>
                            </td>
                        </tr>

                        <!--QUESTIONS START -->

                        <tr class="border-bottom">
                                <td>
                            1. Did you undergo surgery during this hospital stay?
                        </td>
                                <td dir="rtl" class="text-right">
                            1- هل خضعت لعملية جراحية خلال إقامتك هذه بالمستشفى؟
                            </td>
                        </tr>

                        <tr class="border-bottom">
                            <td colspan="2" class="text-center padding-20">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <input <?php echo ($survey->q27 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="false" value="no" type="radio" name="27" 
                                        data-labelauty="<br><br>
                                        <p dir='ltr'>لا (انتقل للقسم التالي)</p>
                                        <br>
                                        No [Proceed to Next Section]<br><br>
                                        " />
                                    </div>

                                    <div class="form-group">
                                        <input <?php echo ($survey->q27 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="false" value="yes" type="radio" name="27" 
                                        data-labelauty="<br><br>
                                        <p dir='ltr'>نعم</p>
                                        <br>
                                        Yes<br><br><br>
                                        " />
                                    </div>
                                </form>
                            </td>
                        </tr>

                        <tr class="border-bottom false" style="display: none">
                                <td>
                            2. Waiting time prior to the start of the surgery
                        </td>
                                <td dir="rtl" class="text-right">
                            2- مدة الانتظار قبل بدء العملية
                            </td>
                        </tr>
                        <tr class="border-bottom false" style="display: none">

                            <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q28 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="28" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                        </tr>

                        <tr class="border-bottom false" style="display: none">
                                <td>
                            3. Instructions you were given by the OR staff about how to prepare for your surgery
                        </td>
                                <td dir="rtl" class="text-right">
                            3- التعليمات التي قدمها لك طاقم غرفة العمليات عن كيفية التحضير للعملية
                            </td>
                        </tr>
                        <tr class="border-bottom false" style="display: none">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q29 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="29" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom false" style="display: none">
                                <td>
                            4. Your confidence that OR staff correctly identified you and your procedure prior to surgery
                        </td>
                                <td dir="rtl" class="text-right">
                            4- ثقتك بأن طاقم غرفة العمليات تأكدوا من هويتك وإجراءك الطبي بشكل صحيح قبل العملية<br>&nbsp;
                            </td>
                        </tr>
                        <tr class="border-bottom false" style="display: none">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q30 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="30" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom false" style="display: none">
                                <td>
                            5. Adequacy of information family received during surgery
                        </td>
                                <td dir="rtl" class="text-right">
                            5- المعلومات المقدمة لعائلتك أثناء إجراء العملية
                            </td>
                        </tr>
                        <tr class="border-bottom false" style="display: none">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q31 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="31" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom false" style="display: none">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom false" style="display: none">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments7"><?php echo $survey->comments7;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab9">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Anesthesia</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">التخدير</h4>
                            </td>
                        </tr>

                        <!--QUESTIONS START -->

                        <tr class="border-bottom">
                                <td>
                            1. Did you undergo anaesthesia before your surgery or procedure?
                        </td>
                                <td dir="rtl" class="text-right">
                            1- هل خضعت للتخدير قبل العملية / الإجراء الطبي ؟
                            </td>
                        </tr>

                        <tr class="border-bottom text-center">
                            <td colspan="2" class="text-center padding-20">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <input <?php echo ($survey->q32 == 2) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="false2" value="no" type="radio" name="32" 
                                        data-labelauty="<br><br>
                                        <p dir='ltr'>لا (انتقل للقسم التالي)</p>
                                        <br>
                                        No [Proceed to Next Section]<br><br>
                                        " />
                                    </div>

                                    <div class="form-group">
                                       <input <?php echo ($survey->q32 == 1) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="false2" value="yes" type="radio" name="32" 
                                        data-labelauty="<br><br>
                                        <p dir='ltr'>نعم</p>
                                        <br>
                                        Yes<br><br><br>
                                        " />
                                    </div>
                                </form>
                            </td>
                        </tr>

                        <tr class="border-bottom false2" style="display: none">
                                <td>
                            2. Explanation by anesthesia staff
                        </td>
                                <td dir="rtl" class="text-right">
                            2- الشرح الذي قدمه لك فريق التخدير
                            </td>
                        </tr>
                        <tr class="border-bottom false2" style="display: none">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q33 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="33" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom false2" style="display: none">
                                <td>
                            3. Friendliness/courtesy of the anesthesia staff
                        </td>
                                <td dir="rtl" class="text-right">
                            3-  اهتمام فريق التخدير
                            </td>
                        </tr>
                        <tr class="border-bottom false2" style="display: none">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q34 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="34" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom false2" style="display: none">
                                <td>
                            4. Your rating of anesthesia experience
                        </td>
                                <td dir="rtl" class="text-right">
                            4- تقييمك لتجربة التخدير
                            </td>
                        </tr>
                        <tr class="border-bottom false2" style="display: none">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q35 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="35" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom false2" style="display: none">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom false2" style="display: none">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments8"><?php echo $survey->comments8;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab10">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Tests and Treatments</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">الاختبارات والعلاج</h4>
                            </td>
                        </tr>

                        <!--QUESTIONS START -->

                        <tr class="border-bottom">
                                <td>
                            1. Waiting time for tests or treatments
                        </td>
                                <td dir="rtl" class="text-right">
                            1- فترة الانتظار للاختبارات والعلاج
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q36 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="36" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            2. Explanations about what would happen during tests and treatments
                        </td>
                                <td dir="rtl" class="text-right">
                            2- الشرح عن ما سيحدث خلال الاختبارات والعلاج
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q37 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="37" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            3. Courtesy of the person who took your blood
                        </td>
                                <td dir="rtl" class="text-right">
                            3- اهتمام الشخص الذي قام بسحب عينة الدم
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q38 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="38" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            4. Skill of the person who took your blood (e.g., did it quickly, with minimal pain)
                        </td>
                                <td dir="rtl" class="text-right">
                            4- مهارة الشخص الذي قام بسحب عينة الدم (قام بسحبها بسرعة، وبأقل ألم)
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q39 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="39" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            5. Courtesy of the person who started the IV
                        </td>
                                <td dir="rtl" class="text-right">
                            5- اهتمام الشخص الذي قام بوضع المحلول الوريدي
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q40 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="40" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments9"><?php echo $survey->comments9;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab11">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Personal Issues</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">أمور شخصية</h4>
                            </td>
                        </tr>

                        <!-- QUESTION START -->
                        <tr class="border-bottom">
                                <td>
                            1. Staff concern for your privacy
                        </td>
                                <td dir="rtl" class="text-right">
                            1- مراعاه العاملين لخصوصيتك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q41 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="41" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            2. How well your pain was controlled
                        </td>
                                <td dir="rtl" class="text-right">
                            2- ما مدى السيطرة على شعورك بالألم
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q42 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="42" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            3. Degree to which hospital staff addressed your emotional needs
                        </td>
                                <td dir="rtl" class="text-right">
                            3- استجابة العاملين بالمستشفى لاحتياجاتك النفسية
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q43 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="43" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            4. Response to concerns/complaints made during your stay
                        </td>
                                <td dir="rtl" class="text-right">
                            4- الاستجابة للمخاوف والشكاوى التي أعربت عنها خلال إقامتك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q44 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="44" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            5. Staff effort to include you in decisions about your treatment
                            </td>
                        </td>
                                <td dir="rtl" class="text-right">
                            5- الجهد الذي بذله العاملون لإشراكك في القرارات الخاصة بعلاجك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q45 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="45" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            6. Staff introduction of themselves
                        </td>
                                <td dir="rtl" class="text-right">
                            6- تعريف الموظفين بأنفسهم لك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q46 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="46" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            7. Staff hand hygiene before examination
                        </td>
                                <td dir="rtl" class="text-right">
                            7- مراعاة العاملين لتعقيم أيديهم قبل فحصك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q47 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="47" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            8. Friendliness/courtesy of patient transporters
                        </td>
                                <td dir="rtl" class="text-right">
                            8- اهتمام العاملين الذين قاموا بنقلك في المستشفى
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q48 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="48" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            9. Extent to which you were informed about all of the medications you received in the hospital (including potential side-effects)
                        </td>
                                <td dir="rtl" class="text-right">
                            9- مدى إطلاعك على جميع الأدوية التي أخذتها في المستشفى (بما في ذلك الأعراض الجانبية المحتملة)
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q49 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="49" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments10"><?php echo $survey->comments10;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab12">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Visitors and Family</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">الزوار والعائلة</h4>
                            </td>
                        </tr>

                        <!-- QUESTION START -->

                        <tr class="border-bottom">
                                <td>
                            1. Accommodations and comfort for visitors
                        </td>
                                <td dir="rtl" class="text-right">
                            1- تجهيزات وراحة الزوار
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q50 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="50" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            2. Staff attitude toward your visitors
                        </td>
                                <td dir="rtl" class="text-right">
                            2- أسلوب العاملين تجاه الزوار
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q51 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="51" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            3. Rating of waiting areas for your visitors
                        </td>
                                <td dir="rtl" class="text-right">
                            3- تقييم منطقة انتظار الزوار
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q52 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="52" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                            <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments11"><?php echo $survey->comments11;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab13">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Discharge</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">الخروج من المستشفى</h4>
                            </td>
                        </tr>

                        <!-- QUESTION START -->
                        <tr class="border-bottom">
                                <td>
                            1. Extent to which you felt ready to be discharged
                        </td>
                                <td dir="rtl" class="text-right">
                            1- مدى شعورك بالاستعداد للخروج من المستشفى
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q53 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="53" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            2. Speed of discharge process after you were told you could go home
                        </td>
                                <td dir="rtl" class="text-right">
                            2- سرعة إجراءات الخروج من المستشفى بعد إخبارك أنه بإمكانك العودة للمنزل
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q54 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="54" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            3. Instructions given about how to care for yourself at home
                        </td>
                                <td dir="rtl" class="text-right">
                            3- التعليمات التي حصلت عليها بشأن العناية بنفسك في المنزل
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q55 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="55" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            4. Explanations regarding taking medicine after discharge including potential side-effects
                        </td>
                                <td dir="rtl" class="text-right">
                            4- الشرح عن الأدوية التي ستأخذها بعد الخروج، بما في ذلك الأعراض الجانبية المحتملة 
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q56 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="56" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            5. Your evaluation of the waiting time to receive medications from the pharmacy
                        </td>
                                <td dir="rtl" class="text-right">
                            5- تقييمك لفترة الانتظار للحصول على الأدوية الموصوفة
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q57 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="57" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            6. Your understanding of follow-up care instructions (appointments, when to call doctor for help) after discharge
                        </td>
                                <td dir="rtl" class="text-right">
                            6- فهمك لتعليمات متابعة العلاج بعد الخروج (موعد المراجعة، متى تتصل بالطبيب للمساعدة)<br>&nbsp;
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q58 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="58" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                                    Comments<br>
                                    (describe good or bad experience):
                                </td>
                                <td dir="rtl" class="text-right">
                                    تعليقات<br>
                                    (صف التجارب جيدة أو سيئة):
                                </td>
                                
                            </tr>

                            <tr class="border-bottom">
                                <td colspan="5">
                                    <textarea rows="5" class="form-control" name="comments12"><?php echo $survey->comments12;?></textarea>
                                </td>
                            </tr>

                        </table>

                    </div>

                </div>

                <div class="tab-pane" id="tab14">

                    <div class="row">

                        <table class="table border-left border-right border-bottom">

                        <tr class="border-bottom">
                                <td>
                            <h4 class="head-section bold">Overall Assesment</h4>
                        </td>
                                <td dir="rtl" class="text-right">
                            <h4 class="head-section bold">التقييم العام</h4>
                            </td>
                        </tr>

                        <!-- QUESTION START -->
                        <tr class="border-bottom">
                                <td>
                            1. How well staff worked together to care for you
                        </td>
                                <td dir="rtl" class="text-right">
                            1- مدى تعاون العاملين في تقديم الرعاية لك
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q59 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="59" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            2. Likelihood of your recommending this hospital to others
                        </td>
                                <td dir="rtl" class="text-right">
                            2- احتمالية أن توصي بهذا المستشفى للآخرين
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q60 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="60" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            3. Overall rating of care given at hospital
                        </td>
                                <td dir="rtl" class="text-right">
                            3- تقييمك العام للرعاية التي تلقيتها في المستشفى
                            </td>
                        </tr>
                        <tr class="border-bottom">

                                <td colspan="2" class="text-center padding-20">
                                    <form class="form-inline">
                                    <?php 
                                        $text = '';
                                        for($count = 1;$count <=5;$count++){ 

                                        switch ($count) {
                                            case '1':
                                                $text = "<br><p dir='rtl'>سيء</p><br>Very Poor<br>";
                                                break;
                                            case '2':
                                                $text = "<br><p dir='rtl'>ضعيف</p><br>Poor<br>";
                                                break;
                                            case '3':
                                                $text = "<br><p dir='rtl'>مقبول</p><br>Fair<br>";
                                                break;
                                            case '4':
                                                $text = "<br><p dir='rtl'>جيد</p><br>Good<br>";
                                                break;
                                            default:
                                                $text = "<br><p dir='rtl'>جيد جداً</p><br>Very Good<br>";
                                                break;
                                        }
                                    ?>
                                        <div class="form-group" style="margin-bottom: 5px">
                                            <input <?php echo ($survey->q61 == $count) ? 'checked' : '';?> class="to-labelauty synch-icon" value="<?php echo $count;?>" type="radio" name="61" 
                                            data-labelauty="
                                            <?php echo $text;?><br>
                                            " />
                                        </div>

                                    <?php } ?>
                                    </form>
                                </td>
                            </tr>

                        <tr class="border-bottom">
                                <td>
                            4. Was there any team member that provided exceptional care / service? Who?
                        </td>
                                <td dir="rtl" class="text-right">
                            4- هل قدّم لك أحد الموظفين رعاية مميزة تذكر؟ هل تذكر اسمهـ(ا)؟
                            </td>
                        </tr>
                        <tr class="border-bottom text-center">
                            <td colspan="2">
                                <center>
                                    <form class="form-inline">
                                        <div class="form-group" style="padding: 0px;padding-bottom: 5px">
                                            <input <?php echo ($survey->hl_qtm == 2) ? 'checked' : '';?> class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="no" name="hl_qtm" 
                                            data-labelauty="<br><br>
                                            <p dir='rtl'>لا</p>
                                            <br>
                                            No<br><br><br>
                                            " />
                                        </div>
                                        <div class="form-group" style="padding: 0px;padding-bottom: 5px">
                                            
                                            <input <?php echo ($survey->hl_qtm == 1) ? 'checked' : '';?> class="to-labelauty synch-icon ACTION" data-show="#ND" type="radio" value="yes" name="hl_qtm"
                                             data-labelauty="
                                                <br>
                                                <p dir='rtl'>نعم (اذكر الإسم والقسم)</p>
                                                <br>
                                                Yes (Mention name & division)<br><br>
                                             "
                                            />

                                        </div>
                                    </form>
                                    <form class="form-inline" id="ND" style="display: none">
                                        <div class="form-group">
                                            Name * <input type="text" value="<?php echo $survey->hl_atm;?>" class="form-control text-center" placeholder="Name" name="hl_atm"> * اسم
                                        </div>
                                        <div>&nbsp;</div>
                                        <div class="form-group">
                                            Division * <input type="text" value="<?php echo $survey->hl_atmd;?>" class="form-control text-center" placeholder="Division" name="hl_atmd"> * القسم
                                        </div>
                                    </form>
                                </center>
                            </td>
                        </tr>

                        <tr class="border-bottom">
                            <td>
                                Comments<br>
                                (describe good or bad experience):
                            </td>
                            <td dir="rtl" class="text-right">
                                تعليقات<br>
                                (صف التجارب جيدة أو سيئة):
                            </td>
                            
                        </tr>

                        <tr class="border-bottom">
                            <td colspan="5">
                                <textarea rows="5" class="form-control" name="comments13"><?php echo $survey->comments13;?></textarea>
                            </td>
                        </tr>

                        <tr class="border-bottom extra" style="display: none">
                                <td>
                                    Would you like to share your contact information along the comments you gave with the hospital?
                                </td>
                                <td dir="rtl" class="text-right">
                                سيتم إرسال تعليقاتك للمستشفى، هل ترغب بإرفاق اسمك ورقم هاتفك معها؟<br>&nbsp;
                                </td>
                        </tr>

                        <tr class="border-bottom text-center extra" style="display: none">
                            <td colspan="2">
                                <center>
                                    <form class="form-inline">
                                        <div class="form-group" style="padding: 0px;padding-bottom: 5px">
                                            <input <?php echo ($survey->hl_disclaimer == 2) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" value="no" data-show="false" type="radio" name="hl_disclaimer" 
                                            data-labelauty="
                                            <br>
                                            <p dir='rtl'>لا</p>
                                            <br>
                                            No<br><br>
                                            " />
                                        </div>
                                        <div class="form-group" style="padding: 0px;padding-bottom: 5px">
                                            
                                            <input <?php echo ($survey->hl_disclaimer == 1) ? 'checked' : '';?> class="to-labelauty synch-icon skipme" data-show="false" value="yes" type="radio" name="hl_disclaimer" 
                                            data-labelauty="
                                            <br>
                                            <p dir='rtl'>نعم</p>
                                            <br>
                                            Yes<br><br>
                                            " />

                                        </div>
                                    </form>
                                </center>
                            </td>

                        </tr>

                        </table>
                    </div>

                </div>

                 <div class="row prog">
                    <div id="bar" class="progress">
                        <div class="progress-bar active progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        </div>
                    </div>
                </div>

                <ul class="pager wizard">
                    <li class="previous prev"><a href="javascript:;">Previous</a></li>
                    <li class="next"><a href="javascript:;">Next</a></li>
                    <li class="finish pull-right" data-setup="completebymail"><a href="javascript:;">Finish <i class="fa fa-check"></i></a></li>
                </ul>

            </div>

        </div>
    </div>
</div>