<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
		<li class="active">Care</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          <h3><i class="fa fa-hospital-o"></i> Hospitals<small>&nbsp; Care hospital services</small></h3>
        </div>
    </div>

</div>

<?php if($host): ?>

	<div class="row">

		<?php foreach($host as $v):?>

		<div class="col-md-4 col-sm-6 col-md-offset-1">

	      <a href="caresurvey/byhospital/<?=$v->code;?>">
	        <div class="border-double">
	        	<center>
		        	<div class="thumbnail" style="border: 1px solid #fff">
			          	<div class="row">
			            	<?php if($v->code == 1): ?>
                      		<img src="<?php echo PATH_IMG ?>careimg.jpg" width="226"/>
	                      	<?php else: ?>
	                      	<img src="<?php echo PATH_IMG ?>rnh.jpg" width="200"/>
	                      	<?php endif; ?>
			            	<div class="col-md-12">&nbsp;</div>
			              	<h3><p dir="rtl"><?=$v->description_ar;?></p><?=$v->description;?></h3>
			          	</div>
			        </div>
		        </center>
	        </div>
	      </a>

	    </div>
		
		<?php endforeach;?>

	</div>
<?php else: ?>
<!-- nothing to show -->
<?php endif; ?>

