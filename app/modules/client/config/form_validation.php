<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config = array(

        'client' => array(

                array(
                        'field' => 'client_id',
                        'label' => 'Client ID',
                        'rules' => 'required|trim|max_length[50]|is_numeric'
                ),
                array(
                        'field' => 'client_name',
                        'label' => 'Client Name',
                        'rules' => 'required|trim|max_length[255]'
                ),
                array(
                        'field' => 'client_description',
                        'label' => 'Client Description',
                        'rules' => 'required|trim|max_length[500]'
                ),
                array(
                        'field' => 'web_url',
                        'label' => 'Web URL',
                        'rules' => 'required|trim|max_length[100]|prep_url|valid_url_format'
                ),
                array(
                        'field' => 'webservice_url',
                        'label' => 'Web Service URL',
                        'rules' => 'required|trim|max_length[100]|prep_url|valid_url_format'
                ),

        ),

        'survey' => array(

                array(
                        'field' => 'cc_dept_overall',
                        'label' => 'CC Dept Overall',
                        'rules' => 'trim|max_length[255]|prep_url|valid_url_format'
                ),
                array(
                        'field' => 'cc_dept',
                        'label' => 'CC Dept',
                        'rules' => 'trim|max_length[255]|prep_url|valid_url_format'
                ),
                array(
                        'field' => 'cc_response',
                        'label' => 'CC Responses',
                        'rules' => 'trim|max_length[255]|prep_url|valid_url_format'
                ),
                array(
                        'field' => 'dept_overall',
                        'label' => 'Dept Overall',
                        'rules' => 'trim|max_length[255]|prep_url|valid_url_format'
                ),
                array(
                        'field' => 'sms_overall',
                        'label' => 'SMS Overall',
                        'rules' => 'trim|max_length[255]|prep_url|valid_url_format'
                ),
                array(
                        'field' => 'sms_dept_overall',
                        'label' => 'SMS Dept Overall',
                        'rules' => 'trim|max_length[255]|prep_url|valid_url_format'
                ),

        ),
);

?>