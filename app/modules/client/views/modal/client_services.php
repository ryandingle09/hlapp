<form class="form-submit" data-identity="survey" data-id="<?php echo $id;?>" data-client="<?php echo (isset($client_id)) ? $client_id : '';?>">
	<div class="modal-header">
		<button type="button" class="close close_modal" data-modal="services_modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><i class="fa fa-link"></i> Client Web Service</h4>
	</div>

	<div class="modal-body">
	
		<div class="row">

			<div class="col-md-12">
				<div class="alert alert-danger" style="display: none;">
					<p class="text"></p>
				</div>

				<div class="alert alert-success" style="display: none;">
					<p>Successfully saved. <i class="fa fa-check"></i></p>
				</div>
			</div>

			<div class="col-md-12">

				<div class="form-group">
					<label>CC Depth Overall</label>
					<input type="url" name="cc_dept_overall" value="<?php echo (isset($survey->cc_dept_overall)) ? $survey->cc_dept_overall : '';?>" class="form-control">
				</div>

				<div class="form-group">
					<label>CC Dept</label>
					<input type="url" name="cc_dept" value="<?php echo (isset($survey->cc_dept)) ? $survey->cc_dept : '';?>" class="form-control">
				</div>

				<div class="form-group">
					<label>CC Responses</label>
					<input type="url" name="cc_response" value="<?php echo (isset($survey->cc_response)) ? $survey->cc_response : 'sdsd';?>" class="form-control">
				</div>

				<div class="form-group">
					<label>Dept Overall</label>
					<input type="url" name="dept_overall" value="<?php echo (isset($survey->dept_overall)) ? $survey->dept_overall : '';?>" class="form-control">
				</div>

				<div class="form-group">
					<label>SMS Overall</label>
					<input type="url" name="sms_overall" value="<?php echo (isset($survey->sms_overall)) ? $survey->sms_overall : '';?>" class="form-control">
				</div>

				<div class="form-group">
					<label>SMS Dept Overall</label>
					<input type="url" name="sms_dept_overall" value="<?php echo (isset($survey->sms_dept_overall)) ? $survey->sms_dept_overall : '';?>" class="form-control">
				</div>				

			</div>

		</div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default close_modal" data-modal="services_modal">Close</button>
		<button type="submit" class="btn btn-primary btn-save" data-loading-text="Saving....." autocomplete="off">Save  <i class="fa fa-save"></i></button>
	</div>
</form>