<div class="row" style="margin-top: -15px">

    <ul class="breadcrumb">
        <li><a href="caresurvey/dashboard" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li class="active">Client</li>
    </ul>

    <div class="col-md-12" style="margin-top: -30px">
        <div class="page-header">
          	<h3>
				<i class="fa fa-users"></i> Clients -<small>&nbsp; Showing list of clients</small>
				<button class="btn btn-success pull-right show_modal"  data-modal="client_modal" data-action="post" data-identity="client" data-id=""><i class="fa fa-plus"></i> Add New Client</button>
			</h3>
        </div>
    </div>
</div>

<div class="row action">
	<div class="col-md-12">
		<table id="clients" class="table table-striped" width="100%" cellspacing="0">
			<thead>
			    <tr>
			        <th>Client ID</th>
			        <th>Client Name</th>
			        <th>Client Description</th>
			        <th>Web URL</th>
			        <th>Web Service URL</th>
			        <th width="15%" class="text-center">Action</th>
			    </tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>