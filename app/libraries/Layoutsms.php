<?php
class Layoutsms
{
	private $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	public function view($view_name, $param = array(), $layouts = 'template_sms_view')
	{
		$render_view = $this->CI->load->view($view_name, $param, TRUE);
		
		$this->CI->load->view('layouts/' . $layouts, array(
			'content' => $render_view
		));
	}
}
