var app 	= require('express')();
var http 	= require('http').Server(app);
var io 		= require('socket.io')(http);

io.on('connection', function(socket){

  	socket.on('click', function(data){

    	io.emit('click', data.code, data.random);

    	console.log(data.code+'/'+data.random);

  	});

});

http.listen(9000, function(){

  	console.log('listening on *:9000');

});