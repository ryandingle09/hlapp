var elements = {
	doc      				: $(document),
	show_modal 		 		: '.show_modal',
	close_modal 			: '.close_modal',
	load_modal  			: '.load_modal',
	form_submit  			: '.form-submit',
	delete 					: '#delete',
	activate 		 		: '.activate',
}

;(function(){

	var Client = {

		activateConf : function(){
			return this.delegate(elements.activate,'click',function(){
				var dis 				= $(this)
				var id 		 			= dis.data('id');
				var employee_id 		= dis.data('employee_id');
				var action 				= dis.data('action');
				var identity 			= dis.data('identity');

				$.post(baseURL+'user_access/activate',{'id': id,'employee_id': employee_id, 'action':action , 'identity':identity },function(){
					if(identity == 'client')
					{
						if(action == 'activate')
						{
							$('.status_c_'+id+'').html('<span class="badge alert-success">Activated</span>');
							$('.action_c_'+id+'').html('<button type="button" class="btn btn-danger btn-sm activate" data-action="deactivate" data-identity="client" data-id="'+id+'" data-employee_id="'+employee_id+'">Deactivate <i class="fa fa-close"></i></button>');
						}
						else
						{
							$('.status_c_'+id+'').html('<span class="badge">Deactivated</span>');
							$('.action_c_'+id+'').html('<button type="button" class="btn btn-info btn-sm activate" data-action="activate" data-identity="client" data-id="'+id+'" data-employee_id="'+employee_id+'">Activate <i class="fa fa-check"></i></button>');
						}
					}
					else
					{
						if(action == 'activate')
						{
							$('.status_m_'+id+'').html('<span class="badge alert-success">Activated</span>');
							$('.action_m_'+id+'').html('<button type="button" class="btn btn-danger btn-sm activate" data-action="deactivate" data-identity="module" data-id="'+id+'" data-employee_id="'+employee_id+'">Deactivate <i class="fa fa-close"></i></button>');
						}
						else
						{
							$('.status_m_'+id+'').html('<span class="badge">Deactivated</span>');
							$('.action_m_'+id+'').html('<button type="button" class="btn btn-info btn-sm activate" data-action="activate" data-identity="module" data-id="'+id+'" data-employee_id="'+employee_id+'">Activate <i class="fa fa-check"></i></button>');
						}
					}
				});
			});
		},

		closeModalConf : function(){
			return this.delegate(elements.close_modal,'click',function(){
				var dis = $(this)
				var modal = dis.data('modal');

				$('.'+modal+'').modal('hide');
			});
		},

		deleteConf : function(){
			return this.delegate(elements.delete,'click',function(){
				var dis 		= $(this)
					identity	= dis.data('identity');
					action 		= dis.data('action');
					id 		 	= dis.data('id');
					modal_name 	= dis.data('modal');
					data   		= {id:id};

				load_button('btn-delete');
				
				$.post(baseURL+'user_access/process/'+action+'/'+identity+'/', data, function(response){
					if(response == '1')
					{
						window.location = baseURL+'user_access/?message=clear';
					}
					else alert('Something went wrong. PLease try again later.');
				});

				reset_button('btn-delete');
			});
		},

		actionConf : function(){
			return this.delegate(elements.show_modal,'click',function(){
				var dis 		= $(this)
					identity	= dis.data('identity');
					action 		= dis.data('action');
					id 		 	= dis.data('id');
					modal_name 	= dis.data('modal');
					data   		= {id:id};

				if(action == 'delete') modal_delete(identity, id);
				else
				{
					if(action == 'get')
					{	
						create_modal(modal_name, 'lg', 'action', 'show');
						load_modal(baseURL+'user_access/load_modal/', modal_name, 'user_info', id, identity);
						
					}
					else
					{
						create_modal(modal_name, 'default', 'action', 'show');
						load_modal(baseURL+'user_access/load_modal', modal_name, 'edit_info', id, identity);
					}
				}
			});
		},

		formSubmitConf : function(e){
			return this.delegate(elements.form_submit,'submit',function(e){
				e.preventDefault();
				e.stopPropagation();

				var dis 			= $(this)
					id 				= dis.data('id');
					uold 			= dis.data('uold');
					eold 			= dis.data('eold');
					action   		= (id == '') ? 'post' : 'update';
					identity 		= dis.data('identity');
					data 			= new FormData(this);
					data.append('id', id);
					data.append('uold', uold);
					data.append('eold', eold);

				load_button('btn-save');
				$.ajax({
					url 	: baseURL+'user_access/process/'+action+'/'+identity+'/',
					type 	: 'POST',
					cache 	: 'FALSE',
					data 	: data,
					enctype: 'multipart/form-data',
					processData: false,
					contentType: false,
					success : function(result){
						if(result == '1')
						{
							if(id == '')
								window.location = baseURL+'user_access/?message=post';
							else
								window.location = baseURL+'user_access/?message=update';
						}
						else
						{
							$('.alert-danger').show();
							$('.alert-success').hide();
							$('.alert-danger .text').html(result);
						}

						reset_button('btn-save');
					},


				});
			});
		},
	}


	$.extend(elements.doc, Client);
	elements.doc.actionConf();
	elements.doc.deleteConf();
	elements.doc.closeModalConf();
	elements.doc.formSubmitConf();
	elements.doc.activateConf();

}(jQuery,window,document));	

load_datatable('users', 'user_access/get_data_table/');

/*$(document).on('hidden.bs.modal', function (event) {
  if ($('.modal:visible').length) {
    $('body').addClass('modal-open');
  }
});*/