/** commont usage **/

//SELETIZE
$("#upload-date").selectize();
'$("select").selectize();';
$(document).ready(function() {

    //DATATABLES
    $('#patients').DataTable();

    //WIZARD 
    $('#rootwizard').bootstrapWizard(
        {
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard .progress-bar').css({width:$percent+'%'});
            },

            //'tabClass': 'nav nav-tabs',

        }
    );

    //ELEMENTS
    $('.ACTION').click(function() {
        var dis = $(this)

        if($(this).val() == 'yes')
        {
            $(dis.attr('data-show')).show();
            $('input[name="name1"]').focus();
        }
        else
        {
            $(dis.attr('data-show')).hide();
            $('input[name="name2"]').focus();
        }
    });

    //SCROLLTOP
    $('.next').click(function() {
         $('html, body').animate({
            scrollTop: $(".container").offset().top
        }, 500);
     });

});