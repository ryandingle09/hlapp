var elements = {
	doc      				: $(document),
	show_modal 		 		: '.show_modal',
	close_modal 			: '.close_modal',
	load_modal  			: '.load_modal',
	form_submit  			: '.form-submit',
}

;(function(){

	var Account = {

		closeModalConf : function(){
			return this.delegate(elements.close_modal,'click',function(){
				var dis = $(this)
				var modal = dis.data('modal');

				$('.'+modal+'').modal('hide');
			});
		},

		actionConf : function(){
			return this.delegate(elements.show_modal,'click',function(){
				var dis 		= $(this)
					identity	= dis.data('identity');
					action 		= dis.data('action');
					id 		 	= dis.data('id');
					client_id 	= dis.data('client');
					modal_name 	= dis.data('modal');
					data   		= {id:id};

				if(identity == 'account')
				{
					create_modal('account_modal', 'default', 'account', 'show');
					load_modal(baseURL+'account/load_modal', modal_name, 'account_form', id, identity);
				}
				if(identity == 'account_password')
				{
					create_modal('account_modal', 'sm', 'account', 'show');
					load_modal(baseURL+'account/load_modal', modal_name, 'account_password_form', id, identity);
				}
			});
		},

		formSubmitConf : function(e){
			return this.delegate(elements.form_submit,'submit',function(e){
				e.preventDefault();
				e.stopPropagation();

				var dis 			= $(this)
					id 				= dis.data('id');
					action   		= (id == '') ? 'post' : 'update';
					identity 		= dis.data('identity');
					data 			= new FormData(this);
					//data.append('id2', client);

				load_button('btn-save');
				$.ajax({
					url 	: baseURL+'account/process/'+action+'/'+identity+'/'+id,
					type 	: 'POST',
					cache 	: 'FALSE',
					data 	: data,
					enctype: 'multipart/form-data',
					processData: false,
					contentType: false,
					success : function(result){
						if(result == '1')
						{
							location.reload();
						}
						else
						{
							$('.alert-danger').show();
							$('.alert-success').hide();
							$('.alert-danger .text').html(result);
						}

						reset_button('btn-save');
					},


				});
			});
		},
	}


	$.extend(elements.doc, Account);
	elements.doc.actionConf();
	elements.doc.closeModalConf();
	elements.doc.formSubmitConf();

}(jQuery,window,document));	