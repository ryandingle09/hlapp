
/**SELETIZE*
//$("#upload-date").selectize();
//$("select").selectize();**/

$("#date, #date2").datetimepicker(
    {
        timepicker:false, format:"m/d/Y"
    }
);

$(document).ready(function() {

    /**radio label authy**/
    $(":radio").labelauty();

    /**DATATABLES**/
    $('#patients').DataTable();

    /**WIZARD**/ 
    $('#rootwizard').bootstrapWizard(
        {
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard .progress-bar').css({width:$percent+'%'});
            },

        }
    );

    /**ELEMENTS**/
    $('.ACTION').click(function() {
        var dis = $(this)

        if($(this).val() == 'yes')
        {
            $(dis.attr('data-show')).show();
            $('input[name="name1"]').focus();
        }
        else
        {
            $(dis.attr('data-show')).hide();
            $('input[name="name2"]').focus();
        }
    });

    $('.skipme').each(function(){
        if($(this).attr('checked'))
        {
            if($(this).val() == 'no')
                $('.'+$(this).data('show')).hide();
            else
                $('.'+$(this).data('show')).show();
        }
    });

    $('.skipme').click(function() {
        if($(this).val() == 'no')
            $('.'+$(this).data('show')).hide();
        else
            $('.'+$(this).data('show')).show();
    });

    $('.initial').click(function(){
        if($(this).val() != '1'){
            $('.a-me').hide();
            $('.'+$(this).data('show')).toggle().addClass('a-me');
            $('.not-proceeding').show();
            $('.pager, .prog').hide();
        }else{
            $('.a-me').hide();
            $('.not-proceeding').hide();
            $('.pager').show();
        }
    });

    $('.proceed').click(function(){
        $('.survey').show();
         $('html, body').animate({
            scrollTop: $(".top-info").offset().top
        }, 500);
        $('.top-info').hide();
    });


    /**TRANSFORM INTO ARABIC **/
    if($('p').attr('dir'))
    {
        $('p').addClass('droid-arabic-kufi');
        
    }
    $("td[dir]").attr('class','ar');

    $('.previous').click(function() {
        if($('.previous').hasClass('disabled'))
        {
            $('.previous').hide();
        }
    });
    /**END**/

    /** MANUAL TRIGGER **/
    //$('.ACTION[checked]').trigger('click');
    $('.ACTION[checked]').each(function(){
        if($(this).val() == 'yes')
        {
            $($(this).data('show')).show();
        }
    });

    $('textarea').each( function(){
        if($(this).val() != ''){
            $('.extra').show();
        }
    });

    /** LAST POSITION TRIGGER **/
    if($('#rootwizard').is(":visible"))
    {
        $('#rootwizard').bootstrapWizard('show',$('input[name="lastposition"]').val());
        if($('#rootwizard').bootstrapWizard('currentIndex') == 0 || $('#rootwizard').bootstrapWizard('currentIndex') == '') $('.previous').hide(); else $('.previous').show();
    }
    else
    {
        if($('.previous').hasClass('disabled'))
        {
            $('.previous').hide();
        }
    }

    /** USER DETECTOR ACTION SURVEY SEARCH **/
    if($('#view').val() == 'search')
    {
        if($('input[name="amendby"]').val() != $('input[name="username"]').val())
        {
            $('input, textarea, select').attr('disabled', 'disabled');
            $('.finish').remove();
            $('.pager').show();
            $('.white, .pager').removeClass('survey');
        }
    }

    /** USER DETECTOR ACTION SURVEY MAIN **/
    if($('#view').val() == 'main')
    {
        if($('input[name="lastposition"]').val() != 0)
        {
            $('.pager, .prog').show();
            $('.top-info').hide();
        }
        else
        {
            $('.top-info').show();
            $('input[name="initial_response"][checked]')
            {
                if($('input[name="initial_response"][checked]').val() == '1')
                {
                    $('.pager').show();
                }
                else
                {
                    $('.'+$('input[name="initial_response"][checked]').data('show')).show().addClass('a-me');
                    $('.not-proceeding').show();
                }
            }
        }
    }

    /** PREPARE MODAL ALERT **/
    var modal_alert = '<div class="modal modal-alert" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
    modal_alert += '<div class="modal-dialog modal-sm" role="document">';
    modal_alert += '<div class="modal-content">';
    modal_alert += '<div class="modal-header">';
    modal_alert += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>';
    modal_alert += '<h4 class="modal-title" id="mySmallModalLabel"></h4>';
    modal_alert += '</div>';
    modal_alert += '<div class="modal-body">';
    modal_alert += '</div>';
    modal_alert += '<div class="modal-footer">';
    modal_alert += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
    modal_alert += '</div>';
    modal_alert += '</div>';
    modal_alert += '</div>';
    modal_alert += '</div>';

    $('body').append(modal_alert);

    /**END ELEMENTS**/

    /**SCROLLTOP**/
    $('.next').click(function() {
         $('html, body').animate({
            scrollTop: $(".top-info").offset().top
        }, 500);

        $('.top-info').hide();
        $('.previous').show();
        $('.first, .last').hide();
        $('.prog').show();
     });

    

    $('.not-proceeding').hide();

});