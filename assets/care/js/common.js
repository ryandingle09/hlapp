/**COMMON HELPFULL FUNCTIONS**/

/** EMAIL ADDRESS **/
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

/** TRIGGER ACTION **/
function trigger_action(element_id_or_class, event)
{
    $(element_id_or_class).trigger(event);
}

/**ACTIVATE LOADING BUTTON**/
function load_button(btn_class)
{
    var $btn = $('.'+btn_class+'');
    $btn.button('loading');
}

/** RESET LOADING BUTTON**/
function reset_button(btn_class)
{
    var $btn = $('.'+btn_class+'');
    $btn.button('reset');
}

/** MODAL DELETE/ALERT **/
function modal_delete(identity, id)
{
    var modal = '<div class="modal modal_delete" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
    modal += '<div class="modal-dialog modal-sm" role="document">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    modal += '<h4 class="modal-title"><i class="fa fa-info"></i> Alert</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p class="text-center" style="font-size: 40px;"><i class="fa fa-trash-o"></i></p>';
    modal += '<h3 class="text-center">Are you sure you want to delete this data ?<br><small>This data will permanently deleted.</small></h3>';
    modal += '</div>';
    modal += '<div class="modal-footer">';
    modal += '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
    modal += '<button type="submit" class="btn btn-primary btn-delete" id="delete" data-action="delete" data-id="'+id+'" data-identity="'+identity+'" data-loading-text="Saving....." autocomplete="off">Proceed</button>';
    modal += '</div>';

    modal += '</div>';
    modal += '</div>';
    modal += '</div>';

    if($('.modal').hasClass('modal_delete')){
        //do nothing
    }
    else{
        $('body').append(modal);  
    }

    $('.modal_delete').modal('show');
}

/** CREATE DYNAMIC MODAL **/
function create_modal(modal_name, modal_size, append_to, event)
{
    var modal = '<div class="modal '+modal_name+' fade" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
    modal += '<div class="modal-dialog modal-'+modal_size+'" role="document">';
    modal += '<div class="modal-content">';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';

    if($('.modal').hasClass(modal_name))
    {
        $('.'+modal_name+' .modal-dialog')
            .removeClass('modal-lg')
            .removeClass('modal-sm')
            .removeClass('modal-')
            .addClass('modal-'+modal_size);
    }
    else
    {
        if(append_to == '')
            $('body').append(modal);
        else
            $('.'+append_to+'').append(modal);
        
    }

    if(event == 'show')
        $('.'+modal_name).modal('show');

    $('.'+modal_name+' .modal-content').html('<h3 class="text-center">Loading...</h3>');
}

/**INSERT MODAL CONTENT **/
function modal_content(modal_name, content)
{
    $('.'+modal_name+' .modal-content').html(content);
}

/** LOAD MODAL **/
function load_modal(url, modal_name, pagefile, id, identity)
{
    $(document).ready(function(){
        $('.'+modal_name+' .modal-content').html('<h3 class="text-center">Loading...</h3>');
        $('.'+modal_name).modal('show');
        
        $.post(url,{ content: pagefile, id:id, identity:identity },function(response){
            modal_content(modal_name, response);
        });
    });
}

/**-- DATA TABLES --**/
function load_datatable(table_id, path, scrollX){

    var order = (table_id == 'audit_log_table') ? 3 : 0;
    
    var table = {
        table_id : function(){ 
    
            $("#"+table_id).dataTable(
                {
                    "bDestroy": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "scrollX": scrollX,
                    "fixedColumns": {
                        "leftColumns": 1,
                        //"rightColumns": 1
                    },
                    "sAjaxSource": baseURL + path, 
                    "order": [[ order, "asc" ]],
                    "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                            "dataType": 'json', 
                            "type": "POST", 
                            "url": sSource, 
                            "data": aoData, 
                            "success": fnCallback
                        } );    
                    },
                    columnDefs: [
                        { orderable: false, targets: -1 }
                    ]
                }
            );
        }
    }

    table.table_id();
}