

function check_empty(values)
{
   if (values == "")
   {
     return 0;
   }
   else
   {
     return parseInt(values);
   }
}


function compute_intros()
{
    document.getElementById('intros').value = check_empty(document.getElementById("intros1").value) + check_empty(document.getElementById("intros2").value) + check_empty(document.getElementById("intros3").value) + check_empty(document.getElementById("intros4").value) + check_empty(document.getElementById("intros5").value) + check_empty(document.getElementById("intros6").value);
    document.getElementById('introc').value = check_empty(document.getElementById("introc1").value) + check_empty(document.getElementById("introc2").value) + check_empty(document.getElementById("introc3").value) + check_empty(document.getElementById("introc4").value) + check_empty(document.getElementById("introc5").value) + check_empty(document.getElementById("introc6").value);
}

function compute_survey()
{
    document.getElementById('surveys').value = check_empty(document.getElementById("surveys1").value) + check_empty(document.getElementById("surveys2").value) + check_empty(document.getElementById("surveys3").value) ;
    document.getElementById('surveyc').value = check_empty(document.getElementById("surveyc1").value) + check_empty(document.getElementById("surveyc2").value) + check_empty(document.getElementById("surveyc3").value) ;
}

function compute_inter()
{
    document.getElementById('inters').value = check_empty(document.getElementById("inters1").value) + check_empty(document.getElementById("inters2").value) + check_empty(document.getElementById("inters3").value) + check_empty(document.getElementById("inters4").value) + check_empty(document.getElementById("inters5").value) + check_empty(document.getElementById("inters6").value) + check_empty(document.getElementById("inters7").value) + check_empty(document.getElementById("inters8").value) + check_empty(document.getElementById("inters9").value) + check_empty(document.getElementById("inters10").value) + check_empty(document.getElementById("inters11").value);
    document.getElementById('interc').value = check_empty(document.getElementById("interc1").value) + check_empty(document.getElementById("interc2").value) + check_empty(document.getElementById("interc3").value) + check_empty(document.getElementById("interc4").value) + check_empty(document.getElementById("interc5").value) + check_empty(document.getElementById("interc6").value) + check_empty(document.getElementById("interc7").value) + check_empty(document.getElementById("interc8").value) + check_empty(document.getElementById("interc9").value) + check_empty(document.getElementById("interc10").value) + check_empty(document.getElementById("interc11").value);
}

function compute_verba()
{
  document.getElementById('verbas').value = check_empty(document.getElementById("verbas1").value) + check_empty(document.getElementById("verbas2").value) + check_empty(document.getElementById("verbas3").value) + check_empty(document.getElementById("verbas4").value) + check_empty(document.getElementById("verbas5").value) + check_empty(document.getElementById("verbas6").value) + check_empty(document.getElementById("verbas7").value) + check_empty(document.getElementById("verbas8").value);
    document.getElementById('verbac').value = check_empty(document.getElementById("verbac1").value) + check_empty(document.getElementById("verbac2").value) + check_empty(document.getElementById("verbac3").value) + check_empty(document.getElementById("verbac4").value) + check_empty(document.getElementById("verbac5").value) + check_empty(document.getElementById("verbac6").value) + check_empty(document.getElementById("verbac7").value) + check_empty(document.getElementById("verbac8").value);

}

function compute_closure()
{
    document.getElementById('closures').value = check_empty(document.getElementById("closures1").value) + check_empty(document.getElementById("closures2").value);
    document.getElementById('closurec').value = check_empty(document.getElementById("closurec1").value) + check_empty(document.getElementById("closurec2").value);
}



function compute_all()
{
  var i;
  var counter;
  i =0;
  counter = 0;
  for (i=1;i<7;i++)
  {
     if (document.getElementById("intros"+i).value != "")
         {
           if (document.getElementById("intros"+i).value == 0)
           {
            counter = counter + 1;
           }
         }
  }

  i =0;
  for (i=1;i<12;i++)
  {
     if (document.getElementById("inters"+i).value != "")
         {
           if (document.getElementById("inters"+i).value == 0)
           {
            counter = counter + 1;
           }
         }
  }

  i =0;
  for (i=1;i<9;i++)
  {
     if (document.getElementById("verbas"+i).value != "")
         {
           if (document.getElementById("verbas"+i).value == 0)
           {
            counter = counter + 1;
           }
         }
  }

  document.getElementById('total_score').value = check_empty(document.getElementById("intros").value)+check_empty(document.getElementById("surveys").value)+check_empty(document.getElementById("inters").value)+check_empty(document.getElementById("verbas").value)+check_empty(document.getElementById("closures").value);
  document.getElementById('ach_score').value = check_empty(document.getElementById("introc").value)+check_empty(document.getElementById("surveyc").value)+check_empty(document.getElementById("interc").value)+check_empty(document.getElementById("verbac").value)+check_empty(document.getElementById("closurec").value);
  document.getElementById('ach_score').value = 148;
    document.getElementById('critical_error').value = counter;

    if (counter>0)
    {
      document.getElementById('final_score').value = '0.0%';
      $('#critical_error').addClass('red-notif');
      $('#final_score').addClass('red-notif');
      
    }
    else
    {
      $('#critical_error').removeClass('red-notif');
      $('#final_score').removeClass('red-notif');
      document.getElementById('final_score').value =  (check_empty(document.getElementById('total_score').value )/check_empty(document.getElementById('ach_score').value) * 100).toFixed(2) + ' %';
    }
}

$(document).ready(function() {

        $('#submit').on('click', function() { 
       
             
                 $.ajax({
                       url: '/careeval/search_ajax',
                       type: "get",
                       data: {
                       client: document.getElementById("client").value,
                       surveyor: document.getElementById("surveyor").value,
                       start_date: document.getElementById("start_date").value,
                       end_date: document.getElementById("end_date").value,
                        },
                       dataType: "json",
                      success:function(data) {

                        document.getElementById("num_records").innerHTML = data.length + " records";
                        document.getElementById("display_records").innerHTML  = "";
                         for (j=0;j<data.length;j++)
                         {
                            document.getElementById("display_records").innerHTML =  document.getElementById("display_records").innerHTML + '<tr><td>' + data[j]['client_name'] + '</td><td>' + data[j]['surveyor_name'] + '</td><td>' + data[j]['completeddate'] + '</td><td><a href="careeval/viewreport?code=' + data[j]['code'] + '">View Report</a></td></tr>';
                         }


                        } //sucess end

                      });    
                 
        }); //submit end


        $('select[name="client"]').on('change', function() {          
              var client_data = document.getElementById("client").value;

              if (client_data > 0)
              { 
                document.getElementById("surveyor").disabled = false;
          
               $.ajax({
                       url: '/careeval/clientsurveyor_ajax',
                       type: "get",
                       data: {
                       client: document.getElementById("client").value,
                        },
                       dataType: "json",
                      success:function(data) {
                        $('select[name="surveyor"]').empty();
                        $('select[name="surveyor"]').append('<option value="">Select Surveyor</option>');
                              for (j=0;j<data.length;j++)
                              {
                                 $('select[name="surveyor"]').append('<option value="'+ data[j]['surveyor_id'] +'">'+ data[j]['surveyor_name'] +'</option>');
                              }


                        } //sucess end


                      });    

             }
             else
             {
               document.getElementById("surveyor").disabled = true;
             }

          });

        $('select[name="intro1"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("intros1").value = "5";
                              document.getElementById("introc1").value = "5";
                              break;
                      case 'No':
                              document.getElementById("intros1").value = "1";
                              document.getElementById("introc1").value = "5";
                              break;
                      default:
                              document.getElementById("intros1").value = "";
                              document.getElementById("introc1").value = "";
                      } 
          compute_intros();           
          compute_all();
       });

        $('select[name="intro2"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("intros2").value = "3";
                              document.getElementById("introc2").value = "3";
                              break;
                      case 'No':
                              document.getElementById("intros2").value = "1";
                              document.getElementById("introc2").value = "3";
                              break;
                      default:
                              document.getElementById("intros2").value = "";
                              document.getElementById("introc2").value = "";
                      } 
          compute_intros();           
          compute_all();
       });


        $('select[name="intro3"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("intros3").value = "10";
                              document.getElementById("introc3").value = "10";
                              break;
                      case 'No':
                              document.getElementById("intros3").value = "0";
                              document.getElementById("introc3").value = "10";
                              break;
                      default:
                              document.getElementById("intros3").value = "";
                              document.getElementById("introc3").value = "";
                      } 
          compute_intros();           
          compute_all();
       });


         $('select[name="intro4"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("intros4").value = "5";
                              document.getElementById("introc4").value = "5";
                              break;
                      case 'No':
                              document.getElementById("intros4").value = "0";
                              document.getElementById("introc4").value = "5";
                              break;
                      default:
                              document.getElementById("intros4").value = "";
                              document.getElementById("introc4").value = "";
                      } 
          compute_intros();           
          compute_all();
       });



        $('select[name="intro5"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("intros5").value = "3";
                              document.getElementById("introc5").value = "3";
                              break;
                      case 'No':
                              document.getElementById("intros5").value = "1";
                              document.getElementById("introc5").value = "3";
                              break;
                      default:
                              document.getElementById("intros5").value = "";
                              document.getElementById("introc5").value = "";
                      } 
          compute_intros();           
          compute_all();
       });


        $('select[name="intro6"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("intros6").value = "3";
                              document.getElementById("introc6").value = "3";
                              break;
                      case 'No':
                              document.getElementById("intros6").value = "1";
                              document.getElementById("introc6").value = "3";
                              break;
                      default:
                              document.getElementById("intros6").value = "";
                              document.getElementById("introc6").value = "";
                      } 
          compute_intros();           
          compute_all();
       });


        $('select[name="survey1"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("surveys1").value = "3";
                              document.getElementById("surveyc1").value = "3";
                              break;
                      case 'No':
                              document.getElementById("surveys1").value = "1";
                              document.getElementById("surveyc1").value = "3";
                              break;
                      default:
                              document.getElementById("surveys1").value = "";
                              document.getElementById("surveyc1").value = "";
                      } 
          compute_survey();           
          compute_all();
       });


        $('select[name="survey2"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("surveys2").value = "15";
                              document.getElementById("surveyc2").value = "15";
                              break;
                      case 'No':
                              document.getElementById("surveys2").value = "1";
                              document.getElementById("surveyc2").value = "15";
                              break;
                      default:
                              document.getElementById("surveys2").value = "";
                              document.getElementById("surveyc2").value = "";
                      } 
          compute_survey();           
          compute_all();
       });


        $('select[name="survey3"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("surveys3").value = "5";
                              document.getElementById("surveyc3").value = "5";
                              break;
                      case 'No':
                              document.getElementById("surveys3").value = "1";
                              document.getElementById("surveyc3").value = "5";
                              break;
                      default:
                              document.getElementById("surveys3").value = "";
                              document.getElementById("surveyc3").value = "";
                      } 
          compute_survey();           
          compute_all();
       });


         $('select[name="inter1"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters1").value = "5";
                              document.getElementById("interc1").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters1").value = "1";
                              document.getElementById("interc1").value = "5";
                              break;
                      default:
                              document.getElementById("inters1").value = "";
                              document.getElementById("interc1").value = "";
                      } 
          compute_inter();            
          compute_all();
       });


          $('select[name="inter2"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters2").value = "5";
                              document.getElementById("interc2").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters2").value = "1";
                              document.getElementById("interc2").value = "5";
                              break;
                      default:
                              document.getElementById("inters2").value = "";
                              document.getElementById("interc2").value = "";
                      } 
          compute_inter();            
          compute_all();
       });



       $('select[name="inter3"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters3").value = "3";
                              document.getElementById("interc3").value = "3";
                              break;
                      case 'No':
                              document.getElementById("inters3").value = "1";
                              document.getElementById("interc3").value = "3";
                              break;
                      default:
                              document.getElementById("inters3").value = "";
                              document.getElementById("interc3").value = "";
                      } 
          compute_inter();            
          compute_all();
       });


        $('select[name="inter4"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters4").value = "3";
                              document.getElementById("interc4").value = "3";
                              break;
                      case 'No':
                              document.getElementById("inters4").value = "1";
                              document.getElementById("interc4").value = "3";
                              break;
                      default:
                              document.getElementById("inters4").value = "";
                              document.getElementById("interc4").value = "";
                      } 
          compute_inter();            
          compute_all();
       });



       $('select[name="inter5"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters5").value = "3";
                              document.getElementById("interc5").value = "3";
                              break;
                      case 'No':
                              document.getElementById("inters5").value = "1";
                              document.getElementById("interc5").value = "3";
                              break;
                      default:
                              document.getElementById("inters5").value = "";
                              document.getElementById("interc5").value = "";
                      } 
          compute_inter();            
          compute_all();
       });


        $('select[name="inter6"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters6").value = "5";
                              document.getElementById("interc6").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters6").value = "0";
                              document.getElementById("interc6").value = "5";
                              break;
                      default:
                              document.getElementById("inters6").value = "";
                              document.getElementById("interc6").value = "";
                      } 
          compute_inter();            
          compute_all();
       });



      $('select[name="inter7"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters7").value = "5";
                              document.getElementById("interc7").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters7").value = "0";
                              document.getElementById("interc7").value = "5";
                              break;
                      default:
                              document.getElementById("inters7").value = "";
                              document.getElementById("interc7").value = "";
                      } 
          compute_inter();            
          compute_all();
       });

       $('select[name="inter8"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters8").value = "5";
                              document.getElementById("interc8").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters8").value = "0";
                              document.getElementById("interc8").value = "5";
                              break;
                      default:
                              document.getElementById("inters8").value = "";
                              document.getElementById("interc8").value = "";
                      } 
          compute_inter();            
          compute_all();
       });



      $('select[name="inter9"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters9").value = "5";
                              document.getElementById("interc9").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters9").value = "1";
                              document.getElementById("interc9").value = "5"
                              break;
                      default:
                              document.getElementById("inters9").value = "";
                              document.getElementById("interc9").value = "";
                      } 
          compute_inter();            
          compute_all();
       });


      $('select[name="inter10"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters10").value = "5";
                              document.getElementById("interc10").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters10").value = "1";
                              document.getElementById("interc10").value = "5"
                              break;
                      default:
                              document.getElementById("inters10").value = "";
                              document.getElementById("interc10").value = "";
                      } 
          compute_inter();            
          compute_all();
       });

      $('select[name="inter11"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("inters11").value = "5";
                              document.getElementById("interc11").value = "5";
                              break;
                      case 'No':
                              document.getElementById("inters11").value = "0";
                              document.getElementById("interc11").value = "5"
                              break;
                      default:
                              document.getElementById("inters11").value = "";
                              document.getElementById("interc11").value = "";
                      } 
          compute_inter();            
          compute_all();
       });


      $('select[name="verba1"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas1").value = "5";
                              document.getElementById("verbac1").value = "5";
                              break;
                      case 'No':
                              document.getElementById("verbas1").value = "1";
                              document.getElementById("verbac1").value = "5"
                              break;
                      default:
                              document.getElementById("verbas1").value = "";
                              document.getElementById("verbac1").value = "";
                      } 
          compute_verba();            
          compute_all();
       });


      $('select[name="verba2"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas2").value = "5";
                              document.getElementById("verbac2").value = "5";
                              break;
                      case 'No':
                              document.getElementById("verbas2").value = "0";
                              document.getElementById("verbac2").value = "5"
                              break;
                      default:
                              document.getElementById("verbas2").value = "";
                              document.getElementById("verbac2").value = "";
                      } 
          compute_verba();            
          compute_all();
       });


      $('select[name="verba3"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas3").value = "3";
                              document.getElementById("verbac3").value = "3";
                              break;
                      case 'No':
                              document.getElementById("verbas3").value = "1";
                              document.getElementById("verbac3").value = "3"
                              break;
                      default:
                              document.getElementById("verbas3").value = "";
                              document.getElementById("verbac3").value = "";
                      } 
          compute_verba();            
          compute_all();
       });

      $('select[name="verba4"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas4").value = "3";
                              document.getElementById("verbac4").value = "3";
                              break;
                      case 'No':
                              document.getElementById("verbas4").value = "1";
                              document.getElementById("verbac4").value = "3"
                              break;
                      default:
                              document.getElementById("verbas4").value = "";
                              document.getElementById("verbac4").value = "";
                      } 
          compute_verba();            
          compute_all();
       });

      $('select[name="verba5"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas5").value = "10";
                              document.getElementById("verbac5").value = "10";
                              break;
                      case 'No':
                              document.getElementById("verbas5").value = "1";
                              document.getElementById("verbac5").value = "10"
                              break;
                      default:
                              document.getElementById("verbas5").value = "";
                              document.getElementById("verbac5").value = "";
                      } 
          compute_verba();            
          compute_all();
       });

      $('select[name="verba6"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas6").value = "5";
                              document.getElementById("verbac6").value = "5";
                              break;
                      case 'No':
                              document.getElementById("verbas6").value = "1";
                              document.getElementById("verbac6").value = "5"
                              break;
                      default:
                              document.getElementById("verbas6").value = "";
                              document.getElementById("verbac6").value = "";
                      } 
          compute_verba();            
          compute_all();
       });

      $('select[name="verba7"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas7").value = "5";
                              document.getElementById("verbac7").value = "5";
                              break;
                      case 'No':
                              document.getElementById("verbas7").value = "1";
                              document.getElementById("verbac7").value = "5"
                              break;
                      default:
                              document.getElementById("verbas7").value = "";
                              document.getElementById("verbac7").value = "";
                      } 
          compute_verba();            
          compute_all();
       });

      $('select[name="verba8"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("verbas8").value = "5";
                              document.getElementById("verbac8").value = "5";
                              break;
                      case 'No':
                              document.getElementById("verbas8").value = "1";
                              document.getElementById("verbac8").value = "5"
                              break;
                      default:
                              document.getElementById("verbas8").value = "";
                              document.getElementById("verbac8").value = "";
                      } 
          compute_verba();            
          compute_all();
       });


      $('select[name="closure1"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("closures1").value = "3";
                              document.getElementById("closurec1").value = "3";
                              break;
                      case 'No':
                              document.getElementById("closures1").value = "1";
                              document.getElementById("closurec1").value = "3";
                              break;
                      default:
                              document.getElementById("closures1").value = "";
                              document.getElementById("closurec1").value = "";
                      } 
          compute_closure();            
          compute_all();
       });


      $('select[name="closure2"]').on('change', function() {          
                switch($(this).val()) {
                      case 'Yes':
                              document.getElementById("closures2").value = "3";
                              document.getElementById("closurec2").value = "3";
                              break;
                      case 'No':
                              document.getElementById("closures2").value = "1";
                              document.getElementById("closurec2").value = "3";
                              break;
                      default:
                              document.getElementById("closures2").value = "";
                              document.getElementById("closurec2").value = "";
                      } 
          compute_closure();            
          compute_all();
       });



   });


